﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Cube.Models;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Authorization;
using Cube.AuthorizationPolicies;
using Cube.Services;

namespace Cube
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var sqlConnectionString = Configuration.GetConnectionString("NpgsqlConnection");
            services.AddDbContext<AppDbContext>(options => options.UseNpgsql(sqlConnectionString));

            //JWT Tokens. set token validation parameters
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    //установка ключа безопасности
                    IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                    //валидация ключа безопасности
                    ValidateIssuerSigningKey = true,
                    //будет ли валидироваться время существования
                    ValidateLifetime = true,

                    //При валидации издателя и потребителя аутентификация падает, возможно при описании токена забивать издателя и потребителя (Возможно проблема в том, что в Payload не было полей)
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            //Add custom authorization handlers JWT
            services.AddAuthorization(options =>
            {
                options.AddPolicy("WhiteTokensOnly", policy => policy.Requirements.Add(new WhiteTokenRequirement()));
            });

            services.AddTransient<IAuthorizationHandler, IsWhiteTokenAuthorizationHandler>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //Set default data in database
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                //var context = serviceScope.ServiceProvider.GetRequiredService<AppDbContext>();

                //context.Database.EnsureDeleted();
                //context.Database.Migrate();
                //context.Database.EnsureCreated();
                //serviceScope.ServiceProvider.GetService<AppDbContext>().EnsureSeedData();
            }

            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            app.UseMvcWithDefaultRoute();
            app.UseDefaultFiles();
            app.UseStaticFiles();   //For the wwwroot folder


            //if (env.IsDevelopment())
            //{
            app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseHsts();
            //}

            app.UseHttpsRedirection();
            app.UseMvc();

            //app.UseSpaStaticFiles();

            //app.UseSpa(spa =>
            //{
            //    spa.Options.SourcePath = "ClientApp/wwwroot";

            //    //if (env.IsDevelopment())
            //    //{
            //    //    spa.UseAngularCliServer(npmScript: "start");
            //    //}

            //});
        }
    }
}
