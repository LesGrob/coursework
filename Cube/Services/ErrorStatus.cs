﻿using System;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Cube.Services
{
    public class ErrorStatus
    {
        public IActionResult ObjectResult(int statusCode, string message)
        {
            var result = new ObjectResult(new { Code = statusCode, Message = message });
            result.StatusCode = statusCode;
            return result;
        }
    }
}

