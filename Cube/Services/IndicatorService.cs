using Cube.DTO;
using Cube.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Cube.Services
{
    public class IndicatorService
    {
        private AppDbContext context;

        public IndicatorService(AppDbContext _context) { context = _context; }
        public IndicatorService() { }

        /// Calculate indicator in some layer
        public void CalculateIndicator(long IndicatorId, Boolean resursive, long MeasureId, long PeriodId, long PlanId, long TypeId, long DepartmentId, DateTimeOffset date)
        {
            Indicator indicator = context.Indicators.Include(x => x.Project).FirstOrDefault(x => x.Id == IndicatorId);
            IndicatorType type = context.IndicatorTypes.FirstOrDefault(x => x.Id == TypeId);
            Measure measure = context.Measures.FirstOrDefault(x => x.Id == MeasureId);
            Plan plan = context.Plans.FirstOrDefault(x => x.Id == PlanId);
            Period period = context.Periods.FirstOrDefault(x => x.Id == PeriodId);
            Department department = context.Departments.FirstOrDefault(x => x.Id == DepartmentId);

            if (indicator == null || type == null || measure == null || plan == null || period == null || department == null)
                throw new Exception();

            if (resursive)
                foreach (var indic in context.Indicators.Where(x => x.Parent == indicator))
                    CalculateIndicator(indic.Id, true, MeasureId, PeriodId, PlanId, TypeId, DepartmentId, date);

            ////////////
            bool indicatorLeaf = !context.Indicators.Any(x => x.Parent == indicator);
            bool departLeaf = !context.Departments.Any(x => x.Parent == department);

            List<DataFact> facts = context.DataFacts.Include(x => x.Indicator).Include(x => x.Indicator.Parent).Include(x => x.Department.Parent).Where(x =>
             x.Indicator.Type.Id == TypeId &&
             x.Measure.Id == MeasureId &&
             x.Period.Id == PeriodId &&
             (indicatorLeaf ? x.Indicator == indicator : x.Indicator.Parent == indicator) &&
             (departLeaf ? x.Department == department : (x.Department.Parent == department && x.Department != department)) &&
             x.ReportDate.Date == date.Date)
                 .OrderBy(x => x.Id).GroupBy(x => new { x.Department, x.Indicator }).Distinct().Select(x => x.Last()).ToList();

            List<DataPlan> dataPlans = new List<DataPlan>();

            foreach (var fact in facts)
                dataPlans.Add(
                    context.DataPlans.Include(x => x.Indicator).Include(x => x.Indicator.Parent).Include(x => x.Department.Parent).OrderBy(x => x.Id).LastOrDefault(x =>
                   x.Indicator.Parent == indicator &&
                   x.Indicator.Type.Id == TypeId &&
                   x.Plan.Id == PlanId &&
                   x.Measure.Id == MeasureId &&
                   x.Period.Id == PeriodId &&
                   x.Department.Id == fact.Department.Id &&
                   x.ReportDate.Date == fact.ReportDate.Date
                        ));
            dataPlans = dataPlans.Where(x => x != null).ToList();

            if (facts.Select(x => x.Value).Sum() > 0)
            {
                DataFact indicatorFact;
                foreach (var fact in facts.GroupBy(x => x.Department).Distinct())
                {
                    indicatorFact = new DataFact();
                    indicatorFact.Indicator = indicator;
                    indicatorFact.Measure = measure;
                    indicatorFact.Period = period;
                    indicatorFact.ReportDate = date.Date;
                    indicatorFact.Project = indicator.Project;

                    indicatorFact.Department = fact.Last().Department;
                    indicatorFact.Value = fact.Select(x => x.Value).Sum();

                    context.DataFacts.Add(indicatorFact);
                }

                indicatorFact = new DataFact();
                indicatorFact.Indicator = indicator;
                indicatorFact.Measure = measure;
                indicatorFact.Period = period;
                indicatorFact.ReportDate = date.Date;
                indicatorFact.Project = indicator.Project;

                indicatorFact.Department = department;
                indicatorFact.Value = facts.Select(x => x.Value).Sum();

                context.DataFacts.Add(indicatorFact);
            }
            if (dataPlans.Select(x => x.Value).Sum() > 0)
            {
                DataPlan indicatorPlan;

                foreach (var planData in dataPlans.GroupBy(x => x.Department).Distinct())
                {
                    indicatorPlan = new DataPlan();
                    indicatorPlan.Indicator = indicator;
                    indicatorPlan.Measure = measure;
                    indicatorPlan.Period = period;
                    indicatorPlan.Plan = plan;
                    indicatorPlan.ReportDate = date.Date;
                    indicatorPlan.Project = indicator.Project;

                    indicatorPlan.Department = planData.Last().Department;
                    indicatorPlan.Value = planData.Select(x => x.Value).Sum();

                    context.DataPlans.Add(indicatorPlan);
                }

                indicatorPlan = new DataPlan();
                indicatorPlan.Indicator = indicator;
                indicatorPlan.Measure = measure;
                indicatorPlan.Period = period;
                indicatorPlan.Plan = plan;
                indicatorPlan.ReportDate = date.Date;
                indicatorPlan.Project = indicator.Project;

                indicatorPlan.Department = department;
                indicatorPlan.Value = dataPlans.Select(x => x.Value).Sum();

                context.DataPlans.Add(indicatorPlan);
            }
            context.SaveChanges();



        }

        #region Get Indicator or Employee data

        public Indicator_DTO IndicatorData(long IndicatorId, DateTime Date, long MeasureId = 0, long PeriodId = 0, long DepartmentId = 0, long EmployeeId = 0)
        {
            Indicator_DTO indicatorData = new Indicator_DTO(context.Indicators.Include(x => x.MeasureDefault).Include(x => x.PeriodDefault).Include(x => x.PlanDefault).Include(x => x.Type).FirstOrDefault(x => x.Id == IndicatorId));
            if (indicatorData != null)
            {
                indicatorData.DataFact = DataFact(IndicatorId, Date, MeasureId, PeriodId, DepartmentId);
                if (indicatorData.DataFact != null)
                {
                    //Рассчет процента выполнения показателя
                    indicatorData.DataPlan = DataPlan(IndicatorId, indicatorData.DataFact.ReportDate, indicatorData.DataFact.Measure.Id, indicatorData.DataFact.Period.Id, indicatorData.DataFact.Department.Id);
                    if (indicatorData.DataFact.Measure.Code == "percent")
                        indicatorData.ExecutionPercentage = (int)indicatorData.DataFact.Value;
                    else
                        indicatorData.ExecutionPercentage = indicatorData.DataPlan == null ? 0 : (int)((indicatorData.DataFact.Value / indicatorData.DataPlan.Value) * 100);

                    //Рассчет данных по подразделениям
                    if (context.Departments.Any(x => x.Parent.Id == DepartmentId))
                    {
                        List<DataFact> tempDataFacts = context.DataFacts.Include(x => x.Department).Include(x => x.Department.Parent).Include(x => x.Indicator)
                                                   .Where(x => x.Indicator.Id == IndicatorId && x.Department.Parent.Id == DepartmentId).ToList();
                        List<Department> departments = tempDataFacts.GroupBy(x => x.Department).Distinct().Select(x => x.FirstOrDefault()).Select(x => x.Department).ToList();



                        var departmentsPercents = departments.Select(x => new
                        {
                            FactValue = DataFact(IndicatorId, Date, MeasureId, PeriodId, x.Id),
                            PlanValue = DataPlan(IndicatorId, Date, MeasureId, PeriodId, x.Id)
                        }).Select(x => (x.FactValue == null || x.PlanValue == null) ? 0.0 : (indicatorData.Type.Code == "inverse" ? (x.PlanValue.Value / x.FactValue.Value * 100) : (x.FactValue.Value / x.PlanValue.Value * 100)));

                        indicatorData.DepartmentsDone = departmentsPercents.Count(x => x > 100);
                        indicatorData.DepartmentsFail = departmentsPercents.Count() - indicatorData.DepartmentsDone;

                    }

                    //Рассчет данных по дочерним показателям
                    if (context.Indicators.Any(x => x.Parent.Id == IndicatorId))
                    {
                        List<Indicator> indicators = context.Indicators.Include(x => x.Parent).Include(x => x.PeriodDefault).Include(x => x.Type).Include(x => x.MeasureDefault)
                            .Where(x => x.Parent.Id == IndicatorId).ToList();

                        var indicatorsPercents = indicators.Select(x => new
                        {
                            FactValue = DataFact(x.Id, Date, x.MeasureDefault.Id, x.PeriodDefault.Id, DepartmentId),
                            PlanValue = DataPlan(x.Id, Date, x.MeasureDefault.Id, x.PeriodDefault.Id, DepartmentId),
                            Type = x.Type
                        }).Select(x => (x.FactValue == null || x.PlanValue == null) ? 0 : (x.Type.Code == "inverse" ? (x.PlanValue.Value / x.FactValue.Value * 100) : (x.FactValue.Value / x.PlanValue.Value * 100)));

                        indicatorData.DriversDone = indicatorsPercents.Count(x => x > 100);
                        indicatorData.DriversFail = indicators.Count() - indicatorData.DriversDone;
                    }

                    //Рассчет динамики
                    #region
                    DateTime DynamicTime = indicatorData.DataFact.ReportDate;
                    switch (indicatorData.DataFact.Period.Code)
                    {
                        case "day":
                            if (DynamicTime > DateTime.MinValue.AddDays(1))
                                DynamicTime = DynamicTime.AddDays(-1);
                            break;
                        case "week":
                            if (DynamicTime > DateTime.MinValue.AddDays(7))
                                DynamicTime = DynamicTime.AddDays(-7);
                            break;
                        case "mounth":
                            if (DynamicTime > DateTime.MinValue.AddMonths(1))
                                DynamicTime = DynamicTime.AddMonths(-1);
                            break;
                        default: break;
                    }

                    DataFact tempRRData = DataFact(IndicatorId, DynamicTime, MeasureId, PeriodId, DepartmentId);

                    indicatorData.Dynamic = new
                    {
                        Gap = (int)(indicatorData.DataFact.Value - (tempRRData == null ? 0 : tempRRData.Value)),
                        Value = tempRRData == null ? 0 : tempRRData.Value,
                        Date = DynamicTime.Date
                    };
                    #endregion

                    //Рассчет RunRate
                    #region
                    List<DataFact> runRate = context.DataFacts.Where(x =>
                                         x.Indicator.Id == IndicatorId &&
                                         x.Measure.Id == MeasureId &&
                                         x.Period.Id == PeriodId &&
                                         x.Department.Id == DepartmentId
                                     ).OrderBy(x => x.ReportDate.Date)
                                     .GroupBy(x => x.ReportDate.Date).Select(x => x.Last())
                                     .Take(20).ToList();

                    indicatorData.RunRate = new
                    {
                        Value = runRate == null ? 0 : (int)runRate.Select(x => x.Value).Average(),
                        Date = AddDate(indicatorData.DataFact.Period.Code, indicatorData.DataFact.ReportDate.Date)
                    };

                    #endregion

                    //Поиск данных по сотруднику
                    if (EmployeeId != 0)
                    {
                        indicatorData.DataEmployee = new DataEmployee_DTO(DataEmployee(IndicatorId, Date, MeasureId, PeriodId, DepartmentId, EmployeeId));

                        var tempStuffNorm = StuffNorm(IndicatorId, Date, MeasureId, PeriodId, DepartmentId, EmployeeId);

                        if (tempStuffNorm != null)
                            indicatorData.DataEmployee.StuffNormValue = tempStuffNorm.Value;
                    }
                }

            }
            return indicatorData;
        }

        /// Get Fact data of indicator in some layer
        public DataFact DataFact(long IndicatorId, DateTime Date, long MeasureId = 0, long PeriodId = 0, long DepartmentId = 0)
        {
            return context.DataFacts.Include(x => x.Department).Include(x => x.Measure).Include(x => x.Period).Include(x => x.Indicator)
                .OrderBy(x => x.Id).LastOrDefault(x => x.Indicator.Id == IndicatorId
                 && (MeasureId != 0 ? x.Measure.Id == MeasureId : true)
                 && (PeriodId != 0 ? x.Period.Id == PeriodId : true)
                 && (DepartmentId != 0 ? x.Department.Id == DepartmentId : true)
                 && (Date == DateTime.MinValue ? true : x.ReportDate.Date == Date.Date));
        }

        /// Get Plan data of indicator in some layer
        public DataPlan DataPlan(long IndicatorId, DateTime Date, long MeasureId = 0, long PeriodId = 0, long DepartmentId = 0)
        {
            return context.DataPlans.Include(x => x.Department).Include(x => x.Measure).Include(x => x.Period).Include(x => x.Indicator)
                    .OrderBy(x => x.ReportDate)
                    .LastOrDefault(x => x.Indicator.Id == IndicatorId
                       && (MeasureId != 0 ? x.Measure.Id == MeasureId : true)
                       && (PeriodId != 0 ? x.Period.Id == PeriodId : true)
                       && (DepartmentId != 0 ? x.Department.Id == DepartmentId : true)
                       && (x.ReportDate.Date <= Date.Date));
        }

        /// Get data by Employee & Indicator in some layer
        public DataEmployee DataEmployee(long IndicatorId, DateTime Date, long MeasureId, long PeriodId, long DepartmentId, long EmployeeId)
        {
            return context.DataEmployee.Include(x => x.Indicator).Include(x => x.Period).Include(x => x.Measure).Include(x => x.Department)
                        .LastOrDefault(x => x.Employee.Id == EmployeeId
                                       && x.Indicator.Id == IndicatorId
                                       && x.Department.Id == DepartmentId
                                       && x.Measure.Id == MeasureId
                                       && x.Period.Id == PeriodId
                                       && x.ReportDate.Date == Date.Date);
        }

        /// Get stuffNorm of Employee in some layer
        public StuffNorm StuffNorm(long IndicatorId, DateTime Date, long MeasureId, long PeriodId, long DepartmentId, long EmployeeId)
        {
            Employee employee = context.Employees.FirstOrDefault(x => x.Id == EmployeeId);
            if (employee == null) return null;
            return context.StuffNorms.Include(x => x.Indicator).Include(x => x.Period).Include(x => x.Measure).Include(x => x.Department)
                .OrderBy(x => x.Id)
                .LastOrDefault(x => x.Indicator.Id == IndicatorId
                   && x.Department.Id == DepartmentId
                   && x.Measure.Id == MeasureId
                   && x.Period.Id == PeriodId
                   && x.DateStart <= Date
                   && (x.DateEnd >= Date
                   || x.DateEnd == DateTime.MinValue));
        }


        private DateTime AddDate(string Period, DateTime Date)
        {
            if (Period == "day")
                Date = Date.AddDays(1);
            if (Period == "week")
                Date = Date.AddDays(7);
            if (Period == "month")
                Date = Date.AddMonths(1);
            return Date;
        }
        #endregion

        #region Post Indicator or Employee data

        /// Post Plan and Fact data
        public HttpStatusCode PlanFactData(List<PlanFactData_DTO> DataList)
        {
            if (DataList == null)
                throw new Exception("Incorrect data!");
            if (DataList.Count == 0)
                throw new Exception("Empty data!");

            foreach (PlanFactData_DTO data in DataList)
            {
                Department department = context.Departments.Include(x => x.Project).FirstOrDefault(x => x.Id == data.DepartmentId);
                Measure measure = context.Measures.FirstOrDefault(x => x.Id == data.MeasureId);
                Period period = context.Periods.FirstOrDefault(x => x.Id == data.PeriodId);
                Indicator indicator = context.Indicators.FirstOrDefault(x => x.Id == data.IndicatorId);
                Plan plan = context.Plans.FirstOrDefault(x => x.Id == data.PlanId);
                DateTimeOffset date = data.ReportDate.Date;

                if (department == null || measure == null || period == null || indicator == null || plan == null)
                    throw new Exception("Incorrect data!");

                if (data.FactValue != 0)
                {
                    DataFact factData = context.DataFacts.FirstOrDefault(x => x.Department == department && x.Measure == measure && x.Period == period && x.Indicator == indicator && x.ReportDate.Date == date.Date);
                    if (factData != null)
                    {
                        factData.Value = data.FactValue;
                    }
                    else
                    {
                        factData = new DataFact();
                        factData.Value = data.FactValue;
                        factData.ReportDate = date.Date;
                        factData.Measure = measure;
                        factData.Period = period;
                        factData.Department = department;
                        factData.Indicator = indicator;
                        factData.Project = department.Project;

                        context.DataFacts.Add(factData);
                    }
                }

                if (data.PlanValue != 0)
                {
                    DataPlan tempPlan = context.DataPlans.FirstOrDefault(x => x.Department == department && x.Measure == measure && x.Period == period && x.Indicator == indicator && x.Plan == plan && x.ReportDate.Date == date.Date);
                    if (tempPlan != null)
                    {
                        tempPlan.Value = data.PlanValue;
                    }
                    else
                    {
                        DataPlan planData = new DataPlan();
                        planData.Value = data.PlanValue;
                        planData.ReportDate = date.Date;
                        planData.Measure = measure;
                        planData.Period = period;
                        planData.Department = department;
                        planData.Indicator = indicator;
                        planData.Plan = plan;
                        planData.Project = department.Project;

                        context.DataPlans.Add(planData);
                    }
                }

                context.SaveChanges();
            }

            return HttpStatusCode.OK;
        }

        /// Post fact data by employee
        public HttpStatusCode EmployeePlanFactData(List<DataEmployee_DTO> DataList)
        {
            if (DataList == null)
                throw new Exception("Incorrect data!");

            if (DataList.Count == 0)
                throw new Exception("Empty data!");

            foreach (DataEmployee_DTO data in DataList)
            {
                Department department = context.Departments.Include(x => x.Project).FirstOrDefault(x => x.Id == data.DepartmentId);
                Measure measure = context.Measures.FirstOrDefault(x => x.Id == data.MeasureId);
                Period period = context.Periods.FirstOrDefault(x => x.Id == data.PeriodId);
                Indicator indicator = context.Indicators.FirstOrDefault(x => x.Id == data.IndicatorId);
                Project project = context.Projects.FirstOrDefault(x => x.Id == data.ProjectId);
                Employee employee = context.Employees.FirstOrDefault(x => x.Id == data.Employee.Id);
                DateTimeOffset date = data.ReportDate.Date;

                if (department == null || measure == null || period == null || indicator == null)
                    throw new Exception("Incorrect data!");

                EmployeePost employeePost = context.EmployeePosts.Include(x => x.Post.Type).FirstOrDefault(x => x.Employee == employee);

                if (data.Value != 0)
                {
                    DataEmployee dataEmployee = context.DataEmployee.FirstOrDefault(x =>
                           x.Indicator == indicator &&
                           x.Measure == measure &&
                           x.Period == period &&
                           x.Project == department.Project &&
                           x.Employee == employee &&
                           x.Department == department &&
                           x.ReportDate.Date == date.Date);
                    if (dataEmployee != null)
                    {
                        dataEmployee.Value = data.Value;
                    }
                    else
                    {
                        dataEmployee = new DataEmployee();
                        dataEmployee.Value = data.Value;
                        dataEmployee.ReportDate = date.Date;
                        dataEmployee.Measure = measure;
                        dataEmployee.Period = period;
                        dataEmployee.Department = department;
                        dataEmployee.Indicator = indicator;
                        dataEmployee.Project = department.Project;
                        dataEmployee.Employee = employee;

                        context.DataEmployee.Add(dataEmployee);
                    }
                }

                if (data.StuffNormValue != 0)
                {
                    StuffNorm(new List<StuffNorm_DTO>() {
                        new StuffNorm_DTO(){
                            Value = data.StuffNormValue,
                            MeasureId = measure.Id,
                            PeriodId = period.Id,
                            DepartmentId = department.Id,
                            IndicatorId = indicator.Id,
                            ProjectId = department.Project.Id,
                            PostTypeId = employeePost.Post.Type.Id,
                            DateStart = date.Date
                    }});
                }
            }

            context.SaveChanges();

            return HttpStatusCode.OK;
        }

        /// Post stuff norm data
        public HttpStatusCode StuffNorm(List<StuffNorm_DTO> DataList)
        {
            if (DataList == null)
                throw new Exception("Incorrect data!");

            if (DataList.Count == 0)
                throw new Exception("Empty data!");

            foreach (StuffNorm_DTO data in DataList)
            {
                if (data.Value != 0)
                {
                    PostType postType = context.PostTypes.Include(x => x.Project).FirstOrDefault(x => x.Id == data.PostTypeId);
                    Department department = context.Departments.Include(x => x.Project).FirstOrDefault(x => x.Id == data.DepartmentId);
                    Measure measure = context.Measures.FirstOrDefault(x => x.Id == data.MeasureId);
                    Period period = context.Periods.FirstOrDefault(x => x.Id == data.PeriodId);
                    Indicator indicator = context.Indicators.FirstOrDefault(x => x.Id == data.IndicatorId);
                    Project workSpace = context.Projects.FirstOrDefault(x => x.Id == data.ProjectId);

                    if (postType == null || department == null || measure == null || period == null || indicator == null)
                        throw new Exception("Incorrect data!");

                    List<StuffNorm> stuffNorm = context.StuffNorms.OrderBy(x => x.Id)
                        .Where(x => x.Post == postType
                                       && x.Department == department
                                       && x.Measure == measure
                                       && x.Period == period
                                       && x.Indicator == indicator
                                       && x.DateStart.Date <= data.DateStart.Date
                                       && (x.DateEnd.Date == DateTime.MinValue
                                           || x.DateEnd.Date >= data.DateStart.Date)
                                           || x.DateStart.Date >= data.DateStart.Date).ToList();
                    foreach (var norm in stuffNorm)
                        if (norm.DateStart.Date <= data.DateStart.Date)
                            norm.DateEnd = data.DateStart.AddDays(-1).Date;
                        else if (norm.DateStart.Date > data.DateStart.Date)
                        {
                            data.DateEnd = norm.DateStart.AddDays(-1).Date;
                            break;
                        }


                    StuffNorm newNorm = new StuffNorm();

                    newNorm.Value = data.Value;
                    newNorm.DateStart = data.DateStart.Date;
                    newNorm.DateEnd = data.DateEnd.Date;
                    newNorm.Measure = measure;
                    newNorm.Period = period;
                    newNorm.Department = department;
                    newNorm.Indicator = indicator;
                    newNorm.Project = department.Project;
                    newNorm.Post = postType;

                    context.StuffNorms.Add(newNorm);
                }
            }

            context.SaveChanges();
            return HttpStatusCode.OK;
        }

        #endregion
    }
}
