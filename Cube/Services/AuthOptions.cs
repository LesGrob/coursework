using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Cube.Services
{
  public class AuthOptions
  {
    public const string ISSUER = "reserveapp_api"; // издатель токена
    public const string AUDIENCE = "Client"; // потребитель токена
    const string KEY = "CubeIsTheBest!CubeIsTheBest!";   // ключ для шифрации
    public const int LIFETIME = 1; // время жизни токена (единицы измерения задаются при создании токена)

    public static SymmetricSecurityKey GetSymmetricSecurityKey()
    {
      return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
    }
  }
}
