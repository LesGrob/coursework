using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    /// <summary>
    /// Объект DTO, описывающий плановое значение за период
    /// </summary>
    public class DataPlan_DTO
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Значение
        /// </summary>
        public float Value { get; set; }
        /// <summary>
        /// Отчетная дата
        /// </summary>
        public DateTimeOffset ReportDate { get; set; }

        /// <summary>
        /// Конструктор, формирующий объект
        /// </summary>
        /// <param name="dataPlan">Плановое значение по офису</param>
        public DataPlan_DTO(DataPlan dataPlan)
        {
            Id = dataPlan.Id;
            Value = dataPlan.Value;
            ReportDate = dataPlan.ReportDate;
        }

        /// <summary>
        /// Конструктор, формирующий объект
        /// </summary>
        /// <param name="stuffNorm">Плановое значение по сотруднику</param>
        public DataPlan_DTO(StuffNorm stuffNorm)
        {
            Id = stuffNorm.Id;
            Value = stuffNorm.Value;
            ReportDate = stuffNorm.DateStart;
        }
    }
}
