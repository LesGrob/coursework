using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    /// <summary>
    /// Объект DTO, описывающий тип департамента
    /// </summary>
    public class DepartmentType_DTO
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Код типа департамента
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Наименование типа департамента
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Объект, описывающий WorkSpace
        /// </summary>
        public Project Project { get; set; }

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public DepartmentType_DTO() { }

        /// <summary>
        /// Конструктор, формирующий объект
        /// </summary>
        /// <param name="departmentType">Сущность тип департамента</param>
        public DepartmentType_DTO(DepartmentType departmentType)
        {
            Id = departmentType.Id;
            Code = departmentType.Code;
            Name = departmentType.Name;
            Project = departmentType.Project;
        }
    }
}
