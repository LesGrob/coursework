using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    public class Shelf_DTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public Project Project { get; set; }
        public List<Indicator_DTO> Indicators { get; set; }

        public Shelf_DTO(Shelf shelf)
        {
            this.Id = shelf.Id;
            this.Name = shelf.Name;
            this.Project = shelf.Project;
        }
        public Shelf_DTO() { }
    }
}
