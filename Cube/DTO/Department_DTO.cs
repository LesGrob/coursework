using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    /// <summary>
    /// Объект DTO, описывающий департамент
    /// </summary>
    public class Department_DTO
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Код департамента
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Наименование департамента
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Дата создания департамента
        /// </summary>
        public DateTimeOffset DateStart { get; set; }
        /// <summary>
        /// Дата закрытия департамента (null - департамент не закрыт)
        /// </summary>
        public DateTimeOffset? DateEnd { get; set; }
        /// <summary>
        /// Объект родительского департамента
        /// </summary>
        public Department_DTO Parent { get; set; }
        /// <summary>
        /// Объект, описывающий тип департамента
        /// </summary>
        public DepartmentType_DTO Type { get; set; }
        /// <summary>
        /// объект, описывающий WorkSpace
        /// </summary>
        public Project Project { get; set; }
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public Department_DTO() { }

        /// <summary>
        /// Конструктор, формирующий объект
        /// </summary>
        /// <param name="department">Сущность департамент</param>
        public Department_DTO(Department department)
        {
            Id = department.Id;
            Code = department.Code;
            Name = department.Name;
            DateStart = department.DateStart;
            DateEnd = department.DateEnd;
            Parent = department.Parent != null ? new Department_DTO(department.Parent) : null;
            Type = department.Type != null ? new DepartmentType_DTO(department.Type) : null;
            Project = department.Project;
        }
    }
}
