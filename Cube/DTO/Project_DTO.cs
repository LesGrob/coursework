using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    /// <summary>
    /// Объект DTO, описывающий WorkSpace
    /// </summary>
    public class Project_DTO
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Наименование WorkSpace
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Конструктор, формирующий объект
        /// </summary>
        /// <param name="project">Сущность project</param>
        public Project_DTO(Project project)
        {
            Id = project.Id;
            Name = project.Name;
        }
    }
}
