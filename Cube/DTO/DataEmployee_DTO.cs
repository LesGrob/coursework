using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    public class DataEmployee_DTO
    {
        public long Id { get; set; }

        public float Value { get; set; }

        public DateTimeOffset ReportDate { get; set; }

        public long MeasureId { get; set; }
        public long PeriodId { get; set; }
        public long DepartmentId { get; set; }
        public long IndicatorId { get; set; }
        public virtual Employee Employee { get; set; }

        public long ProjectId { get; set; }

        public float StuffNormValue { get; set; }

        public DataEmployee_DTO() { }

        public DataEmployee_DTO(DataEmployee dataEmployee) {
            if(dataEmployee != null)
            {
                this.Id = dataEmployee.Id;
                this.Value = dataEmployee.Value;
                this.ReportDate = dataEmployee.ReportDate;
                this.MeasureId = dataEmployee.Measure.Id;
                this.PeriodId = dataEmployee.Period.Id;
                this.DepartmentId = dataEmployee.Department.Id;
                this.Employee = dataEmployee.Employee;
                this.ProjectId = dataEmployee.Project.Id;
            }
        }
    }
}
