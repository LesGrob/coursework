using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    /// <summary>
    /// Объект DTO, формирующий базовые поля индикатора
    /// </summary>
    public class IndicatorBase_DTO
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Код индикатора
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Наименование индикатора
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public IndicatorBase_DTO() { }

        /// <summary>
        /// Конструктор, формирующий объект
        /// </summary>
        /// <param name="indicator">Сущность индикатор</param>
        public IndicatorBase_DTO(Indicator indicator)
        {
            Id = indicator.Id;
            Code = indicator.Code;
            Name = indicator.Name;
        }
    }
}
