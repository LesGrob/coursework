using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    public class PlanFactData_DTO
    {
        public int DepartmentId { get; set; }
        public float FactValue { get; set; }
        public float PlanValue { get; set; }
        public int IndicatorId { get; set; }
        public int PlanId { get; set; }
        public int MeasureId { get; set; }
        public int PeriodId { get; set; }
        public DateTime ReportDate { get; set; }
    }
}
