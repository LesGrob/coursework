using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    /// <summary>
    /// Объект DTO, описывающий сотрудника
    /// </summary>
    public class Employee_DTO
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Объект DTO, описывающий должность сотрудника
        /// </summary>
        //public Post_DTO Post { get; set; }
        /// <summary>
        /// Объект DTO, описывающий департамент, к которому прикреплен сотрудник
        /// </summary>
        //public Department_DTO Department { get; set; }
        /// <summary>
        /// Табельный номер сотрудника
        /// </summary>
        public string TabNum { get; set; }

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public Employee_DTO() { }

        /// <summary>
        /// Конструктор, формирующий объект
        /// </summary>
        /// <param name="employee">Сущность сотрудник</param>
        public Employee_DTO(Employee employee)
        {
            Id = employee.Id;
            Surname = employee.Surname;
            Name = employee.Name;
            MiddleName = employee.Middlename;
            //Post = new Post_DTO(employee.Post);
            //Department = new Department_DTO(employee.Post.Department);
            TabNum = employee.TabNum;
        }
    }
}
