using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    /// <summary>
    /// Объект DTO, описывающий фактическое значение за период
    /// </summary>
    public class DataFact_DTO
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Значение
        /// </summary>
        public float Value { get; set; }
        /// <summary>
        /// Отчетная дата
        /// </summary>
        public DateTime ReportDate { get; set; }

        /// <summary>
        /// Конструктор формирующий объект
        /// </summary>
        /// <param name="dataFact">Фактическое значение по офису</param>
        public DataFact_DTO(DataFact dataFact)
        {
            Id = dataFact.Id;
            Value = dataFact.Value;
            ReportDate = dataFact.ReportDate;
        }

        /// <summary>
        /// Конструктор формирующий объект
        /// </summary>
        /// <param name="dataFact">Фактическое значение по сотруднику</param>
        public DataFact_DTO(DataEmployee dataFact)
        {
            Id = dataFact.Id;
            Value = dataFact.Value;
            ReportDate = dataFact.ReportDate;
        }
    }
}
