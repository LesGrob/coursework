using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Cube.DTO
{
    public class Indicator_DTO
    {
        public long Id { get; set; }
        public long UserShelfId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public virtual IndicatorType Type { get; set; }
        public virtual Department Owner { get; set; }
        public virtual Indicator Parent { get; set; }

        public virtual Measure MeasureDefault { get; set; }
        public virtual Period PeriodDefault { get; set; }
        public virtual Plan PlanDefault { get; set; }

        public DataEmployee_DTO DataEmployee { get; set; }

        public int ExecutionPercentage { get; set; }

        public int DepartmentsFail { get; set; }
        public int DepartmentsDone { get; set; }

        public int DriversFail { get; set; }
        public int DriversDone { get; set; }

        public object Dynamic { get; set; }

        public object RunRate { get; set; }

        public virtual Project Project { get; set; }

        public virtual DataFact DataFact { get; set; }
        public virtual DataPlan DataPlan { get; set; }

        public Indicator_DTO() { }

        public Indicator_DTO(Indicator indicator)
        {
            Id = indicator.Id;
            Code = indicator.Code;
            Name = indicator.Name;
            Type = indicator.Type;
            Owner = indicator.Owner;
            Parent = indicator.Parent;
            MeasureDefault = indicator.MeasureDefault;
            PeriodDefault = indicator.PeriodDefault;
            PlanDefault = indicator.PlanDefault;
            Project = indicator.Project;
        }
    }
}
