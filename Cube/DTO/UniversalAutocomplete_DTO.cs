using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    public class UniversalAutocomplete_DTO
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public UniversalAutocomplete_DTO(Department department)
        {
            Id = department.Id;
            Name = department.Name;
        }
        public UniversalAutocomplete_DTO(Post post)
        {
            Id = post.Id;
            Name = post.Name;
        }
       
        public UniversalAutocomplete_DTO(Indicator indicator)
        {
            Id = indicator.Id;
            Name = indicator.Name;
        }

        public UniversalAutocomplete_DTO(Plan plan)
        {
            Id = plan.Id;
            Name = plan.Name;
        }
    }
}
