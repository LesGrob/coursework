using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    /// <summary>
    /// Объект DTO, описывающий должность сотрудника
    /// </summary>
    public class Post_DTO
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Наименование должности
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Конструктор, формирующий объект
        /// </summary>
        /// <param name="post">Сущность долнжость</param>
        public Post_DTO(Post post)
        {
            Id = post.Id;
            Name = post.Name;
        }
    }
}
