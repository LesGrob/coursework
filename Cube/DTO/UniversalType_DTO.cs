using Cube.Models;

namespace Cube.DTO
{
    public class UniversalType_DTO
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public Project Project { get; set; }
        public float Value { get; set; }

        public UniversalType_DTO() { }
        public UniversalType_DTO(DepartmentType departmentType)
        {
            Id = departmentType.Id;
            Code = departmentType.Code;
            Name = departmentType.Name;
            Project = departmentType.Project;
        }
        public UniversalType_DTO(PostType postType)
        {
            Id = postType.Id;
            Code = postType.Code;
            Name = postType.Name;
            Project = postType.Project;
        }
    }
}
