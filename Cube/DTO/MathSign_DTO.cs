using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    /// <summary>
    /// Объект DTO для мат. знаков
    /// </summary>
    public class MathSign_DTO
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Значение знака
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public MathSign_DTO() { }
        /// <summary>
        /// Конструктор, формирующий объект
        /// </summary>
        /// <param name="mathSign"></param>
        public MathSign_DTO(MathSign mathSign)
        {
            Id = mathSign.Id;
            Value = mathSign.Value;
        }
    }
}
