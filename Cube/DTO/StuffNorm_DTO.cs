using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    public class StuffNorm_DTO
    {
        public long Id { get; set; }

        public float Value { get; set; }

        public DateTimeOffset DateStart { get; set; }
        public DateTimeOffset DateEnd { get; set; }

        public long PostTypeId { get; set; }

        public long MeasureId { get; set; }
        public long PeriodId { get; set; }
        public long DepartmentId { get; set; }
        public long IndicatorId { get; set; }

        public long ProjectId { get; set; }


        public StuffNorm_DTO() { }

        public StuffNorm_DTO(StuffNorm stuffnorm)
        {
            if (stuffnorm != null)
            {
                this.Id = stuffnorm.Id;
                this.Value = stuffnorm.Value;

                this.DateStart = stuffnorm.DateStart;
                this.DateEnd = stuffnorm.DateEnd;

                this.IndicatorId = stuffnorm.Indicator.Id;

                this.PeriodId = stuffnorm.Period.Id;
                this.MeasureId = stuffnorm.Measure.Id;
                this.PeriodId = stuffnorm.Period.Id;
                this.DepartmentId = stuffnorm.Department.Id;

                this.ProjectId = stuffnorm.Project.Id;
            }
        }
    }
}
