using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    public class IndicatorTree_DTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long ParentId { get; set; }
        public virtual IndicatorType Type { get; set; }

        public List<IndicatorTree_DTO> Children { get; set; }

        public virtual Measure MeasureDefault { get; set; }
        public virtual Period PeriodDefault { get; set; }
        public virtual Plan PlanDefault { get; set; }

        public virtual  Project Project { get; set; }

        public IndicatorTree_DTO(Indicator indicator)
        {
            Id = indicator.Id;
            Name = indicator.Name;
            ParentId = indicator.Parent == null ? 0 : indicator.Parent.Id;
            Type = indicator.Type;
            MeasureDefault = indicator.MeasureDefault;
            PeriodDefault = indicator.PeriodDefault;
            PlanDefault = indicator.PlanDefault;
            Project = indicator.Project;
        }
    }
}
