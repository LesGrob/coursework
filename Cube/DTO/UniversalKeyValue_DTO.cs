using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    /// <summary>
    /// Объект DTO, для описания пары id-name
    /// </summary>
    public class UniversalKeyValue_DTO
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public UniversalKeyValue_DTO() { }

        public UniversalKeyValue_DTO(Indicator indicator)
        {
            Id = indicator.Id;
            Name = indicator.Name;
        }

        public UniversalKeyValue_DTO(Plan plan)
        {
            Id = plan.Id;
            Name = plan.Name;
        }

        public UniversalKeyValue_DTO(Post post)
        {
            if(post == null)
            {
                Id = 0;
                Name = null;
            }
            else
            {
                Id = post.Id;
                Name = post.Name;
            }
        }

        public UniversalKeyValue_DTO(DataFact fact)
        {
            Id = fact.Id;
            Name = fact.ReportDate.Date.ToString();
        }
    }
}
