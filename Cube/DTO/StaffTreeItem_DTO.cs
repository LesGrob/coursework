using Cube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.DTO
{
    public class StaffTreeItem_DTO
    {
        public long Id { get; set; }
        public Guid UID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public DateTime DateOpen { get; set; }
        public DateTime? DateClose { get; set; }

        public long ParentId { get; set; }
        public string ParentUID { get; set; }
        //public List<StaffTreeItem_DTO> Children { get; set; }

        public UniversalType_DTO Type { get; set; }
        public Project Project { get; set; }

        public StaffTreeItem_DTO() { }

        public StaffTreeItem_DTO(Department department)
        {
            Id = department.Id;
            UID = Guid.NewGuid();
            Name = department.Name;
            Code = "Department";
            ParentId = department.Parent == null ? 0 : department.Parent.Id;

            DateOpen = department.DateStart.Date;
            if (department.DateEnd != null)
                DateClose = department.DateEnd.Value.Date;

            Type = new UniversalType_DTO( department.Type );
            Project = department.Project;
        }

        public StaffTreeItem_DTO(Post post)
        {
            Id = post.Id;
            UID = Guid.NewGuid();
            Name = post.Name;
            Code = "Post";
            ParentId = post.Department == null ? 0 : post.Department.Id;

            DateOpen = post.DateOpen.Date;
            if (post.DateClose != null)
                DateClose = post.DateClose.Value.Date;

            Type = new UniversalType_DTO( post.Type );
            Project = post.Project;
        }

        public StaffTreeItem_DTO(EmployeePost employeePost)
        {
            Id = employeePost.Id;
            UID = Guid.NewGuid();
            Name = employeePost.Employee.Name + " " + employeePost.Employee.Surname + " " + employeePost.Employee.Middlename;
            Code = "Employee";
            ParentId = employeePost.Post == null ? 0 : employeePost.Post.Id;

            DateOpen = employeePost.DateOpen.Date;
            if (employeePost.DateClose != null)
                DateClose = employeePost.DateClose.Value.Date;

            Type = new UniversalType_DTO() { Id = employeePost.Id, Value = employeePost.Unit };
            Project = employeePost.Employee.Project;
        }
    }
}
