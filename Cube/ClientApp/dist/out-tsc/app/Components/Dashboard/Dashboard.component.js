var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { BehaviorSubject } from "rxjs";
import { Dialog } from '@app/Templates/Dialogs/Dialog';
import { ChangeIndicatorDialog } from '@app/Templates/Dialogs/DialogChangeIndicator/DialogChangeIndicator';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
import { map } from 'rxjs/operators';
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(dialogConfirm, dialogShelf, dialogShelfIndicators, dialog, dashboardRepository) {
        this.dialogConfirm = dialogConfirm;
        this.dialogShelf = dialogShelf;
        this.dialogShelfIndicators = dialogShelfIndicators;
        this.dialog = dialog;
        this.dashboardRepository = dashboardRepository;
        this.dataLoader = new BehaviorSubject(false);
        this.shelfName = new FormControl('', [Validators.required]);
        this.tempShelfData = null;
        this.indicatorControl = new FormControl('', [Validators.required]);
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dashboardRepository.httpService.getData('Indicator', 'Shelves', [this.dashboardRepository.ProjectId]).subscribe(function (data) {
            _this.Shelves = data;
            _this.dataLoader.next(true);
        });
    };
    DashboardComponent.prototype.RemoveIndicator = function (dashId) {
        var _this = this;
        this.dashboardRepository.httpService.postData('Indicator', 'RemoveShelfIndicator' + '/' + dashId, {}).subscribe(function (data) {
            if (data) {
                var shelf = _this.Shelves.filter(function (x) { return x.indicators && x.indicators.filter(function (i) { return i.userShelfId == dashId; }).length > 0; })[0];
                if (shelf)
                    shelf.indicators.splice(shelf.indicators.findIndex(function (i) { return i.userShelfId == dashId; }), 1);
            }
        });
    };
    DashboardComponent.prototype.onDepartmentsLoad = function (dash) {
        if (dash.id && dash.measureDefault && dash.periodDefault && dash.dataFact && dash.dataFact.department && dash.dataFact.reportDate) {
            var shelf_1 = this.Shelves.filter(function (x) { return x.indicators.includes(dash); })[0];
            if (shelf_1) {
                var date = new Date(dash.dataFact.reportDate);
                this.dashboardRepository.httpService.getData("Indicator", "IndicatorByDepartment", [dash.id, date.toDateString(), dash.measureDefault.id, dash.periodDefault.id, dash.dataFact.department.id, shelf_1.id]).subscribe(function (data) {
                    var _a;
                    (_a = shelf_1.indicators).push.apply(_a, data);
                });
            }
        }
    };
    DashboardComponent.prototype.onDriversLoad = function (dash) {
        if (dash.id && dash.measureDefault && dash.periodDefault && dash.dataFact && dash.dataFact.department && dash.dataFact.reportDate) {
            var shelf_2 = this.Shelves.filter(function (x) { return x.indicators.includes(dash); })[0];
            if (shelf_2) {
                var date = new Date(dash.dataFact.reportDate);
                this.dashboardRepository.httpService.getData("Indicator", "IndicatorByParent", [dash.id, date.toDateString(), dash.dataFact.department.id, shelf_2.id]).subscribe(function (data) {
                    var _a;
                    (_a = shelf_2.indicators).push.apply(_a, data);
                });
            }
        }
    };
    DashboardComponent.prototype.onIndicatorChange = function (dash) {
        var _this = this;
        if (dash.remove) {
            this.RemoveIndicator(dash.dash.id);
            return;
        }
        dash = dash.dash;
        var dialogRef = this.dialog.open(ChangeIndicatorDialog, {
            width: '600px',
            data: { dash: dash }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result) {
                var shelf_3 = _this.Shelves.filter(function (x) { return x.indicators.includes(dash); })[0];
                if (result.remove) {
                    _this.RemoveIndicator(dash.userShelfId);
                }
                else if (shelf_3 && result.Period && result.Measure && result.Department && result.Date) {
                    var date = new Date(result.Date);
                    date.setDate(date.getDate() + 1);
                    _this.dashboardRepository.httpService.postData('Indicator', 'UserShelfChangeData', {
                        Id: dash.userShelfId,
                        Shelf: { Id: shelf_3.id },
                        Indicator: { Id: dash.id },
                        Measure: { Id: result.Measure.id },
                        Period: { Id: result.Period.id },
                        Department: { Id: result.Department.id },
                        Employee: { Id: result.Employee.id },
                        Date: date.toUTCString()
                    }).pipe(map(function (data) { return data; })).subscribe(function (data) {
                        if (data) {
                            var index = shelf_3.indicators.findIndex(function (i) { return i.userShelfId == data.userShelfId; });
                            if (index)
                                shelf_3.indicators[index] = data;
                        }
                    });
                }
            }
        });
    };
    DashboardComponent.prototype.removeShelf = function (shelfId) {
        var _this = this;
        if (shelfId)
            this.dialogConfirm.openDialog("Полка", 'Вы желаете удалить полку?', true).subscribe(function (responce) {
                if (responce)
                    _this.dashboardRepository.httpService.postData('Indicator', 'RemoveShelf' + '/' + shelfId, {}).subscribe(function (data) {
                        var shelf = _this.Shelves.filter(function (x) { return x.id == shelfId; })[0];
                        if (shelf)
                            _this.Shelves.splice(_this.Shelves.indexOf(shelf, 0), 1);
                    });
            });
    };
    DashboardComponent.prototype.openShelfDialog = function (shelf) {
        var _this = this;
        this.tempShelfData = shelf ? shelf : null;
        this.shelfName.setValue(shelf ? shelf.name : '');
        this.dialogShelf.openDialogTemplate(this.DShelf).subscribe(function (result) {
            if (result) {
                var params = _this.dashboardRepository.ProjectId + '/' + _this.shelfName.value + '/' + (shelf ? shelf.id : 0);
                _this.dashboardRepository.httpService.postData('Indicator', 'Shelf/' + params, {})
                    .pipe(map(function (data) { return data; })).subscribe(function (data) {
                    if (shelf) {
                        var shelf_4 = _this.Shelves.filter(function (x) { return x.id == data.id; })[0];
                        if (shelf_4)
                            shelf_4.name = data.name;
                    }
                    else {
                        _this.Shelves.push(data);
                    }
                });
            }
        });
    };
    DashboardComponent.prototype.openIndicatorDialog = function (_shelfId) {
        var _this = this;
        this.dashboardRepository.httpService.getData('Indicator', 'FreeIndicators', [this.dashboardRepository.ProjectId]).subscribe(function (data) {
            _this.Indicators = data;
            _this.dialogShelfIndicators.openDialogTemplate(_this.DShelfIndicators).subscribe(function (result) {
                if (result)
                    _this.dashboardRepository.httpService.postData('Indicator', 'ShelfIndicator/' + _shelfId + '/' + _this.indicatorControl.value, {}).pipe(map(function (data) { return data; })).subscribe(function (data) {
                        var shelf = _this.Shelves.filter(function (x) { return x.id == _shelfId; })[0];
                        if (shelf) {
                            if (shelf.indicators)
                                shelf.indicators.push(data);
                            else
                                shelf.indicators = [data];
                        }
                    });
                _this.indicatorControl.setValue(null);
            });
        });
    };
    __decorate([
        ViewChild('DShelf'),
        __metadata("design:type", TemplateRef)
    ], DashboardComponent.prototype, "DShelf", void 0);
    __decorate([
        ViewChild('DShelfIndicators'),
        __metadata("design:type", TemplateRef)
    ], DashboardComponent.prototype, "DShelfIndicators", void 0);
    DashboardComponent = __decorate([
        Component({
            selector: 'Dashboard',
            templateUrl: './Dashboard.component.html',
            styleUrls: ['./Dashboard.component.css']
        }),
        __metadata("design:paramtypes", [Dialog, Dialog, Dialog,
            MatDialog, DashboardRepository])
    ], DashboardComponent);
    return DashboardComponent;
}());
export { DashboardComponent };
//# sourceMappingURL=Dashboard.component.js.map