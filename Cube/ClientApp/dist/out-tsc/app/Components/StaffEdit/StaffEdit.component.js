var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { BehaviorSubject } from "rxjs";
import { map } from "rxjs/operators";
import { StaffEditDialog } from '@app/Templates/Dialogs/DialogStaffEdit/DialogStaffEdit';
import { ActivatedRoute } from '@angular/router';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
var StaffEditComponent = /** @class */ (function () {
    function StaffEditComponent(dashboardRepository, dialog, snackBar, route) {
        var _this = this;
        this.dashboardRepository = dashboardRepository;
        this.dialog = dialog;
        this.snackBar = snackBar;
        this.route = route;
        this.dataLoader = new BehaviorSubject(null);
        this.dashboardRepository.httpService.getData('Staff', 'StaffData', [this.dashboardRepository.ProjectId]).pipe(map(function (data) { return data; })).subscribe(function (data) { _this.StaffData = data; });
    }
    StaffEditComponent.prototype.ChangeItem = function (item) {
        var _this = this;
        var parents = Array();
        switch (item.code) {
            case "Department":
                parents = this.StaffData.filter(function (x) { return x.code == "Department" && (item.uid ? x.uid != item.uid && !_this.IsUnderDepLeaf(item.uid, x.uid) : true); }).sort(this.compare);
                break;
            case "Post":
                parents = this.StaffData.filter(function (x) { return x.code == "Department"; }).sort(this.compare);
                break;
            case "Employee":
                parents = this.StaffData.filter(function (x) { return x.code != "Employee"; }).sort(this.compare);
                break;
        }
        var dialogRef = this.dialog.open(StaffEditDialog, {
            width: '600px',
            data: { item: item, parents: parents }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result && result.StaffItem) {
                if (result.StaffItem.dateOpen)
                    result.StaffItem.dateOpen = _this.SetDate(result.StaffItem.dateOpen, 1);
                if (result.StaffItem.dateClose)
                    result.StaffItem.dateClose = _this.SetDate(result.StaffItem.dateClose, 1);
                var parent_1 = _this.StaffData.find(function (x) { return x.uid == result.StaffItem.parentUID; });
                if (parent_1)
                    result.StaffItem.parentId = parent_1.id;
                if (result.action == "edit") {
                    _this.dashboardRepository.httpService.postData("Staff", "ChangeStaffItem", result.StaffItem).subscribe(function (data) {
                        if (data) {
                            var index = _this.StaffData.findIndex(function (x) { return x.uid == item.uid; });
                            if (index) {
                                if (result.StaffItem.dateOpen)
                                    result.StaffItem.dateOpen = _this.SetDate(result.StaffItem.dateOpen, -1);
                                if (result.StaffItem.dateClose)
                                    result.StaffItem.dateClose = _this.SetDate(result.StaffItem.dateClose, -1);
                                _this.StaffData[index] = result.StaffItem;
                                _this.StaffGrid.instance.refresh();
                            }
                        }
                    });
                }
                else if (result.action == "add") {
                    _this.dashboardRepository.httpService.postData("Staff", "AddStaffItem", {
                        Name: result.StaffItem.name,
                        Code: result.StaffItem.code,
                        DateOpen: result.StaffItem.dateOpen,
                        DateClose: result.StaffItem.dateClose,
                        ParentId: result.StaffItem.parentId,
                        Type: { Id: result.StaffItem.type.id, Value: result.StaffItem.type.value },
                        Project: { Id: result.StaffItem.project.id },
                    }).pipe(map(function (data) { return data; })).subscribe(function (data) {
                        if (data) {
                            var dataParent = void 0;
                            if (data.code == "Employee")
                                dataParent = _this.StaffData.find(function (x) { return x.id == data.parentId && x.code == "Post"; });
                            else
                                dataParent = _this.StaffData.find(function (x) { return x.id == data.parentId && x.code == "Department"; });
                            if (dataParent)
                                data.parentUID = dataParent.uid;
                            _this.StaffData.push(data);
                            _this.StaffGrid.instance.refresh();
                        }
                    });
                }
            }
        });
    };
    StaffEditComponent.prototype.compare = function (a, b) {
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    };
    //find if item is under itself
    StaffEditComponent.prototype.IsUnderDepLeaf = function (parentUID, itemUID) {
        var result = false;
        for (var _i = 0, _a = this.StaffData.filter(function (x) { return x.parentUID == parentUID; }); _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.uid == itemUID)
                return true;
            result = this.IsUnderDepLeaf(item.uid, itemUID);
            if (result)
                return true;
        }
        return result;
    };
    StaffEditComponent.prototype.SetDate = function (date, days) {
        date = new Date(date);
        date.setDate(date.getDate() + days);
        return date;
    };
    __decorate([
        ViewChild('StaffGrid'),
        __metadata("design:type", Object)
    ], StaffEditComponent.prototype, "StaffGrid", void 0);
    StaffEditComponent = __decorate([
        Component({
            selector: 'StaffEdit',
            templateUrl: './StaffEdit.component.html',
            styleUrls: ['./StaffEdit.component.css']
        }),
        __metadata("design:paramtypes", [DashboardRepository, MatDialog, MatSnackBar, ActivatedRoute])
    ], StaffEditComponent);
    return StaffEditComponent;
}());
export { StaffEditComponent };
//# sourceMappingURL=StaffEdit.component.js.map