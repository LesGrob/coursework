var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { map } from "rxjs/operators";
import { BottomSheet } from '@app/Templates/BottomSheets/BottomSheet';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
var ProjectsListComponent = /** @class */ (function () {
    function ProjectsListComponent(router, route, dashboardRepository, location, projectBS) {
        this.router = router;
        this.route = route;
        this.dashboardRepository = dashboardRepository;
        this.location = location;
        this.projectBS = projectBS;
    }
    ProjectsListComponent.prototype.SelectProject = function (project) {
        this.router.navigate(['/Project', project.id, 'Table'], {});
    };
    ProjectsListComponent.prototype.isLinkActive = function (projectId) { return this.dashboardRepository.ProjectId == projectId; };
    ProjectsListComponent.prototype.openProjectDialog = function () {
        this.projectBS.openDialog(this.BSProjectTemplate);
    };
    ProjectsListComponent.prototype.AddProject = function (projectName) {
        var _this = this;
        this.dashboardRepository.httpService.postData("Projects", "Project", { name: projectName }).pipe(map(function (data) { return data; })).subscribe(function (data) {
            _this.dashboardRepository.Projects$.value.push(data);
            _this.SelectProject(data);
            _this.projectBS.dismiss();
        });
    };
    __decorate([
        ViewChild('BSProjectTemplate'),
        __metadata("design:type", TemplateRef)
    ], ProjectsListComponent.prototype, "BSProjectTemplate", void 0);
    ProjectsListComponent = __decorate([
        Component({
            selector: 'ProjectsList',
            templateUrl: './ProjectsList.component.html',
            styleUrls: ['./ProjectsList.component.css']
        }),
        __metadata("design:paramtypes", [Router, ActivatedRoute, DashboardRepository,
            Location, BottomSheet])
    ], ProjectsListComponent);
    return ProjectsListComponent;
}());
export { ProjectsListComponent };
//# sourceMappingURL=ProjectsList.component.js.map