var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
import { MatDialog, MatSnackBar } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { Dialog } from '@app/Templates/Dialogs//Dialog';
import { NodeIndicatorDialog } from '@app/Templates/Dialogs/DialogNodeIndicator/DialogNodeIndicator';
import { CalculateIndicatorDialog } from '@app/Templates/Dialogs/DialogCalculateIndicator/DialogCalculateIndicator';
import { AddIndicatorDialog } from '@app/Templates/Dialogs/DialogAddIndicator/DialogAddIndicator';
import { filter, map } from "rxjs/operators";
var IndicatorsTreeComponent = /** @class */ (function () {
    function IndicatorsTreeComponent(dialogConfirm, dashboardRepository, dialog, snackBar) {
        this.dialogConfirm = dialogConfirm;
        this.dashboardRepository = dashboardRepository;
        this.dialog = dialog;
        this.snackBar = snackBar;
        this.dataLoader = new BehaviorSubject(false);
    }
    IndicatorsTreeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dashboardRepository.getData(this.dashboardRepository.ProjectId).subscribe(function (result) {
            if (result)
                _this.dataLoader.next(true);
        });
    };
    IndicatorsTreeComponent.prototype.ChangeIndicator = function (dash) {
        var _this = this;
        var dialogRef = this.dialog.open(NodeIndicatorDialog, {
            width: '600px',
            data: dash
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result) {
                if (result.removeId) {
                    //Remove indicatror
                    _this.dialogConfirm.openDialog("Показатель", "Вы действительно желаете удалить показатель '" + dash.name + "'", true).subscribe(function (data) {
                        if (data)
                            _this.dashboardRepository.httpService.postData("Indicator", "RemoveIndicator/" + result.removeId, {}).pipe(map(function (data) { return data; })).subscribe(function (data) {
                                if (data) {
                                    for (var _i = 0, _a = _this.dashboardRepository.Indicators.filter(function (x) { return x.parent && x.parent.id == data.id; }); _i < _a.length; _i++) {
                                        var child = _a[_i];
                                        child.parent = data.parent;
                                    }
                                    var index = _this.dashboardRepository.Indicators.findIndex(function (x) { return x.id == data.id; });
                                    if (index)
                                        _this.dashboardRepository.Indicators.splice(index, 1);
                                }
                                _this.IndicatorsGrid.instance.refresh();
                            });
                    });
                }
                else {
                    //Change indicator default values
                    _this.dashboardRepository.httpService.postData("Indicator", "IndicatorDefaults", {
                        Id: dash.id,
                        Parent: { Id: result.parent.id },
                        MeasureDefault: { Id: result.measure.id },
                        PeriodDefault: { Id: result.period.id },
                        PlanDefault: { Id: result.plan.id },
                        Type: { Id: result.type.id },
                        Owner: { Id: result.owner.id }
                    }).pipe(map(function (data) { return data; })).subscribe(function (data) {
                        var indicator = _this.dashboardRepository.Indicators.find(function (x) { return x.id == data.id; });
                        if (indicator) {
                            indicator.parent = data.parent;
                            indicator.measureDefault = data.measureDefault;
                            indicator.periodDefault = data.periodDefault;
                            indicator.planDefault = data.planDefault;
                            indicator.type = data.type;
                            indicator.owner = data.owner;
                            _this.IndicatorsGrid.instance.refresh();
                            _this.snackBar.open("Показатель изменен", "ОК", { duration: 2000 });
                        }
                    });
                }
            }
        });
    };
    IndicatorsTreeComponent.prototype.CalculateIndicator = function (dash) {
        var _this = this;
        var dialogRef = this.dialog.open(CalculateIndicatorDialog, {
            width: '600px',
            data: dash
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result && result.date && result.department.id != 0 && result.measure.id != 0
                && result.plan.id != 0 && result.period.id != 0) {
                var date = new Date(result.date);
                var params = "/" + dash.id + "/" + result.recursive + "/" + result.measure.id + "/" +
                    result.period.id + "/" + result.plan.id + "/" + dash.type.id + "/" + result.department.id + "/" + date.toUTCString();
                _this.dashboardRepository.httpService.postData("Indicator", "CalculateIndicator" + params, {}).pipe(filter(function (data) { return data != null; }), map(function (data) { return data; })).subscribe(function (data) { _this.snackBar.open("Показатель рассчитан", "ОК", { duration: 2000 }); });
            }
        });
    };
    //Modal to Add Indicator
    IndicatorsTreeComponent.prototype.openIndicatorDialog = function () {
        var _this = this;
        if (this.dataLoader.value) {
            var dialogRef = this.dialog.open(AddIndicatorDialog, {
                width: '600px',
                data: { ProjectId: this.dashboardRepository.ProjectId }
            });
            dialogRef.afterClosed().subscribe(function (result) {
                if (result && result.correct && result.indicator) {
                    _this.dashboardRepository.httpService.postData('Indicator', 'Indicator', result.indicator).subscribe(function (data) {
                        _this.dashboardRepository.Indicators.push(data);
                        _this.snackBar.open("Показатель добавлен", "ОК", { duration: 2000 });
                    });
                }
            });
        }
    };
    __decorate([
        ViewChild('IndicatorsGrid'),
        __metadata("design:type", Object)
    ], IndicatorsTreeComponent.prototype, "IndicatorsGrid", void 0);
    IndicatorsTreeComponent = __decorate([
        Component({
            selector: 'IndicatorsTree',
            templateUrl: './IndicatorsTree.component.html',
            styleUrls: ['./IndicatorsTree.component.css']
        }),
        __metadata("design:paramtypes", [Dialog, DashboardRepository, MatDialog, MatSnackBar])
    ], IndicatorsTreeComponent);
    return IndicatorsTreeComponent;
}());
export { IndicatorsTreeComponent };
//# sourceMappingURL=IndicatorsTree.component.js.map