var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewContainerRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
var IndexComponent = /** @class */ (function () {
    function IndexComponent(router, route, dashboardRepository) {
        var _this = this;
        this.router = router;
        this.route = route;
        this.dashboardRepository = dashboardRepository;
        this.dashboardRepository.getProjects();
        this.routeSubscribtion = route.params.subscribe(function (params) { _this.dashboardRepository.ProjectId = params.id; });
    }
    IndexComponent.prototype.ngOnDestroy = function () { this.routeSubscribtion.unsubscribe(); };
    __decorate([
        ViewChild('LeftBarTemplateContainer', { read: ViewContainerRef }),
        __metadata("design:type", Object)
    ], IndexComponent.prototype, "LeftBarTemplateContainer", void 0);
    IndexComponent = __decorate([
        Component({
            selector: 'index-component',
            templateUrl: './index.component.html',
            styleUrls: ['./index.component.css']
        }),
        __metadata("design:paramtypes", [Router, ActivatedRoute, DashboardRepository])
    ], IndexComponent);
    return IndexComponent;
}());
export { IndexComponent };
//# sourceMappingURL=index.component.js.map