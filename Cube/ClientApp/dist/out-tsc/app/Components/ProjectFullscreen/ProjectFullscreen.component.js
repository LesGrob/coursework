var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, TemplateRef } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { BottomSheet } from '@app/Templates/BottomSheets/BottomSheet';
import { map } from "rxjs/operators";
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
var ProjectFullscreenComponent = /** @class */ (function () {
    function ProjectFullscreenComponent(dashboardRepository, dialog, snackBar, projectBS) {
        var _this = this;
        this.dashboardRepository = dashboardRepository;
        this.dialog = dialog;
        this.snackBar = snackBar;
        this.projectBS = projectBS;
        this.Project = null;
        this.cubeData = {
            indicator: null,
            plan: null,
            measure: null,
            period: null,
            date: null
        };
        this.employeeTableData = {
            indicator: null,
            department: null,
            measure: null,
            period: null,
            date: null
        };
        this.dashboardRepository.getData(this.dashboardRepository.ProjectId).subscribe(function (data) {
            if (data)
                _this.dashboardRepository.httpService.getData('Projects', 'Project', [_this.dashboardRepository.ProjectId]).subscribe(function (data) {
                    _this.Project = data;
                });
        });
    }
    ProjectFullscreenComponent.prototype.ShowResults = function () {
        var _this = this;
        if (this.cubeData.date && this.cubeData.indicator && this.cubeData.plan && this.cubeData.measure && this.cubeData.period)
            this.dashboardRepository.httpService.getData('Indicator', 'DepartmentTableData', [this.cubeData.date.toUTCString(), this.Project.id, this.cubeData.indicator, this.cubeData.plan, this.cubeData.measure, this.cubeData.period])
                .subscribe(function (data) {
                _this.departmentsData = data;
            });
    };
    ProjectFullscreenComponent.prototype.ShowEmployeeResults = function () {
        var _this = this;
        if (this.employeeTableData.department && this.employeeTableData.date && this.employeeTableData.indicator && this.employeeTableData.measure && this.employeeTableData.period)
            this.dashboardRepository.httpService.getData('Indicator', 'EmployeeTableData', [this.employeeTableData.date.toUTCString(), this.Project.id, this.employeeTableData.indicator, this.employeeTableData.department, this.employeeTableData.measure, this.employeeTableData.period])
                .subscribe(function (data) {
                _this.employeeData = data;
            });
    };
    ProjectFullscreenComponent.prototype.onEmployeeRowUpdated = function ($event) {
        this.dashboardRepository.httpService.postData("Indicator", "EmployeePlanFactData", [{
                Employee: { Id: $event.oldData.id },
                DepartmentId: this.employeeTableData.department,
                Value: $event.newData.dataEmployee ? $event.newData.dataEmployee.value : 0,
                StuffNormValue: $event.newData.stuffNorm ? $event.newData.stuffNorm.value : 0,
                IndicatorId: this.employeeTableData.indicator,
                MeasureId: this.employeeTableData.measure,
                PeriodId: this.employeeTableData.period,
                ReportDate: this.employeeTableData.date.toUTCString()
            }]).subscribe(function (responce) { }, function (error) { console.log(error); });
    };
    ProjectFullscreenComponent.prototype.onRowUpdated = function ($event) {
        this.dashboardRepository.httpService.postData("Indicator", "PlanFactData", [{
                DepartmentId: $event.oldData.id,
                FactValue: ($event.newData.dataFact != undefined ? $event.newData.dataFact.value : 0),
                PlanValue: ($event.newData.dataPlan != undefined ? $event.newData.dataPlan.value : 0),
                IndicatorId: this.cubeData.indicator,
                PlanId: this.cubeData.plan,
                MeasureId: this.cubeData.measure,
                PeriodId: this.cubeData.period,
                ReportDate: this.cubeData.date
            }]).subscribe(function (responce) { }, function (error) { console.log(error); });
    };
    ProjectFullscreenComponent.prototype.openProjectDialog = function () {
        this.projectBS.openDialog(this.BSProjectTemplate);
    };
    ProjectFullscreenComponent.prototype.ChangeProjectName = function (projectName) {
        var _this = this;
        if (projectName != "" && this.Project.id)
            this.dashboardRepository.httpService.postData("Projects", "RenameProject", { id: this.Project.id, name: projectName }).pipe(map(function (data) { return data; })).subscribe(function (data) {
                _this.Project = data;
                var index = _this.dashboardRepository.Projects$.value.findIndex(function (x) { return x.id == data.id; });
                if (index > -1)
                    _this.dashboardRepository.Projects$.value[index].name = data.name;
                _this.projectBS.dismiss();
            });
    };
    __decorate([
        ViewChild('BSProjectTemplate'),
        __metadata("design:type", TemplateRef)
    ], ProjectFullscreenComponent.prototype, "BSProjectTemplate", void 0);
    __decorate([
        ViewChild('BSAccessTemplate'),
        __metadata("design:type", TemplateRef)
    ], ProjectFullscreenComponent.prototype, "BSAccessTemplate", void 0);
    ProjectFullscreenComponent = __decorate([
        Component({
            selector: 'ProjectFullscreen',
            templateUrl: './ProjectFullscreen.component.html',
            styleUrls: ['./ProjectFullscreen.component.css']
        }),
        __metadata("design:paramtypes", [DashboardRepository, MatDialog,
            MatSnackBar, BottomSheet])
    ], ProjectFullscreenComponent);
    return ProjectFullscreenComponent;
}());
export { ProjectFullscreenComponent };
//# sourceMappingURL=ProjectFullscreen.component.js.map