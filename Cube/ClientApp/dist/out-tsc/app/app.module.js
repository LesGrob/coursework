var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { BrowserModule, NgModule, FormsModule, FlexLayoutModule, ReactiveFormsModule, BrowserAnimationsModule, AngularSplitModule, DragulaModule, DxDataGridModule, DxContextMenuModule, DxPieChartModule, DxTreeListModule, DxChartModule, MatAutocompleteModule, MatToolbarModule, MatBottomSheetModule, MatBadgeModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatExpansionModule, MatIconModule, MatSnackBarModule, MatProgressBarModule, MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule, MatSelectModule, MatSidenavModule, MatTabsModule, } from "@app/BarrelFile";
import { HttpClientModule, DateAdapter, HttpService, DashboardRepository, AddIndicatorDialog, ChangeIndicatorDialog, NodeIndicatorDialog, CalculateIndicatorDialog, Dialog, DialogConfirmComponent, DialogInfoComponent, DialogComponent, ProjectsListComponent, IndicatorsTreeComponent, AppComponent, DashboardComponent, StaffEditDialog, ProjectFullscreenComponent, StaffEditComponent, IndexComponent, StaffPostFilter, DashboardTemplateComponent, BottomSheetComponent, BottomSheet, } from "@app/BarrelFile";
import 'hammerjs';
import { AppRoutingModule } from "@app/app.routing.module";
var AppModule = /** @class */ (function () {
    function AppModule(dateAdapter) {
        this.dateAdapter = dateAdapter;
        this.dateAdapter.setLocale('ru-RU');
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                BottomSheetComponent,
                ProjectsListComponent, AppComponent, DashboardComponent,
                ProjectFullscreenComponent, StaffEditComponent,
                IndicatorsTreeComponent, IndexComponent,
                DashboardTemplateComponent,
                NodeIndicatorDialog, DialogConfirmComponent, DialogInfoComponent,
                DialogComponent, AddIndicatorDialog, ChangeIndicatorDialog,
                CalculateIndicatorDialog, StaffEditDialog,
                StaffPostFilter
            ],
            imports: [
                AppRoutingModule, BrowserAnimationsModule, FormsModule, ReactiveFormsModule, HttpClientModule,
                BrowserModule, FormsModule,
                AngularSplitModule, DragulaModule.forRoot(), FlexLayoutModule,
                DxContextMenuModule, DxChartModule, DxPieChartModule, DxDataGridModule, DxTreeListModule,
                MatBottomSheetModule, MatAutocompleteModule, MatButtonModule, MatButtonToggleModule,
                MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatTabsModule,
                MatDialogModule, MatExpansionModule, MatIconModule, MatInputModule, MatSnackBarModule,
                MatListModule, MatMenuModule, MatNativeDateModule, MatSelectModule, MatBadgeModule,
                MatProgressBarModule, MatSidenavModule, MatToolbarModule
            ],
            bootstrap: [AppComponent],
            providers: [
                BottomSheet, HttpClientModule, HttpService, Dialog, DashboardRepository
            ],
            entryComponents: [
                BottomSheetComponent, DialogConfirmComponent, DialogInfoComponent, DialogComponent,
                AddIndicatorDialog, ChangeIndicatorDialog, NodeIndicatorDialog,
                StaffEditDialog, CalculateIndicatorDialog
            ]
        }),
        __metadata("design:paramtypes", [DateAdapter])
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map