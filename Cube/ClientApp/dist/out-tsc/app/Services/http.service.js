var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { throwError as observableThrowError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import * as FileSaver from 'file-saver';
import { Dialog } from "@app/Templates/Dialogs/Dialog";
var Task = /** @class */ (function () {
    function Task() {
    }
    return Task;
}());
export { Task };
var HttpService = /** @class */ (function () {
    function HttpService(router, http, dialog) {
        this.router = router;
        this.http = http;
        this.dialog = dialog;
    }
    HttpService.prototype.stringifyData = function (data) {
        var _data = "/";
        for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
            var item = data_1[_i];
            _data += (item + "/");
        }
        return _data;
    };
    //Временные экземпляры с JWT Tokens - полный перекат с прошлых на эти методы с JWT
    HttpService.prototype.postData = function (_controller, _method, data, parameters) {
        var _this = this;
        var headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post('/api/' + _controller + '/' + _method, data, { headers: this.JWTHeader(headers), params: parameters }).pipe(catchError(function (error) {
            return _this.catchError(error);
        }));
    };
    HttpService.prototype.getData = function (_controller, _method, _data, parameters) {
        var _this = this;
        return this.http.get('/api/' + _controller + '/' + _method + this.stringifyData(_data), { headers: this.JWTHeader(), params: this.HandleParameter(parameters) })
            .pipe(catchError(function (error) {
            return _this.catchError(error);
        }));
    };
    //Для выгрузки файла
    HttpService.prototype.postFile = function (_controller, _method, data) {
        var _this = this;
        return this.http.post('/api/' + _controller + '/' + _method, data, { headers: this.JWTHeader() })
            .pipe(catchError(function (error) {
            return _this.catchError(error);
        }));
    };
    HttpService.prototype.DownloadFile = function (file) {
        //Download File from server
        var headers = new HttpHeaders().set('Content-Type', 'application/json');
        headers = headers.append('MyApp-Application', 'AppName');
        headers = headers.append('Accept', 'application/pdf');
        this.http.post("/api/Files/Download/" + file.id, "", { headers: this.JWTHeader(headers), responseType: "blob" })
            .subscribe(function (data) {
            FileSaver.saveAs(data, file.name);
        });
    };
    HttpService.prototype.catchError = function (error) {
        if (error.status === 401 || error.status === 403) {
            this.router.navigate(['/LogIn']);
            return observableThrowError(new Error(error.status));
        }
        this.dialog.openDialog("Error " + error.status, error.error ? error.error.message : error.message);
        return;
    };
    //Добавление заголовка для аутентификации JWT Tokens
    HttpService.prototype.JWTHeader = function (headers) {
        //headers - Остальные заголовки, к которым нужно добавить заголовок аутентификации, [если есть]
        //create JWT header
        var token = localStorage.getItem("JWTtoken");
        if (headers) {
            headers = headers.append('Authorization', 'Bearer ' + token);
            return headers;
        }
        if (token) {
            return new HttpHeaders().set('Authorization', 'Bearer ' + token);
        }
    };
    //Формирование параметров для запроса
    HttpService.prototype.HandleParameter = function (parameters) {
        var params = new HttpParams();
        if (!parameters || parameters.length == 0)
            return;
        for (var _i = 0, parameters_1 = parameters; _i < parameters_1.length; _i++) {
            var parameter = parameters_1[_i];
            if (parameter)
                params = params.append(parameter.name, parameter.value);
        }
        return params;
    };
    HttpService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Router, HttpClient, Dialog])
    ], HttpService);
    return HttpService;
}());
export { HttpService };
//# sourceMappingURL=http.service.js.map