var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpService } from '@app/Services/http.service';
import { BehaviorSubject, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
var DashboardRepository = /** @class */ (function () {
    function DashboardRepository(httpService) {
        this.httpService = httpService;
        this.ProjectId = null;
        this.Projects$ = new BehaviorSubject(null);
        this.sortFunc = function (x, y) {
            if (x.name > y.name)
                return 1;
            if (x.name < y.name)
                return -1;
            return 0;
        };
    }
    DashboardRepository.prototype.getProjects = function () {
        var _this = this;
        this.httpService.getData("Projects", "Projects").pipe(map(function (data) { return data; })).subscribe(function (data) {
            _this.Projects$.next(data);
        });
    };
    DashboardRepository.prototype.getData = function (projectId) {
        var _this = this;
        var result = new BehaviorSubject(null);
        this.PlansObserver = this.httpService.getData('Plan', 'Plans', [projectId]);
        this.MeasuresObserver = this.httpService.getData('Measure', 'Measures', [projectId]);
        this.PeriodsObserver = this.httpService.getData('Period', 'Periods', [projectId]);
        this.IndicatorsObserver = this.httpService.getData('Indicator', 'Indicators', [projectId]);
        this.IndicatorTypesObserver = this.httpService.getData('Indicator', 'IndicatorTypes', [projectId]);
        this.DepartmentsObserver = this.httpService.getData('Department', 'Departments', [projectId]);
        forkJoin([this.PlansObserver, this.MeasuresObserver, this.PeriodsObserver, this.IndicatorsObserver, this.IndicatorTypesObserver, this.DepartmentsObserver]).subscribe(function (results) {
            _this.Plans = results[0].sort(_this.sortFunc);
            _this.Measures = results[1].sort(_this.sortFunc);
            _this.Periods = results[2].sort(_this.sortFunc);
            _this.Indicators = results[3].sort(_this.sortFunc);
            _this.IndicatorTypes = results[4].sort(_this.sortFunc);
            _this.Departments = results[5].sort(_this.sortFunc);
            result.next(true);
        });
        return result;
    };
    //Cleardata in all variables
    DashboardRepository.prototype.ClearAllData = function () {
        this.Indicators = null;
        this.Plans = null;
        this.Measures = null;
        this.Periods = null;
        this.Indicators = null;
        this.IndicatorTypes = null;
        this.Departments = null;
    };
    DashboardRepository = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpService])
    ], DashboardRepository);
    return DashboardRepository;
}());
export { DashboardRepository };
//# sourceMappingURL=dashboard.repository.js.map