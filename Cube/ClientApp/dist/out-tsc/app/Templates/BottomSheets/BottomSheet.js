var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject, Injectable } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheet, MatBottomSheetRef } from '@angular/material';
var BottomSheet = /** @class */ (function () {
    function BottomSheet(bottomSheet) {
        this.bottomSheet = bottomSheet;
    }
    BottomSheet.prototype.openDialog = function (Template) {
        this.bSheet = this.bottomSheet.open(BottomSheetComponent, {
            data: Template
        });
        return this.bSheet.afterDismissed();
    };
    BottomSheet.prototype.dismiss = function () {
        return this.bSheet.dismiss();
    };
    BottomSheet = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [MatBottomSheet])
    ], BottomSheet);
    return BottomSheet;
}());
export { BottomSheet };
var BottomSheetComponent = /** @class */ (function () {
    function BottomSheetComponent(bottomSheetRef, data) {
        this.bottomSheetRef = bottomSheetRef;
        this.data = data;
    }
    BottomSheetComponent = __decorate([
        Component({
            selector: 'bottom-sheet',
            template: "<template [ngTemplateOutlet]='data'></template>"
        }),
        __param(1, Inject(MAT_BOTTOM_SHEET_DATA)),
        __metadata("design:paramtypes", [MatBottomSheetRef, Object])
    ], BottomSheetComponent);
    return BottomSheetComponent;
}());
export { BottomSheetComponent };
//# sourceMappingURL=BottomSheet.js.map