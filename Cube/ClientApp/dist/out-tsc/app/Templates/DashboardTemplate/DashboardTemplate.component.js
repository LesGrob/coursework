var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { HttpService } from '@app/Services/http.service';
import { map } from "rxjs/operators";
import * as moment from 'moment';
var DashboardTemplateComponent = /** @class */ (function () {
    function DashboardTemplateComponent(httpService) {
        this.httpService = httpService;
        this.onChange = new EventEmitter();
        this.onDepartmentsLoad = new EventEmitter();
        this.onDriversLoad = new EventEmitter();
        this.PlanFactDates = [];
        this.Ratings = [];
    }
    ;
    DashboardTemplateComponent.prototype.ChangeIndicator = function (dash, remove) {
        this.onChange.emit({ dash: dash, remove: remove });
    };
    DashboardTemplateComponent.prototype.selectedTabChange = function ($event) {
        var _this = this;
        if (this.dash.dataFact)
            if ($event.index == 1) {
                var date = new Date(this.dash.dataFact.reportDate);
                this.httpService.getData('Indicator', 'DashboardChartData', [this.dash.id, date.toUTCString(), this.dash.dataFact.measure.id, this.dash.dataFact.period.id, this.dash.dataFact.department.id]).pipe(map(function (data) { return data; })).subscribe(function (data) {
                    _this.PlanFactDates = data;
                });
            }
            else if ($event.index == 2) {
                var date = new Date(this.dash.dataFact.reportDate);
                date.setDate(date.getDate() - 1);
                this.httpService.getData('Indicator', 'DashboardRatings', [this.dash.id, date.toUTCString(), this.dash.dataFact.measure.id, this.dash.dataFact.period.id, this.dash.dataFact.department.id]).pipe(map(function (data) { return data; })).subscribe(function (data) {
                    _this.Ratings = data;
                });
            }
    };
    DashboardTemplateComponent.prototype.customizeTooltip = function (arg) {
        return {
            text: moment(arg.argument).format("DD.MM.YYYY") + "<br/>" + Math.round(arg.value)
        };
    };
    DashboardTemplateComponent.prototype.GetDepartments = function () {
        this.onDepartmentsLoad.emit(this.dash);
    };
    DashboardTemplateComponent.prototype.GetDrivers = function () {
        this.onDriversLoad.emit(this.dash);
    };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], DashboardTemplateComponent.prototype, "dash", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], DashboardTemplateComponent.prototype, "onChange", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], DashboardTemplateComponent.prototype, "onDepartmentsLoad", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], DashboardTemplateComponent.prototype, "onDriversLoad", void 0);
    DashboardTemplateComponent = __decorate([
        Component({
            selector: 'DashboardTemplate',
            templateUrl: './DashboardTemplate.component.html',
            styleUrls: ['./DashboardTemplate.component.css']
        }),
        __metadata("design:paramtypes", [HttpService])
    ], DashboardTemplateComponent);
    return DashboardTemplateComponent;
}());
export { DashboardTemplateComponent };
//# sourceMappingURL=DashboardTemplate.component.js.map