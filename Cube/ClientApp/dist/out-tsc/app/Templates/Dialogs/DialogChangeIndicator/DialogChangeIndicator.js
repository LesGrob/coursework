var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
var ChangeIndicatorDialog = /** @class */ (function () {
    function ChangeIndicatorDialog(dashboardRepository, dialogRef, data) {
        this.dashboardRepository = dashboardRepository;
        this.dialogRef = dialogRef;
        this.data = data;
        this.dataLoader = new BehaviorSubject(false);
        this.myControl = new FormControl();
        this.Departments = [];
        this.Employees = [];
        this.Measures = [];
        this.Periods = [];
        this.Dates = [];
        this.Period = { id: 0 };
        this.Measure = { id: 0 };
        this.Department = { id: 0 };
        this.Employee = { id: 0 };
        this.Date = undefined;
        if (data && data.dash && data.dash.id) {
            if (data.dash.dataFact) {
                this.Period = data.dash.dataFact.period;
                this.Measure = data.dash.measureDefault;
                this.Department = data.dash.dataFact.department;
                this.Date = data.dash.dataFact.reportDate;
            }
            if (data.dash.dataEmployee && data.dash.dataEmployee.employee)
                this.Employee = data.dash.dataEmployee.employee;
            this.getRepository(data.dash.id, 'd', this.Department.id);
        }
    }
    ChangeIndicatorDialog.prototype.getRepository = function (indicatorId, type, value) {
        var _this = this;
        this.dashboardRepository.httpService.getData('Indicator', 'ShelfIndicatorRepository', [indicatorId, type, value]).pipe(map(function (data) { return data; })).subscribe(function (data) {
            _this.Departments = data.departments;
            _this.Measures = data.measures;
            _this.Periods = data.periods;
            _this.Dates = data.dates;
            _this.Employees = data.employees;
            var period = _this.Periods.filter(function (x) { return x.id == _this.Period.id; })[0];
            var measure = _this.Measures.filter(function (x) { return x.id == _this.Measure.id; })[0];
            var department = _this.Departments.filter(function (x) { return x.id == _this.Department.id; })[0];
            var employee = _this.Employees.filter(function (x) { return x.id == _this.Employee.id; })[0];
            _this.Date = _this.Dates.filter(function (x) { return x == _this.Date; })[0];
            _this.Period = period == undefined ? { id: 0 } : period;
            _this.Measure = measure == undefined ? { id: 0 } : measure;
            _this.Department = department == undefined ? { id: 0 } : department;
            _this.Employee = employee == undefined ? { id: 0 } : employee;
            _this.dataLoader.next(true);
        });
    };
    ChangeIndicatorDialog.prototype.selectChange = function ($event, type) {
        this.getRepository(this.data.dash.id, type, $event);
    };
    ChangeIndicatorDialog.prototype.dateChange = function ($event) {
        var date = new Date($event);
        this.getRepository(this.data.dash.id, 'date', date.toDateString());
    };
    ChangeIndicatorDialog.prototype.validation = function () { return (this.Period.id == 0 || this.Measure.id == 0 || this.Department.id == 0 || this.Date == undefined); };
    ChangeIndicatorDialog = __decorate([
        Component({
            selector: 'shelfIndicators-dialog',
            templateUrl: './DialogChangeIndicator.html'
        }),
        __param(2, Inject(MAT_DIALOG_DATA)),
        __metadata("design:paramtypes", [DashboardRepository, MatDialogRef, Object])
    ], ChangeIndicatorDialog);
    return ChangeIndicatorDialog;
}());
export { ChangeIndicatorDialog };
//# sourceMappingURL=DialogChangeIndicator.js.map