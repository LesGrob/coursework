var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject, Pipe } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
var StaffPostFilter = /** @class */ (function () {
    function StaffPostFilter() {
    }
    StaffPostFilter.prototype.transform = function (array, value) {
        return array.filter(function (x) { return x.parentUID == value && x.code == "Post"; });
    };
    StaffPostFilter = __decorate([
        Pipe({ name: 'staffPostFilter' })
    ], StaffPostFilter);
    return StaffPostFilter;
}());
export { StaffPostFilter };
var StaffEditDialog = /** @class */ (function () {
    function StaffEditDialog(dashboardRepository, dialogRef, data) {
        var _this = this;
        this.dashboardRepository = dashboardRepository;
        this.dialogRef = dialogRef;
        this.data = data;
        this.dataLoader = new BehaviorSubject(false);
        this.StaffItem = {
            id: 0,
            uid: "",
            name: "",
            code: "",
            parentId: 0,
            parentUID: "",
            dateOpen: null,
            dateClose: null,
            type: { id: 0, value: 1 },
            project: { id: 0 }
        };
        this.action = "add";
        this.unitCorrect = false;
        this.filter = new StaffPostFilter();
        this.nameControl = new FormControl('', [Validators.required, Validators.nullValidator]);
        this.parentControl = new FormControl('', [Validators.required, Validators.nullValidator]);
        this.unitControl = new FormControl(this.unitCorrect, [Validators.min(0.1), Validators.max(1)]);
        this.typeControl = new FormControl('', [Validators.required, Validators.nullValidator]);
        this.StaffItem.code = data.item.code;
        this.StaffItem.project.id = this.dashboardRepository.ProjectId;
        this.StaffParents = data.parents.filter(function (x) { return x.code == "Department"; });
        if (data && data.item.uid) {
            this.action = "edit";
            this.StaffItem = __assign({}, data.item);
            if (data.item.code == "Employee" && data.item.parentUID) {
                var post_1 = data.parents.find(function (x) { return x.uid == data.item.parentUID && x.code == 'Post'; });
                if (post_1) {
                    var dep = data.parents.find(function (x) { return x.uid == post_1.parentUID; });
                    if (dep)
                        this.postParent = dep.uid;
                }
                this.onUnitChange();
            }
        }
        if (this.StaffItem.code && this.StaffItem.code != "Employee")
            this.dashboardRepository.httpService.getData("Staff", "GetStaffTypes", [this.dashboardRepository.ProjectId, this.StaffItem.code]).pipe(map(function (data) { return data; })).subscribe(function (data) {
                _this.StaffTypes = data;
                _this.StaffItem.type = _this.StaffTypes[_this.StaffTypes.findIndex(function (x) { return x.id == _this.StaffItem.type.id; })];
                _this.dataLoader.next(true);
            });
        else
            this.dataLoader.next(true);
    }
    StaffEditDialog.prototype.Validation = function () {
        if (this.StaffItem.dateOpen && this.nameControl.valid && this.unitControl.valid && this.parentControl.valid
            && (this.StaffItem.code == "Employee" ? this.unitCorrect : this.typeControl.valid))
            return true;
        return false;
    };
    StaffEditDialog.prototype.onUnitChange = function () {
        var _this = this;
        var parent = this.data.parents.find(function (x) { return x.uid == _this.StaffItem.parentUID; });
        if (parent)
            this.dashboardRepository.httpService.getData('Staff', 'CheckEmployeeUnit', [this.StaffItem.id, parent.id, this.StaffItem.type.value]).subscribe(function (data) { _this.unitCorrect = data; });
        else
            this.unitCorrect = false;
    };
    StaffEditDialog.prototype.onDepartmentChange = function () {
        this.StaffItem.parentUID = null;
    };
    StaffEditDialog = __decorate([
        Component({
            selector: 'staffItem-dialog',
            templateUrl: './DialogStaffEdit.html'
        }),
        __param(2, Inject(MAT_DIALOG_DATA)),
        __metadata("design:paramtypes", [DashboardRepository, MatDialogRef, Object])
    ], StaffEditDialog);
    return StaffEditDialog;
}());
export { StaffEditDialog };
//# sourceMappingURL=DialogStaffEdit.js.map