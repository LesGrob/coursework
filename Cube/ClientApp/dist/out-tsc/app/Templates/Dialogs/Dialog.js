var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Injectable, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
var Dialog = /** @class */ (function () {
    function Dialog(dialog) {
        this.dialog = dialog;
    }
    Dialog.prototype.openDialog = function (label, description, confirm) {
        var component;
        if (confirm)
            component = DialogConfirmComponent;
        else
            component = DialogInfoComponent;
        var dialogRef = this.dialog.open(component, {
            width: '400px',
            data: { label: label, description: description }
        });
        return dialogRef.afterClosed();
    };
    ;
    Dialog.prototype.openDialogTemplate = function (Template, width) {
        var dialogRef = this.dialog.open(DialogComponent, {
            width: width ? width : '400px',
            data: Template
        });
        return dialogRef.afterClosed();
    };
    ;
    Dialog.prototype.close = function () { this.dialog.closeAll(); };
    Dialog = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [MatDialog])
    ], Dialog);
    return Dialog;
}());
export { Dialog };
//Modal 
var DialogInfoComponent = /** @class */ (function () {
    function DialogInfoComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    DialogInfoComponent = __decorate([
        Component({
            selector: 'dialog-error',
            template: "<h2 mat-dialog-title>{{data.label}}</h2>\
    <mat-dialog-content>{{data.description}}</mat-dialog-content>\
    <mat-dialog-actions>\
    <button mat-button mat-dialog-close color='accent' mat-dialog-close>OK</button>\
    </mat-dialog-actions>"
        }),
        __param(1, Inject(MAT_DIALOG_DATA)),
        __metadata("design:paramtypes", [MatDialogRef, Object])
    ], DialogInfoComponent);
    return DialogInfoComponent;
}());
export { DialogInfoComponent };
var DialogConfirmComponent = /** @class */ (function () {
    function DialogConfirmComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    DialogConfirmComponent = __decorate([
        Component({
            selector: 'dialog-confirm',
            template: "<h2 mat-dialog-title>{{data.label}}</h2>\
    <mat-dialog-content>{{data.description}}</mat-dialog-content>\
    <mat-dialog-actions>\
        <button mat-button mat-dialog-close color='accent' [mat-dialog-close]='true'>Подтвердить</button>\
        <button mat-button mat-dialog-close>Отмена</button>\
    </mat-dialog-actions>"
        }),
        __param(1, Inject(MAT_DIALOG_DATA)),
        __metadata("design:paramtypes", [MatDialogRef, Object])
    ], DialogConfirmComponent);
    return DialogConfirmComponent;
}());
export { DialogConfirmComponent };
var DialogComponent = /** @class */ (function () {
    function DialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    DialogComponent = __decorate([
        Component({
            selector: 'dialog-component',
            template: "<template [ngTemplateOutlet]='data'></template>"
        }),
        __param(1, Inject(MAT_DIALOG_DATA)),
        __metadata("design:paramtypes", [MatDialogRef, Object])
    ], DialogComponent);
    return DialogComponent;
}());
export { DialogComponent };
//# sourceMappingURL=Dialog.js.map