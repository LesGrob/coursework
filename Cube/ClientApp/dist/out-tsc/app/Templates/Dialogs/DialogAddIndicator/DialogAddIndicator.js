var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
var AddIndicatorDialog = /** @class */ (function () {
    function AddIndicatorDialog(dashboardRepository, dialogRef, data) {
        this.dashboardRepository = dashboardRepository;
        this.dialogRef = dialogRef;
        this.data = data;
        this.dataLoader = new BehaviorSubject(false);
        this.Indicator = {
            Id: 0,
            Code: "",
            Name: null,
            Type: null,
            Owner: null,
            Parent: { Id: 0 },
            MeasureDefault: null,
            PeriodDefault: null,
            PlanDefault: null,
            Project: { Id: this.dashboardRepository.ProjectId }
        };
        this.myControl = new FormControl();
        this.dataLoader.next(true);
    }
    AddIndicatorDialog.prototype.ownerValueChanged = function (event) {
        var _this = this;
        if (event.data)
            this.dashboardRepository.httpService.getData("Department", "AutocompleteDepartments", [event.data, this.data.ProjectId]).subscribe(function (res) {
                return _this.Departments = res;
            }, function (error) { console.log("Error", error); });
        else
            this.Departments = [];
    };
    ;
    AddIndicatorDialog.prototype.ownerSelectionChange = function (department) {
        if (department && department.id)
            this.Indicator.Owner = department;
    };
    AddIndicatorDialog.prototype.formValidation = function () {
        return (this.Indicator.Name != null && this.Indicator.Type != null && this.Indicator.Owner != null
            && this.Indicator.MeasureDefault != null && this.Indicator.PeriodDefault != null && this.Indicator.PlanDefault != null);
    };
    AddIndicatorDialog = __decorate([
        Component({
            selector: 'indicator-dialog',
            templateUrl: './DialogAddIndicator.html'
        }),
        __param(2, Inject(MAT_DIALOG_DATA)),
        __metadata("design:paramtypes", [DashboardRepository, MatDialogRef, Object])
    ], AddIndicatorDialog);
    return AddIndicatorDialog;
}());
export { AddIndicatorDialog };
//# sourceMappingURL=DialogAddIndicator.js.map