var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
var NodeIndicatorDialog = /** @class */ (function () {
    function NodeIndicatorDialog(dialogRef, dashboardRepository, dash) {
        var _this = this;
        this.dialogRef = dialogRef;
        this.dashboardRepository = dashboardRepository;
        this.dash = dash;
        this.Indicators = this.dashboardRepository.Indicators.filter(function (x) { return x.id != _this.dash.id; });
        this.RemoveChildren(dash.id);
        this.Indicators.push({ id: 0, name: "Без родителя" });
        this.parent = dash.parent ? Object.assign({}, dash.parent) : { id: 0 };
        this.measureDefault = Object.assign({}, dash.measureDefault);
        this.periodDefault = Object.assign({}, dash.periodDefault);
        this.planDefault = Object.assign({}, dash.planDefault);
        this.typeDefault = Object.assign({}, dash.type);
        this.owner = Object.assign({}, dash.owner);
    }
    NodeIndicatorDialog.prototype.RemoveChildren = function (dashId) {
        for (var _i = 0, _a = this.Indicators.filter(function (x) { return x.parent && x.parent.id == dashId; }); _i < _a.length; _i++) {
            var item = _a[_i];
            this.RemoveChildren(item.id);
            var index = this.Indicators.findIndex(function (x) { return x.id == item.id; });
            this.Indicators.splice(index, 1);
        }
    };
    NodeIndicatorDialog = __decorate([
        Component({
            selector: 'nodeIndicator-dialog',
            templateUrl: './DialogNodeIndicator.html'
        }),
        __param(2, Inject(MAT_DIALOG_DATA)),
        __metadata("design:paramtypes", [MatDialogRef,
            DashboardRepository, Object])
    ], NodeIndicatorDialog);
    return NodeIndicatorDialog;
}());
export { NodeIndicatorDialog };
//# sourceMappingURL=DialogNodeIndicator.js.map