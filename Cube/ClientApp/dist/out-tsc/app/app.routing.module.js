var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule, RouterModule, DashboardComponent, IndicatorsTreeComponent, StaffEditComponent, ProjectFullscreenComponent, IndexComponent } from "./BarrelFile";
//Main Routing Module
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot([
                    { path: '', redirectTo: '/Project/0/Dashboard', pathMatch: 'full' },
                    { path: 'Project', redirectTo: '/Project/0/Table', pathMatch: 'full' },
                    { path: 'Project/:id', redirectTo: '/Project/:id/Table', pathMatch: 'full' },
                    {
                        path: 'Project/:id', component: IndexComponent, children: [
                            { path: "Dashboard", component: DashboardComponent },
                            { path: "Staff", component: StaffEditComponent },
                            { path: "IndicatorsTree", component: IndicatorsTreeComponent },
                            { path: 'Info', component: ProjectFullscreenComponent }
                        ]
                    }
                ])],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app.routing.module.js.map