//Barrel File, which can give access to different projects and angular components
//Angular components and modules
export { Component, NgModule } from "@angular/core";
export { BrowserModule } from "@angular/platform-browser";
export { BrowserAnimationsModule } from "@angular/platform-browser/animations";
export { HttpClientModule } from '@angular/common/http';
export { FormsModule, FormControl, FormGroup, Validators, FormBuilder, ReactiveFormsModule } from "@angular/forms";
export { Routes, RouterModule } from "@angular/router";

export { DateAdapter } from '@angular/material';
export { FlexLayoutModule } from '@angular/flex-layout'
export { DxContextMenuModule, DxChartModule, DxDataGridModule, DxPieChartModule, DxTreeListModule } from 'devextreme-angular';
export { AngularSplitModule } from 'angular-split';
export { DragulaModule } from 'ng2-dragula';
export {
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatIconModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatSelectModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,
    MatBadgeModule,
    MatBottomSheetModule
} from "@angular/material";


//Custom components and modules
export * from "@app/app.component";
export * from "@app/Components/Dashboard/Dashboard.component";
export * from "@app/Components/ProjectsList/ProjectsList.component";
export * from "@app/Components/ProjectFullscreen/ProjectFullscreen.component";
export * from "@app/Components/IndicatorsTree/IndicatorsTree.component";
export * from "@app/Components/StaffEdit/StaffEdit.component";
export * from "@app/Components/IndexComponent/index.component";

//Templates
export * from "@app/Templates/BottomSheets/BottomSheet";
export * from "@app/Templates/DashboardTemplate/DashboardTemplate.component";
export * from "@app/Templates/Dialogs/Dialog";
export * from "@app/Templates/Dialogs/DialogNodeIndicator/DialogNodeIndicator";
export * from "@app/Templates/Dialogs/DialogChangeIndicator/DialogChangeIndicator";
export * from "@app/Templates/Dialogs/DialogAddIndicator/DialogAddIndicator";
export * from "@app/Templates/Dialogs/DialogCalculateIndicator/DialogCalculateIndicator";
export * from "@app/Templates/Dialogs/DialogStaffEdit/DialogStaffEdit";

//Services
export { HttpService } from '@app/Services/http.service';

//Repositories
export { DashboardRepository } from '@app/Repositories/dashboard.repository'

//Pipes
export { CustomDateAdapter } from '@app/Pipes/customDateAdapter';
