import { throwError as observableThrowError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import * as FileSaver from 'file-saver';
import { Dialog } from "@app/Templates/Dialogs/Dialog"
export class Task {
    ID: number;
    Name: string;
    Description: string;
    ParentID: number;
    ChildrenID: Array<number>;
}

@Injectable()
export class HttpService {

    constructor(private router: Router, private http: HttpClient, private dialog: Dialog) { }

    private stringifyData(data: Array<any>) {
        let _data: string = "/";
        for (let item of data)
            _data += (item + "/");
        return _data;
    }

    //Временные экземпляры с JWT Tokens - полный перекат с прошлых на эти методы с JWT
    postData(_controller: string, _method: string, data: any, parameters?: HttpParams): Observable<object> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post('/api/' + _controller + '/' + _method, data, { headers: this.JWTHeader(headers), params: parameters }).pipe(
            catchError(error => {
                return this.catchError(error);
            })
        );
    }

    getData(_controller: string, _method: string, _data: Array<any> = [], parameters?: Array<any>): Observable<object> {
        return this.http.get('/api/' + _controller + '/' + _method + this.stringifyData(_data), { headers: this.JWTHeader(), params: this.HandleParameter(parameters) })
            .pipe(
                catchError(error => {
                    return this.catchError(error);
                })
            );
    }

    //Для выгрузки файла
    postFile(_controller: string, _method: string, data: any): Observable<any> {
        return this.http.post('/api/' + _controller + '/' + _method, data, { headers: this.JWTHeader() })
            .pipe(
                catchError(error => {
                    return this.catchError(error);
                })
            );
    }

    DownloadFile(file: any) {
        //Download File from server
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        headers = headers.append('MyApp-Application', 'AppName');
        headers = headers.append('Accept', 'application/pdf');

        this.http.post("/api/Files/Download/" + file.id, "", { headers: this.JWTHeader(headers), responseType: "blob" })
            .subscribe(data => {
                FileSaver.saveAs(data, file.name);
            });
    }

    private catchError(error: any): Observable<any> {

        if (error.status === 401 || error.status === 403 ) {
            this.router.navigate(['/LogIn']);
            return observableThrowError(new Error(error.status));
        }
        this.dialog.openDialog("Error " + error.status, error.error ? error.error.message : error.message);
        return;
    }


    //Добавление заголовка для аутентификации JWT Tokens
    private JWTHeader(headers?: HttpHeaders): HttpHeaders {
        //headers - Остальные заголовки, к которым нужно добавить заголовок аутентификации, [если есть]
        //create JWT header
        let token = localStorage.getItem("JWTtoken");
        if (headers) {
            headers = headers.append('Authorization', 'Bearer ' + token);
            return headers;
        }
        if (token) {
            return new HttpHeaders().set('Authorization', 'Bearer ' + token);
        }
    }

    //Формирование параметров для запроса
    private HandleParameter(parameters?: Array<any>): HttpParams {
        let params = new HttpParams();
        if (!parameters || parameters.length == 0) return;
        for (let parameter of parameters)
            if (parameter) params = params.append(parameter.name, parameter.value);
        return params;
    }
}
