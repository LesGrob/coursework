import {
    BrowserModule, NgModule, FormsModule, FlexLayoutModule, ReactiveFormsModule, BrowserAnimationsModule,

    AngularSplitModule, DragulaModule,

    DxDataGridModule, DxContextMenuModule, DxPieChartModule, DxTreeListModule, DxChartModule,

    MatAutocompleteModule, MatToolbarModule, MatBottomSheetModule, MatBadgeModule,
    MatButtonModule, MatButtonToggleModule,
    MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule,
    MatExpansionModule, MatIconModule, MatSnackBarModule, MatProgressBarModule, MatInputModule,
    MatListModule, MatMenuModule, MatNativeDateModule, MatSelectModule, MatSidenavModule, MatTabsModule,
} from "@app/BarrelFile";

import {
    HttpClientModule, DateAdapter,

    HttpService, DashboardRepository,

    AddIndicatorDialog, ChangeIndicatorDialog, NodeIndicatorDialog, CalculateIndicatorDialog,
    Dialog, DialogConfirmComponent, DialogInfoComponent, DialogComponent,

    ProjectsListComponent, IndicatorsTreeComponent, AppComponent,
    DashboardComponent, StaffEditDialog,
    ProjectFullscreenComponent, StaffEditComponent, 
    IndexComponent,

    StaffPostFilter,

    DashboardTemplateComponent, BottomSheetComponent, BottomSheet,
} from "@app/BarrelFile"

import 'hammerjs';
import { AppRoutingModule } from "@app/app.routing.module";

@NgModule({
    declarations: [
        BottomSheetComponent,

        ProjectsListComponent, AppComponent, DashboardComponent,
        ProjectFullscreenComponent, StaffEditComponent,
        IndicatorsTreeComponent, IndexComponent,
        DashboardTemplateComponent, 

        NodeIndicatorDialog, DialogConfirmComponent, DialogInfoComponent,
        DialogComponent, AddIndicatorDialog, ChangeIndicatorDialog,
        CalculateIndicatorDialog, StaffEditDialog,

        StaffPostFilter
    ],
    imports: [
        AppRoutingModule, BrowserAnimationsModule, FormsModule, ReactiveFormsModule, HttpClientModule,
        BrowserModule, FormsModule,

        AngularSplitModule, DragulaModule.forRoot(), FlexLayoutModule,

        DxContextMenuModule, DxChartModule, DxPieChartModule, DxDataGridModule, DxTreeListModule,

        MatBottomSheetModule, MatAutocompleteModule, MatButtonModule, MatButtonToggleModule,
        MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatTabsModule,
        MatDialogModule, MatExpansionModule, MatIconModule, MatInputModule, MatSnackBarModule,
        MatListModule, MatMenuModule, MatNativeDateModule, MatSelectModule, MatBadgeModule,
        MatProgressBarModule, MatSidenavModule, MatToolbarModule
    ],
    bootstrap: [AppComponent],
    providers: [
        BottomSheet, HttpClientModule, HttpService, Dialog, DashboardRepository
    ],
    entryComponents: [
        BottomSheetComponent, DialogConfirmComponent, DialogInfoComponent, DialogComponent,
        AddIndicatorDialog, ChangeIndicatorDialog, NodeIndicatorDialog,
        StaffEditDialog, CalculateIndicatorDialog
    ]
})


export class AppModule {
    constructor(private dateAdapter: DateAdapter<Date>) { this.dateAdapter.setLocale('ru-RU'); }
}
