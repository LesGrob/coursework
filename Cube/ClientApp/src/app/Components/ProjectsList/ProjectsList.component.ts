import { Component, Input, ViewChild, TemplateRef, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { map } from "rxjs/operators"
import { BottomSheet } from '@app/Templates/BottomSheets/BottomSheet';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';

@Component({
    selector: 'ProjectsList',
    templateUrl: './ProjectsList.component.html',
    styleUrls: ['./ProjectsList.component.css']
})
export class ProjectsListComponent {

    constructor(public router: Router, private route: ActivatedRoute, public dashboardRepository: DashboardRepository,
        private location: Location, private projectBS: BottomSheet) { }

    SelectProject(project) {
            this.router.navigate(['/Project', project.id, 'Dashboard'], {});
    }
    private isLinkActive(projectId: number): boolean { return this.dashboardRepository.ProjectId$.value == projectId; }

    //Workspace dialog
    @ViewChild('BSProjectTemplate') BSProjectTemplate: TemplateRef<any>;
    openProjectDialog(): void {
        this.projectBS.openDialog(this.BSProjectTemplate);
    }
    AddProject(projectName: string) {
        this.dashboardRepository.httpService.postData("Projects", "Project", { name: projectName }).pipe(map(data => data as any)).subscribe(
            data => {
                this.dashboardRepository.Projects$.value.push(data);
                this.SelectProject(data);
                this.projectBS.dismiss();
            });
    }
}
