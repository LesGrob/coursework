import { Component, ViewChild, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { BehaviorSubject } from "rxjs";

import { Dialog } from '@app/Templates/Dialogs/Dialog';
import { ChangeIndicatorDialog } from '@app/Templates/Dialogs/DialogChangeIndicator/DialogChangeIndicator';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
import { map } from 'rxjs/operators';

@Component({
    selector: 'Dashboard',
    templateUrl: './Dashboard.component.html',
    styleUrls: ['./Dashboard.component.css']
})
export class DashboardComponent {


    constructor(private dialogConfirm: Dialog, private dialogShelf: Dialog, private dialogShelfIndicators: Dialog,
        private dialog: MatDialog, private dashboardRepository: DashboardRepository) { }

    ngOnInit() {
        this.dashboardRepository.ProjectId$.subscribe(id => {
            this.dataLoader.next(false);
            if (id && id != 0) {
                this.dashboardRepository.httpService.getData('Indicator', 'Shelves', [this.dashboardRepository.ProjectId$.value]).subscribe(
                    data => {
                        this.Shelves = data as Array<any>
                        this.dataLoader.next(true);
                    });
            }
        });
    }

    Shelves: Array<any>;

    dataLoader: BehaviorSubject<boolean> = new BehaviorSubject(false);

    RemoveIndicator(dashId: any) {
        this.dashboardRepository.httpService.postData('Indicator', 'RemoveShelfIndicator' + '/' + dashId, {}).subscribe(
            data => {
                if (data) {
                    let shelf = this.Shelves.filter(x => x.indicators && x.indicators.filter(i => i.userShelfId == dashId).length > 0)[0];
                    if (shelf) shelf.indicators.splice(shelf.indicators.findIndex(i => i.userShelfId == dashId), 1);
                }

            });
    }


    onDepartmentsLoad(dash) {
        if (dash.id && dash.measureDefault && dash.periodDefault && dash.dataFact && dash.dataFact.department && dash.dataFact.reportDate) {
            let shelf = this.Shelves.filter(x => (x.indicators as Array<any>).includes(dash))[0];
            if (shelf) {
                let date: Date = new Date(dash.dataFact.reportDate as Date);
                this.dashboardRepository.httpService.getData("Indicator", "IndicatorByDepartment", [dash.id, (date as Date).toDateString(), dash.measureDefault.id, dash.periodDefault.id, dash.dataFact.department.id, shelf.id]).subscribe(
                    data => {
                        shelf.indicators.push(...(data as Array<any>));
                    });
            }
        }
    }
    onDriversLoad(dash) {
        if (dash.id && dash.measureDefault && dash.periodDefault && dash.dataFact && dash.dataFact.department && dash.dataFact.reportDate) {
            let shelf = this.Shelves.filter(x => (x.indicators as Array<any>).includes(dash))[0];
            if (shelf) {
                let date: Date = new Date(dash.dataFact.reportDate as Date);
                this.dashboardRepository.httpService.getData("Indicator", "IndicatorByParent", [dash.id, (date as Date).toDateString(), dash.dataFact.department.id, shelf.id]).subscribe(
                    data => {
                        shelf.indicators.push(...(data as Array<any>));
                    });
            }
        }
    }

    onIndicatorChange(dash: any) {
        if (dash.remove) {
            this.RemoveIndicator(dash.dash.id);
            return;
        }

        dash = dash.dash;

        let dialogRef = this.dialog.open(ChangeIndicatorDialog, {
            width: '600px',
            data: { dash: dash }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                let shelf = this.Shelves.filter(x => (x.indicators as Array<any>).includes(dash))[0];
                if (result.remove) {
                    this.RemoveIndicator(dash.userShelfId);
                } else if (shelf && result.Period && result.Measure && result.Department && result.Date) {
                    let date: Date = new Date(result.Date as Date);
                    date.setDate(date.getDate() + 1);
                    this.dashboardRepository.httpService.postData('Indicator', 'UserShelfChangeData', {
                        Id: dash.userShelfId,
                        Shelf: { Id: shelf.id },
                        Indicator: { Id: dash.id },
                        Measure: { Id: result.Measure.id },
                        Period: { Id: result.Period.id },
                        Department: { Id: result.Department.id },
                        Employee: { Id: result.Employee.id },
                        Date: date.toUTCString()
                    }).pipe(map(data => data as any)).subscribe(
                        data => {
                            if (data) {
                                let index = shelf.indicators.findIndex(i => i.userShelfId == data.userShelfId);
                                if (index) shelf.indicators[index] = data;
                            }
                        });
                }
            }
        });
    }



    removeShelf(shelfId: number) {
        if (shelfId)
            this.dialogConfirm.openDialog("Полка", 'Вы желаете удалить полку?', true).subscribe(responce => {
                if (responce)
                    this.dashboardRepository.httpService.postData('Indicator', 'RemoveShelf' + '/' + shelfId, {}).subscribe(
                        data => {
                            let shelf = this.Shelves.filter(x => x.id == shelfId)[0];
                            if (shelf) this.Shelves.splice(this.Shelves.indexOf(shelf, 0), 1);
                        });
            });
    }



    //#region Shelf
    @ViewChild('DShelf') DShelf: TemplateRef<any>;
    shelfName = new FormControl('', [Validators.required]);
    tempShelfData: any = null;
    openShelfDialog(shelf?: any) {
        this.tempShelfData = shelf ? shelf : null;
        this.shelfName.setValue(shelf ? shelf.name : '');
        this.dialogShelf.openDialogTemplate(this.DShelf).subscribe(result => {
            if (result) {
                let params = this.dashboardRepository.ProjectId$.value + '/' + this.shelfName.value + '/' + (shelf ? shelf.id : 0);
                this.dashboardRepository.httpService.postData('Indicator', 'Shelf/' + params, {})
                    .pipe(map(data => data as any)).subscribe(data => {
                        if (shelf) {
                            let shelf = this.Shelves.filter(x => x.id == data.id)[0];
                            if (shelf) shelf.name = data.name;
                        } else {
                            this.Shelves.push(data)
                        }
                    });
            }
        });
    }
    //#endregion

    //#region Add Indicator
    @ViewChild('DShelfIndicators') DShelfIndicators: TemplateRef<any>;
    indicatorControl = new FormControl('', [Validators.required]);
    Indicators: Array<any>;
    openIndicatorDialog(_shelfId: number): void {
        this.dashboardRepository.httpService.getData('Indicator', 'FreeIndicators', [this.dashboardRepository.ProjectId$.value]).subscribe(
            data => {
                this.Indicators = data as Array<any>;
                this.dialogShelfIndicators.openDialogTemplate(this.DShelfIndicators).subscribe(result => {
                    if (result)
                        this.dashboardRepository.httpService.postData('Indicator', 'ShelfIndicator/' + _shelfId + '/' + this.indicatorControl.value, {}).pipe(map(data => data as any)).subscribe(
                            data => {
                                let shelf = this.Shelves.filter(x => x.id == _shelfId)[0];
                                if (shelf) {
                                    if (shelf.indicators)
                                        shelf.indicators.push(data);
                                    else
                                        shelf.indicators = [data];
                                }
                            });
                    this.indicatorControl.setValue(null);
                });
            });
    }
    //#endregion
}