import { Component, ViewChild } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { BehaviorSubject } from "rxjs";
import { map } from "rxjs/operators"
import { StaffEditDialog } from '@app/Templates/Dialogs/DialogStaffEdit/DialogStaffEdit';
import { ActivatedRoute } from '@angular/router';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';

@Component({
    selector: 'StaffEdit',
    templateUrl: './StaffEdit.component.html',
    styleUrls: ['./StaffEdit.component.css']
})

export class StaffEditComponent {
    constructor(public dashboardRepository: DashboardRepository, public dialog: MatDialog, public snackBar: MatSnackBar, private route: ActivatedRoute) {
        this.dashboardRepository.httpService.getData('Staff', 'StaffData', [this.dashboardRepository.ProjectId$.value]).pipe(map(data => data as Array<any>)).subscribe(
            data => { this.StaffData = data; });
    }

    dataLoader = new BehaviorSubject<any>(null);
    @ViewChild('StaffGrid') StaffGrid: any;

    StaffData: Array<any>;

    ChangeItem(item: any) {
        let parents: Array<any> = Array<any>();
        switch (item.code) {
            case "Department": parents = this.StaffData.filter(x => x.code == "Department" && (item.uid ? x.uid != item.uid && !this.IsUnderDepLeaf(item.uid, x.uid) : true)).sort(this.compare); break;
            case "Post": parents = this.StaffData.filter(x => x.code == "Department").sort(this.compare); break;
            case "Employee": parents = this.StaffData.filter(x => x.code != "Employee").sort(this.compare); break;
        }

        let dialogRef = this.dialog.open(StaffEditDialog, {
            width: '600px',
            data: { item: item, parents: parents }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result && result.StaffItem) {
                if (result.StaffItem.dateOpen) result.StaffItem.dateOpen = this.SetDate(result.StaffItem.dateOpen, 1);
                if (result.StaffItem.dateClose) result.StaffItem.dateClose = this.SetDate(result.StaffItem.dateClose, 1);

                let parent = this.StaffData.find(x => x.uid == result.StaffItem.parentUID);
                if (parent) result.StaffItem.parentId = parent.id;

                if (result.action == "edit") {
                    this.dashboardRepository.httpService.postData("Staff", "ChangeStaffItem", result.StaffItem).subscribe(
                        data => {
                            if (data) {
                                let index = this.StaffData.findIndex(x => x.uid == item.uid);
                                if (index) {
                                    if (result.StaffItem.dateOpen) result.StaffItem.dateOpen = this.SetDate(result.StaffItem.dateOpen, -1);
                                    if (result.StaffItem.dateClose) result.StaffItem.dateClose = this.SetDate(result.StaffItem.dateClose, -1);

                                    this.StaffData[index] = result.StaffItem;

                                    this.StaffGrid.instance.refresh();
                                }
                            }
                        });
                } else if (result.action == "add") {
                    this.dashboardRepository.httpService.postData("Staff", "AddStaffItem", {
                        Name: result.StaffItem.name,
                        Code: result.StaffItem.code,
                        DateOpen: result.StaffItem.dateOpen,
                        DateClose: result.StaffItem.dateClose,
                        ParentId: result.StaffItem.parentId,
                        Type: { Id: result.StaffItem.type.id, Value: result.StaffItem.type.value },
                        Project: { Id: result.StaffItem.project.id },
                    }).pipe(map(data => data as any)).subscribe(
                        data => {
                            if (data) {
                                let dataParent;
                                if (data.code == "Employee") dataParent = this.StaffData.find(x => x.id == data.parentId && x.code == "Post");
                                else dataParent = this.StaffData.find(x => x.id == data.parentId && x.code == "Department");
                                if (dataParent) data.parentUID = dataParent.uid;

                                this.StaffData.push(data);
                                this.StaffGrid.instance.refresh();
                            }
                        });
                }
            }

        });
    }

    compare(a, b) {
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    }
    //find if item is under itself
    IsUnderDepLeaf(parentUID: any, itemUID: any): boolean {
        let result = false;
        for (var item of this.StaffData.filter(x => x.parentUID == parentUID)) {
            if (item.uid == itemUID) return true;
            result = this.IsUnderDepLeaf(item.uid, itemUID);
            if (result) return true;
        }
        return result;
    }

    SetDate(date: Date, days: number): Date {
        date = new Date(date);
        date.setDate(date.getDate() + days);
        return date;
    }
}
