import { Component, ViewContainerRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';

@Component({
    selector: 'index-component',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent {
    constructor(private router: Router, private route: ActivatedRoute, private dashboardRepository: DashboardRepository) {
        this.dashboardRepository.getProjects();
        this.routeSubscribtion = route.params.subscribe(
            params => { this.dashboardRepository.ProjectId$.next(params.id) });
    }
    @ViewChild('LeftBarTemplateContainer', { read: ViewContainerRef }) LeftBarTemplateContainer;
    ngOnDestroy() { this.routeSubscribtion.unsubscribe(); }
    private routeSubscribtion: Subscription;
}