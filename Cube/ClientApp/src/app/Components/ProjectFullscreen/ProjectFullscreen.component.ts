import { Component, ViewChild, TemplateRef } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';

import { BottomSheet } from '@app/Templates/BottomSheets/BottomSheet';
import { map } from "rxjs/operators"
import { DashboardRepository } from '@app/Repositories/dashboard.repository';

@Component({
    selector: 'ProjectFullscreen',
    templateUrl: './ProjectFullscreen.component.html',
    styleUrls: ['./ProjectFullscreen.component.css']
})

export class ProjectFullscreenComponent {

    constructor(public dashboardRepository: DashboardRepository, public dialog: MatDialog,
        public snackBar: MatSnackBar, private projectBS: BottomSheet) {
        this.dashboardRepository.getData(this.dashboardRepository.ProjectId$.value).subscribe(
            data => {
                if (data)
                    this.dashboardRepository.httpService.getData('Projects', 'Project', [this.dashboardRepository.ProjectId$.value]).subscribe(
                        data => {
                            this.Project = data;
                        });
            });
    }


    Project: any = null;
    cubeData: any = {
        indicator: null,
        plan: null,
        measure: null,
        period: null,
        date: null
    };

    employeeTableData: any = {
        indicator: null,
        department: null,
        measure: null,
        period: null,
        date: null
    };

    departmentsData: Array<any>;
    employeeData: Array<any>

    ShowResults() {
        if (this.cubeData.date && this.cubeData.indicator && this.cubeData.plan && this.cubeData.measure && this.cubeData.period)
            this.dashboardRepository.httpService.getData('Indicator', 'DepartmentTableData',
                [(this.cubeData.date as Date).toUTCString(), this.Project.id, this.cubeData.indicator, this.cubeData.plan, this.cubeData.measure, this.cubeData.period])
                .subscribe(data => {
                    this.departmentsData = data as Array<any>;
                });
    }

    ShowEmployeeResults() {
        if (this.employeeTableData.department && this.employeeTableData.date && this.employeeTableData.indicator && this.employeeTableData.measure && this.employeeTableData.period)
            this.dashboardRepository.httpService.getData('Indicator', 'EmployeeTableData',
                [(this.employeeTableData.date as Date).toUTCString(), this.Project.id, this.employeeTableData.indicator, this.employeeTableData.department, this.employeeTableData.measure, this.employeeTableData.period])
                .subscribe(data => {
                    this.employeeData = data as Array<any>;
                });
    }

    onEmployeeRowUpdated($event) {
        this.dashboardRepository.httpService.postData("Indicator", "EmployeePlanFactData", [{
            Employee: { Id: $event.oldData.id },
            DepartmentId: this.employeeTableData.department,
            Value: $event.newData.dataEmployee ? $event.newData.dataEmployee.value : 0,
            StuffNormValue: $event.newData.stuffNorm ? $event.newData.stuffNorm.value : 0,
            IndicatorId: this.employeeTableData.indicator,
            MeasureId: this.employeeTableData.measure,
            PeriodId: this.employeeTableData.period,
            ReportDate: (this.employeeTableData.date as Date).toUTCString()
        }]).subscribe(responce => { }, error => { console.log(error); });
    }

    onRowUpdated($event) {
        this.dashboardRepository.httpService.postData("Indicator", "PlanFactData", [{
            DepartmentId: $event.oldData.id,
            FactValue: ($event.newData.dataFact != undefined ? $event.newData.dataFact.value : 0),
            PlanValue: ($event.newData.dataPlan != undefined ? $event.newData.dataPlan.value : 0),
            IndicatorId: this.cubeData.indicator,
            PlanId: this.cubeData.plan,
            MeasureId: this.cubeData.measure,
            PeriodId: this.cubeData.period,
            ReportDate: (this.cubeData.date as Date)
        }]).subscribe(responce => { }, error => { console.log(error); });
    }

    //Project dialog
    @ViewChild('BSProjectTemplate') BSProjectTemplate: TemplateRef<any>;
    @ViewChild('BSAccessTemplate') BSAccessTemplate: TemplateRef<any>;
    openProjectDialog(): void {
        this.projectBS.openDialog(this.BSProjectTemplate);
    }
    ChangeProjectName(projectName: string) {
        if (projectName != "" && this.Project.id)
            this.dashboardRepository.httpService.postData("Projects", "RenameProject", { id: this.Project.id, name: projectName }).pipe(map(data => data as any)).subscribe(
                data => {
                    this.Project = data
                    let index = this.dashboardRepository.Projects$.value.findIndex(x => x.id == data.id)
                    if (index > -1) this.dashboardRepository.Projects$.value[index].name = data.name;
                    this.projectBS.dismiss();
                });
    }
}
