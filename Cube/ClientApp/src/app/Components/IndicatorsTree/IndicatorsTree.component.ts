import { Component, ViewChild } from '@angular/core';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';
import { MatDialog, MatSnackBar } from '@angular/material';

import { BehaviorSubject } from 'rxjs';

import { Dialog } from '@app/Templates/Dialogs//Dialog'
import { NodeIndicatorDialog } from '@app/Templates/Dialogs/DialogNodeIndicator/DialogNodeIndicator'
import { CalculateIndicatorDialog } from '@app/Templates/Dialogs/DialogCalculateIndicator/DialogCalculateIndicator'
import { AddIndicatorDialog } from '@app/Templates/Dialogs/DialogAddIndicator/DialogAddIndicator'

import { filter, map } from "rxjs/operators"

@Component({
    selector: 'IndicatorsTree',
    templateUrl: './IndicatorsTree.component.html',
    styleUrls: ['./IndicatorsTree.component.css']
})
export class IndicatorsTreeComponent {


    constructor(public dialogConfirm: Dialog, public dashboardRepository: DashboardRepository, public dialog: MatDialog, public snackBar: MatSnackBar) { }

    ngOnInit() {
        this.dashboardRepository.getData(this.dashboardRepository.ProjectId$.value).subscribe(
            result => {
                if (result) this.dataLoader.next(true);
            });
    }

    dataLoader: BehaviorSubject<boolean> = new BehaviorSubject(false);
    @ViewChild('IndicatorsGrid') IndicatorsGrid: any;

    ChangeIndicator(dash: any) {
        let dialogRef = this.dialog.open(NodeIndicatorDialog, {
            width: '600px',
            data: dash
        });
        dialogRef.afterClosed().subscribe(
            result => {
                if (result) {
                    if (result.removeId) {
                        //Remove indicatror
                        this.dialogConfirm.openDialog("Показатель", "Вы действительно желаете удалить показатель '" + dash.name + "'", true).subscribe(
                            data => {
                                if (data)
                                    this.dashboardRepository.httpService.postData("Indicator", "RemoveIndicator/" + result.removeId, {}).pipe(map(data => data as any)).subscribe(
                                        data => {
                                            if (data) {
                                                for (var child of this.dashboardRepository.Indicators.filter(x => x.parent && x.parent.id == data.id))
                                                    child.parent = data.parent;
                                                let index = this.dashboardRepository.Indicators.findIndex(x => x.id == data.id);
                                                if (index) this.dashboardRepository.Indicators.splice(index, 1);
                                            }
                                            this.IndicatorsGrid.instance.refresh();
                                        });
                            });

                    } else {
                        //Change indicator default values
                        this.dashboardRepository.httpService.postData("Indicator", "IndicatorDefaults", {
                            Id: dash.id,
                            Parent: { Id: result.parent.id },
                            MeasureDefault: { Id: result.measure.id },
                            PeriodDefault: { Id: result.period.id },
                            PlanDefault: { Id: result.plan.id },
                            Type: { Id: result.type.id },
                            Owner: { Id: result.owner.id }
                        }).pipe(map(data => data as any)).subscribe(
                            data => {
                                let indicator = this.dashboardRepository.Indicators.find(x => x.id == data.id);
                                if (indicator) {

                                    indicator.parent = data.parent;
                                    indicator.measureDefault = data.measureDefault;
                                    indicator.periodDefault = data.periodDefault;
                                    indicator.planDefault = data.planDefault;
                                    indicator.type = data.type;
                                    indicator.owner = data.owner;

                                    this.IndicatorsGrid.instance.refresh();
                                    this.snackBar.open("Показатель изменен", "ОК", { duration: 2000 });
                                }
                            });
                    }
                }
            });
    }

    CalculateIndicator(dash: any) {
        let dialogRef = this.dialog.open(CalculateIndicatorDialog, {
            width: '600px',
            data: dash
        });
        dialogRef.afterClosed().subscribe(
            result => {
                if (result && result.date && result.department.id != 0 && result.measure.id != 0
                    && result.plan.id != 0 && result.period.id != 0) {
                    let date: Date = new Date(result.date as Date);
                    let params: string = "/" + dash.id + "/" + result.recursive + "/" + result.measure.id + "/" +
                        result.period.id + "/" + result.plan.id + "/" + dash.type.id + "/" + result.department.id + "/" + date.toUTCString();
                    this.dashboardRepository.httpService.postData("Indicator", "CalculateIndicator" + params, {}).pipe(filter(data => data != null), map(data => data as any)).subscribe(
                        data => { this.snackBar.open("Показатель рассчитан", "ОК", { duration: 2000 }); });
                }
            });
    }



    //Modal to Add Indicator
    openIndicatorDialog(): void {
        if (this.dataLoader.value) {
            let dialogRef = this.dialog.open(AddIndicatorDialog, {
                width: '600px',
                data: { ProjectId: this.dashboardRepository.ProjectId$.value }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result && result.correct && result.indicator) {
                    this.dashboardRepository.httpService.postData('Indicator', 'Indicator', result.indicator).subscribe(
                        data => {
                            this.dashboardRepository.Indicators.push(data);
                            this.snackBar.open("Показатель добавлен", "ОК", { duration: 2000 });
                        });
                }
            });
        }
    }
}
