import { Injectable } from '@angular/core';
import { HttpService } from '@app/Services/http.service'
import { Observable, BehaviorSubject, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class DashboardRepository {
    constructor(public httpService: HttpService) { }

    public ProjectId$: BehaviorSubject<number> = new BehaviorSubject(null);
    public ProjectId: number = null;
    public Projects$: BehaviorSubject<Array<any>> = new BehaviorSubject([]);

    public Indicators: Array<any>;
    public Plans: Array<any>;
    public Measures: Array<any>;
    public Periods: Array<any>;
    public IndicatorTypes: Array<any>;
    public Departments: Array<any>;

    private sortFunc = (x, y) => {
        if (x.name > y.name) return 1;
        if (x.name < y.name) return -1;
        return 0;
    }

    public getProjects() {
        this.httpService.getData("Projects", "Projects").pipe(map(data => data as Array<any>)).subscribe(data => {
            this.Projects$.next(data);
        });
    }

    public getData(projectId: number): BehaviorSubject<any> {
        let result = new BehaviorSubject<any>(null);

        this.PlansObserver = this.httpService.getData('Plan', 'Plans', [projectId]);
        this.MeasuresObserver = this.httpService.getData('Measure', 'Measures', [projectId]);
        this.PeriodsObserver = this.httpService.getData('Period', 'Periods', [projectId]);
        this.IndicatorsObserver = this.httpService.getData('Indicator', 'Indicators', [projectId]);
        this.IndicatorTypesObserver = this.httpService.getData('Indicator', 'IndicatorTypes', [projectId]);
        this.DepartmentsObserver = this.httpService.getData('Department', 'Departments', [projectId]);

        forkJoin([this.PlansObserver, this.MeasuresObserver, this.PeriodsObserver, this.IndicatorsObserver, this.IndicatorTypesObserver, this.DepartmentsObserver]).subscribe(
            results => {

                this.Plans = (results[0] as Array<any>).sort(this.sortFunc);
                this.Measures = (results[1] as Array<any>).sort(this.sortFunc);
                this.Periods = (results[2] as Array<any>).sort(this.sortFunc);
                this.Indicators = (results[3] as Array<any>).sort(this.sortFunc);
                this.IndicatorTypes = (results[4] as Array<any>).sort(this.sortFunc);
                this.Departments = (results[5] as Array<any>).sort(this.sortFunc);

                result.next(true);
            });

        return result;
    }

    PlansObserver: Observable<object>;
    MeasuresObserver: Observable<object>;
    PeriodsObserver: Observable<object>;
    IndicatorsObserver: Observable<object>;
    IndicatorTypesObserver: Observable<object>;
    DepartmentsObserver: Observable<object>;



    //Cleardata in all variables
    public ClearAllData() {
        this.Indicators = null;
        this.Plans = null;
        this.Measures = null;
        this.Periods = null;
        this.Indicators = null;
        this.IndicatorTypes = null;
        this.Departments = null;
    }
}
