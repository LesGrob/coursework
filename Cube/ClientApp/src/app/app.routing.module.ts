import {
    NgModule,
    RouterModule,

    DashboardComponent,
    IndicatorsTreeComponent,
    StaffEditComponent,
    ProjectFullscreenComponent,
    IndexComponent
} from "./BarrelFile"

//Main Routing Module
@NgModule({
    imports: [RouterModule.forRoot([
        { path: '', redirectTo: '/Project/0/Dashboard', pathMatch: 'full' },
        { path: 'Project', redirectTo: '/Project/0/Table', pathMatch: 'full' },
        { path: 'Project/:id', redirectTo: '/Project/:id/Table', pathMatch: 'full' },
        {
            path: 'Project/:id', component: IndexComponent, children: [
                { path: "Dashboard", component: DashboardComponent },
                { path: "Staff", component: StaffEditComponent },
                { path: "IndicatorsTree", component: IndicatorsTreeComponent },
                { path: 'Info', component: ProjectFullscreenComponent }
            ]
        }
    ])],
    exports: [RouterModule]
})
export class AppRoutingModule {}