import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';

@Component({
    selector: 'calculateIndicator-dialog',
    templateUrl: './DialogCalculateIndicator.html'
})
export class CalculateIndicatorDialog {

    constructor(public dialogRef: MatDialogRef<CalculateIndicatorDialog>,
        public dashboardRepository: DashboardRepository, @Inject(MAT_DIALOG_DATA) public dash: any) {

        this.date = new Date();
        this.department = { id: 0, name: "" }
        this.measure = { id: 0, name: "" }
        this.plan = { id: 0, name: "" }
        this.period = { id: 0, name: "" }
    }

    recursive: boolean = false;
    date: Date;
    department: any;
    measure: any;
    plan: any;
    period: any;
}
