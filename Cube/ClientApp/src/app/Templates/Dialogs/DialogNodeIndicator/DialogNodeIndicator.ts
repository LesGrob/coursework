import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';

@Component({
    selector: 'nodeIndicator-dialog',
    templateUrl: './DialogNodeIndicator.html'
})
export class NodeIndicatorDialog {

    constructor(public dialogRef: MatDialogRef<NodeIndicatorDialog>,
        public dashboardRepository: DashboardRepository, @Inject(MAT_DIALOG_DATA) public dash: any) {

        this.Indicators = this.dashboardRepository.Indicators.filter(x => x.id != this.dash.id);
        this.RemoveChildren(dash.id);
        this.Indicators.push({ id: 0, name: "Без родителя" });

        this.parent = dash.parent ? Object.assign({}, dash.parent) : { id: 0 };
        this.measureDefault = Object.assign({}, dash.measureDefault);
        this.periodDefault = Object.assign({}, dash.periodDefault);
        this.planDefault = Object.assign({}, dash.planDefault);
        this.typeDefault = Object.assign({}, dash.type);
        this.owner = Object.assign({}, dash.owner);
    }

    Indicators: Array<any>;
    parent: any;
    measureDefault: any;
    periodDefault: any;
    planDefault: any;
    typeDefault: any;
    owner: any;


    RemoveChildren(dashId: number) {
        for (var item of this.Indicators.filter(x => x.parent && x.parent.id == dashId)) {
            this.RemoveChildren(item.id);
            let index = this.Indicators.findIndex(x => x.id == item.id);
            this.Indicators.splice(index, 1);
        }
    }

}
