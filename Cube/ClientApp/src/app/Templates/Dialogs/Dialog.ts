import { Component, Injectable, Inject, TemplateRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';

@Injectable()
export class Dialog {

    constructor(private dialog: MatDialog) { }

    openDialog(label: string, description: string, confirm?: boolean): Observable<any> {
        let component;
        if (confirm) component = DialogConfirmComponent;
        else component = DialogInfoComponent;
        let dialogRef = this.dialog.open(component, {
            width: '400px',
            data: { label: label, description: description }
        });
        return dialogRef.afterClosed();
    };
    openDialogTemplate(Template: TemplateRef<any>, width?: string): Observable<any> {
        let dialogRef = this.dialog.open(DialogComponent, {
            width: width ? width : '400px',
            data: Template
        });
        return dialogRef.afterClosed();
    };
    close() { this.dialog.closeAll(); }
}

//Modal 
@Component({
    selector: 'dialog-error',
    template: "<h2 mat-dialog-title>{{data.label}}</h2>\
    <mat-dialog-content>{{data.description}}</mat-dialog-content>\
    <mat-dialog-actions>\
    <button mat-button mat-dialog-close color='accent' mat-dialog-close>OK</button>\
    </mat-dialog-actions>"
})
export class DialogInfoComponent {
    constructor(private dialogRef: MatDialogRef<DialogInfoComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }
}
@Component({
    selector: 'dialog-confirm',
    template: "<h2 mat-dialog-title>{{data.label}}</h2>\
    <mat-dialog-content>{{data.description}}</mat-dialog-content>\
    <mat-dialog-actions>\
        <button mat-button mat-dialog-close color='accent' [mat-dialog-close]='true'>Подтвердить</button>\
        <button mat-button mat-dialog-close>Отмена</button>\
    </mat-dialog-actions>"
})
export class DialogConfirmComponent {
    constructor(private dialogRef: MatDialogRef<DialogConfirmComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }
}


@Component({
    selector: 'dialog-component',
    template: "<template [ngTemplateOutlet]='data'></template>"
})
export class DialogComponent {
    constructor(private dialogRef: MatDialogRef<DialogInfoComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }
}
