import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';

@Component({
    selector: 'indicator-dialog',
    templateUrl: './DialogAddIndicator.html'
})
export class AddIndicatorDialog {
    constructor(public dashboardRepository: DashboardRepository, public dialogRef: MatDialogRef<AddIndicatorDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
        this.dataLoader.next(true);
    }

    dataLoader: BehaviorSubject<boolean> = new BehaviorSubject(false);

    Indicator = {
        Id: 0,
        Code: "",
        Name: null,
        Type: null,
        Owner: null,
        Parent: { Id: 0 },
        MeasureDefault: null,
        PeriodDefault: null,
        PlanDefault: null,
        Project: { Id: this.dashboardRepository.ProjectId$.value }
    };

    myControl: FormControl = new FormControl();

    public Departments: Array<any>;
    ownerValueChanged(event: any) {
        if (event.data)
            this.dashboardRepository.httpService.getData("Department", "AutocompleteDepartments", [event.data, this.data.ProjectId$.value]).subscribe(
                res => {
                    return this.Departments = res as Array<any>;
                }, error => { console.log("Error", error); });
        else
            this.Departments = [];

    };
    ownerSelectionChange(department) {
        if (department && department.id)
            this.Indicator.Owner = department;
    }

    formValidation(): boolean {
        return (this.Indicator.Name != null && this.Indicator.Type != null && this.Indicator.Owner != null
            && this.Indicator.MeasureDefault != null && this.Indicator.PeriodDefault != null && this.Indicator.PlanDefault != null);
    }
}
