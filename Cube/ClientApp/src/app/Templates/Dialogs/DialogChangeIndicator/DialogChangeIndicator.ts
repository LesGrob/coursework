import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';

@Component({
    selector: 'shelfIndicators-dialog',
    templateUrl: './DialogChangeIndicator.html'
})
export class ChangeIndicatorDialog {

    constructor(public dashboardRepository: DashboardRepository, public dialogRef: MatDialogRef<ChangeIndicatorDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any) {

        if (data && data.dash && data.dash.id) {
            if (data.dash.dataFact) {
                this.Period = data.dash.dataFact.period;
                this.Measure = data.dash.measureDefault;
                this.Department = data.dash.dataFact.department;
                this.Date = data.dash.dataFact.reportDate;
            }
            if (data.dash.dataEmployee && data.dash.dataEmployee.employee)
                this.Employee = data.dash.dataEmployee.employee;
            this.getRepository(data.dash.id, 'd', this.Department.id);
        }
    }

    getRepository(indicatorId: number, type?: string, value?: string) {
        this.dashboardRepository.httpService.getData('Indicator', 'ShelfIndicatorRepository', [indicatorId, type, value]).pipe(map(data => data as any)).subscribe(
            data => {
                this.Departments = data.departments as Array<any>;
                this.Measures = data.measures as Array<any>;
                this.Periods = data.periods as Array<any>;
                this.Dates = data.dates as Array<any>;
                this.Employees = data.employees as Array<any>;

                let period = this.Periods.filter(x => x.id == this.Period.id)[0];
                let measure = this.Measures.filter(x => x.id == this.Measure.id)[0];
                let department = this.Departments.filter(x => x.id == this.Department.id)[0];
                let employee = this.Employees.filter(x => x.id == this.Employee.id)[0];
                this.Date = this.Dates.filter(x => x == this.Date)[0];

                this.Period = period == undefined ? { id: 0 } : period;
                this.Measure = measure == undefined ? { id: 0 } : measure;
                this.Department = department == undefined ? { id: 0 } : department;
                this.Employee = employee == undefined ? { id: 0 } : employee;

                this.dataLoader.next(true);
            });
    }

    selectChange($event, type: string) {
        this.getRepository(this.data.dash.id, type, $event);
    }
    dateChange($event) {
        let date: Date = new Date($event);
        this.getRepository(this.data.dash.id, 'date', date.toDateString());
    }

    validation(): boolean { return (this.Period.id == 0 || this.Measure.id == 0 || this.Department.id == 0 || this.Date == undefined); }

    dataLoader: BehaviorSubject<boolean> = new BehaviorSubject(false);

    myControl: FormControl = new FormControl();

    public Departments: Array<any> = [];
    public Employees: Array<any> = [];
    public Measures: Array<any> = [];
    public Periods: Array<any> = [];
    public Dates: Array<any> = [];

    Period: any = { id: 0 };
    Measure: any = { id: 0 };
    Department: any = { id: 0 };
    Employee: any = { id: 0 };
    Date: any = undefined;
}
