import { Component, Inject, Pipe, PipeTransform } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';
import { DashboardRepository } from '@app/Repositories/dashboard.repository';

@Pipe({ name: 'staffPostFilter' })
export class StaffPostFilter implements PipeTransform {
    transform(array: Array<any>, value: number): Array<any> {
        return array.filter(x => x.parentUID == value && x.code == "Post");
    }
}

@Component({
    selector: 'staffItem-dialog',
    templateUrl: './DialogStaffEdit.html'
})
export class StaffEditDialog {

    constructor(public dashboardRepository: DashboardRepository, public dialogRef: MatDialogRef<StaffEditDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {

        this.StaffItem.code = data.item.code;
        this.StaffItem.project.id = this.dashboardRepository.ProjectId$.value;
        this.StaffParents = data.parents.filter(x => x.code == "Department");

        if (data && data.item.uid) {
            this.action = "edit";
            this.StaffItem = { ...data.item };

            if (data.item.code == "Employee" && data.item.parentUID) {
                let post = data.parents.find(x => x.uid == data.item.parentUID && x.code == 'Post');
                if (post) {
                    let dep = data.parents.find(x => x.uid == post.parentUID);
                    if (dep) this.postParent = dep.uid;
                }
                this.onUnitChange();
            }

        }

        if (this.StaffItem.code && this.StaffItem.code != "Employee")
            this.dashboardRepository.httpService.getData("Staff", "GetStaffTypes", [this.dashboardRepository.ProjectId$.value, this.StaffItem.code]).pipe(map(data => data as Array<any>)).subscribe(
                data => {
                    this.StaffTypes = data;
                    this.StaffItem.type = this.StaffTypes[this.StaffTypes.findIndex(x => x.id == this.StaffItem.type.id)];
                    this.dataLoader.next(true);
                });
        else this.dataLoader.next(true);
    }

    dataLoader: BehaviorSubject<boolean> = new BehaviorSubject(false);

    StaffItem = {
        id: 0,
        uid: "",
        name: "",
        code: "",
        parentId: 0,
        parentUID: "",
        dateOpen: null,
        dateClose: null,
        type: { id: 0, value: 1 },
        project: { id: 0 }
    };

    StaffParents: Array<any>;
    StaffTypes: Array<any>;
    action = "add";
    unitCorrect: any = false;

    postParent: number;
    filter = new StaffPostFilter();

    nameControl: FormControl = new FormControl('', [Validators.required, Validators.nullValidator]);
    parentControl: FormControl = new FormControl('', [Validators.required, Validators.nullValidator]);
    unitControl: FormControl = new FormControl(this.unitCorrect, [Validators.min(0.1), Validators.max(1)]);
    typeControl: FormControl = new FormControl('', [Validators.required, Validators.nullValidator]);

    Validation(): boolean {
        if (this.StaffItem.dateOpen && this.nameControl.valid && this.unitControl.valid && this.parentControl.valid
            && (this.StaffItem.code == "Employee" ? this.unitCorrect : this.typeControl.valid))
            return true;
        return false;
    }


    onUnitChange() {
        let parent = this.data.parents.find(x => x.uid == this.StaffItem.parentUID);
        if (parent)
            this.dashboardRepository.httpService.getData('Staff', 'CheckEmployeeUnit', [this.StaffItem.id, parent.id, this.StaffItem.type.value]).subscribe(
                data => { this.unitCorrect = data; });
        else this.unitCorrect = false;
    }
    onDepartmentChange() {
        this.StaffItem.parentUID = null;
    }
}
