import { Component, Inject, Injectable, TemplateRef } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheet, MatBottomSheetRef } from '@angular/material';
import { Observable } from 'rxjs';

@Injectable()
export class BottomSheet {

    constructor(private bottomSheet: MatBottomSheet) { }

    bSheet: MatBottomSheetRef<BottomSheetComponent, any>;
    openDialog(Template: TemplateRef<any>): Observable<any> {
        this.bSheet = this.bottomSheet.open(BottomSheetComponent, {
            data: Template
        });
        return this.bSheet.afterDismissed();
    }
    dismiss() {
        return this.bSheet.dismiss();
    }
}

@Component({
    selector: 'bottom-sheet',
    template: "<template [ngTemplateOutlet]='data'></template>"
})
export class BottomSheetComponent {
    constructor(private bottomSheetRef: MatBottomSheetRef<BottomSheetComponent>, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) { }
}