import { Component, EventEmitter, Input, Output } from '@angular/core';
import { HttpService } from '@app/Services/http.service';
import { map } from "rxjs/operators"
import * as moment from 'moment';

@Component({
    selector: 'DashboardTemplate',
    templateUrl: './DashboardTemplate.component.html',
    styleUrls: ['./DashboardTemplate.component.css']
})
export class DashboardTemplateComponent {

    constructor(private httpService: HttpService) { };

    spline: any;

    @Input() dash: any;

    @Output() onChange = new EventEmitter<any>();
    @Output() onDepartmentsLoad = new EventEmitter<any>();
    @Output() onDriversLoad = new EventEmitter<any>();

    ChangeIndicator(dash: any, remove?: boolean) {
        this.onChange.emit({ dash, remove });
    }

    PlanFactDates: Array<any> = [];

    Ratings: Array<any> = [];

    selectedTabChange($event) {
        if (this.dash.dataFact)
            if ($event.index == 1) {
                let date: Date = new Date(this.dash.dataFact.reportDate);
                this.httpService.getData('Indicator', 'DashboardChartData', [this.dash.id, date.toUTCString(), this.dash.dataFact.measure.id, this.dash.dataFact.period.id, this.dash.dataFact.department.id]).pipe(map(data => data as any)).subscribe(
                    data => {
                        this.PlanFactDates = data;
                    });
            } else if ($event.index == 2) {
                let date: Date = new Date(this.dash.dataFact.reportDate);
                date.setDate(date.getDate() - 1);
                this.httpService.getData('Indicator', 'DashboardRatings', [this.dash.id, date.toUTCString(), this.dash.dataFact.measure.id, this.dash.dataFact.period.id, this.dash.dataFact.department.id]).pipe(map(data => data as any)).subscribe(
                    data => {
                        this.Ratings = data;
                    });
            }
    }

    customizeTooltip(arg) {
        return {
            text: moment(arg.argument as Date).format("DD.MM.YYYY") + "<br/>" + Math.round(arg.value)
        }
    }
    GetDepartments() {
        this.onDepartmentsLoad.emit(this.dash);
    }
    GetDrivers() {
        this.onDriversLoad.emit(this.dash);
    }
}
