import { Component } from '@angular/core';

import { locale, loadMessages } from 'devextreme/localization';
import ruMessages from 'devextreme/localization/messages/ru.json';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    preserveWhitespaces: false
})
export class AppComponent {

    constructor() {
        loadMessages(ruMessages);
        locale("ru");
    }
}