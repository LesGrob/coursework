using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Cube.Models;
using Cube.DTO;
namespace Cube.Controllers.API
{
    [Route("api/[controller]/[action]")]
    public class PlanController : Controller
    {
        private readonly AppDbContext context;

        public PlanController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpGet("{ProjectID}")]
        public object Plans(long ProjectID)
        {
            return context.Plans.Include(i => i.Project).Where(i => i.Project.Id == ProjectID).ToArray();
        }

        [HttpPost]
        public object Plan([FromBody] Plan _plan)
        {
            context.Plans.Add(_plan);
            context.SaveChanges();

            return Json(_plan);
        }

        /// <summary>
        /// Autocomplete для отображения планов
        /// </summary>
        /// <param name="value">Подстрока для поиска</param>
        /// <param name="ProjectId">Идентификатор Project в котором ищется</param>
        /// <param name="OptionalTopAmount">Количество отображения n записей, если пользователь ничего не ввел</param>
        /// <returns>Коллекция планов, удовлетворяющая условиям</returns>
        [HttpGet("{ProjectId}")]
        [ProducesResponseType(typeof(List<UniversalAutocomplete_DTO>), 200)]
        public IActionResult AutocompletePlan(string value, long ProjectId, int OptionalTopAmount = 5)
        {
            List<UniversalAutocomplete_DTO> plans = null;
            if (string.IsNullOrEmpty(value))
                plans = context.Plans.Include(x => x.Project).Where(x => x.Project.Id == ProjectId).Take(OptionalTopAmount)
                    .Select(x => new UniversalAutocomplete_DTO(x)).ToList();
            else
                plans = context.Plans.Include(x => x.Project).Where(x => x.Project.Id == ProjectId && x.Name.ToLower().Contains(value.ToLower()))
                    .Select(x => new UniversalAutocomplete_DTO(x)).ToList();

            return Json(plans);
        }
    }
}
