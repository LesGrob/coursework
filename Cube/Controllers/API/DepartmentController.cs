using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Cube.Models;

namespace Cube.Controllers.API
{
    [Route( "api/[controller]/[action]" )]
    public class DepartmentController : Controller
    {
        private readonly AppDbContext context;

        public DepartmentController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpGet( "{ProjectID}" )]
        public object Departments(long ProjectID)
        {
            return context.Departments.Include( i => i.Project ).Where( i => i.Project.Id == ProjectID ).ToArray();
        }

        [HttpPost]
        public object Department([FromBody] Department _departments)
        {
            context.Departments.Add( _departments );
            context.SaveChanges();

            return Json( _departments );
        }


        [HttpGet( "{ProjectID}" )]
        public object DepartmentTypes(long ProjectID)
        {
            return context.DepartmentTypes.ToArray();
            //return context.Periods.Include( i => i.Project ).Where( i => i.Project.Id == ProjectID ).ToArray();
        }

        [HttpPost]
        public object DepartmentType([FromBody] DepartmentType _departmentType)
        {
            context.DepartmentTypes.Add( _departmentType );
            context.SaveChanges();

            return Json( _departmentType );
        }

        [HttpGet( "{value}/{ProjectID}" )]
        public object AutocompleteDepartments(string value, long ProjectID)
        {
            if (value == null)
                return null;
            return context.Departments.Include( x => x.Project )
                          .Where( x => x.Project.Id == ProjectID && x.Name.ToLower().Contains( value.ToLower() ) ).ToList();
        }
    }
}
