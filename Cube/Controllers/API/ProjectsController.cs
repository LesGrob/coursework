using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Cube.Models;
using Cube.Services;

namespace Cube.Controllers.API
{
    [Route("api/[controller]/[action]")]
    public class ProjectsController : Controller
    {
        private AppDbContext context;
        private ErrorStatus ErrorStatus;

        public ProjectsController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpGet]
        public List<Project> Projects()
        {
            return context.Projects.ToList();
        }

        [HttpGet("{id}")]
        public object Project(long id)
        {
            return context.Projects.Find(id);
        }

        [HttpPost]
        public object Project([FromBody]Project project)
        {
            project.UID = Guid.NewGuid();
            context.Projects.Add(project);

            context.SaveChanges();

            context.Measures.AddRange(new Measure { Project = project, Code = "rur", Name = "Рубли" },
                                      new Measure { Project = project, Code = "unit", Name = "Штуки" },
                                      new Measure { Project = project, Code = "percent", Name = "Проценты" });

            context.Plans.AddRange(new Plan { Project = project, Code = "basic", Name = "Базовый" },
                                   new Plan { Project = project, Code = "stress", Name = "Стресс план" });

            context.Periods.AddRange(new Period { Project = project, Code = "day", Name = "День" },
                                     new Period { Project = project, Code = "week", Name = "Неделя" },
                                     new Period { Project = project, Code = "mounth", Name = "Месяц" });

            context.DepartmentTypes.AddRange(new DepartmentType { Project = project, Code = "division", Name = "Отдел" },
                                             new DepartmentType { Project = project, Code = "branch", Name = "Точка продажи" });

            context.IndicatorTypes.AddRange(new IndicatorType { Project = project, Code = "direct", Name = "Прямой" },
                                            new IndicatorType { Project = project, Code = "inverse", Name = "Обратный" });

            context.PostTypes.AddRange(new PostType { Project = project, Code = "presenter", Name = "Презентационная должность" });

            context.Posts.AddRange(new Post { Project = project, Name = "Презентатор", Type = context.PostTypes.FirstOrDefault(), Department = context.Departments.FirstOrDefault(), DateOpen = DateTime.Now });

            context.SaveChanges();
            return project;
        }

        [HttpPost]
        public object RenameProject([FromBody] Project project)
        {
            Project _project = context.Projects.FirstOrDefault(x => x.Id == project.Id);
            _project.Name = project.Name;
            context.SaveChanges();
            return _project;
        }
    }
}
