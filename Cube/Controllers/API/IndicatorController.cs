using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Cube.Models;
using Cube.DTO;
using Cube.Services;

namespace Cube.Controllers.API
{
    [Route("api/[controller]/[action]")]
    public class IndicatorController : Controller
    {
        private readonly AppDbContext context;
        IndicatorService IndicatorService;

        public IndicatorController(AppDbContext dbContext)
        {
            context = dbContext;
            IndicatorService = new IndicatorService(context);
        }

        [HttpGet("{ProjectID}")]
        public object Indicators(long ProjectID)
        {
            List<Indicator_DTO> Indicators = context.Indicators.Include(i => i.Project).Include(i => i.MeasureDefault).Include(i => i.PeriodDefault).Include(i => i.Type).Include(i => i.PlanDefault).Include(i => i.Owner).Include(i => i.Parent)
                .Where(x => x.Project.Id == ProjectID).Select(x => new Indicator_DTO(x)).ToList();

            foreach (var item in Indicators)
            {
                item.DataFact = context.DataFacts.FirstOrDefault(x => x.Indicator.Id == item.Id);
                item.DataPlan = context.DataPlans.FirstOrDefault(x => x.Indicator.Id == item.Id);
            }

            return Indicators;
        }

        [HttpPost]
        public object Indicator([FromBody] Indicator _indicator)
        {
            Indicator indicator = new Indicator();

            indicator.Name = _indicator.Name;
            indicator.Type = context.IndicatorTypes.FirstOrDefault(x => x.Id == _indicator.Type.Id);
            indicator.Parent = context.Indicators.FirstOrDefault(x => x.Id == _indicator.Parent.Id);
            indicator.Owner = context.Departments.FirstOrDefault(x => x.Id == _indicator.Owner.Id);
            indicator.MeasureDefault = context.Measures.FirstOrDefault(x => x.Id == _indicator.MeasureDefault.Id);
            indicator.PeriodDefault = context.Periods.FirstOrDefault(x => x.Id == _indicator.PeriodDefault.Id);
            indicator.PlanDefault = context.Plans.FirstOrDefault(x => x.Id == _indicator.PlanDefault.Id);
            indicator.Project = context.Projects.FirstOrDefault(x => x.Id == _indicator.Project.Id);

            context.Indicators.Add(indicator);
            context.SaveChanges();

            return Json(indicator);
        }

        [HttpPost("{IndicatorId}")]
        public object RemoveIndicator(long IndicatorId)
        {
            Indicator indicator = context.Indicators.Include(x => x.Parent).FirstOrDefault(x => x.Id == IndicatorId);
            if (indicator != null)
            {
                List<Indicator> children = context.Indicators.Include(x => x.Parent).Where(x => x.Parent.Id == IndicatorId).ToList();
                foreach (var child in children)
                    child.Parent = indicator.Parent;

                context.UserShelf.RemoveRange(context.UserShelf.Include(x => x.Indicator).Where(x => x.Indicator.Id == IndicatorId));

                //Remove data by indicator
                context.DataFacts.RemoveRange(context.DataFacts.Include(x => x.Indicator).Where(x => x.Indicator.Id == IndicatorId));
                context.DataPlans.RemoveRange(context.DataPlans.Include(x => x.Indicator).Where(x => x.Indicator.Id == IndicatorId));
                context.DataEmployee.RemoveRange(context.DataEmployee.Include(x => x.Indicator).Where(x => x.Indicator.Id == IndicatorId));

                context.Indicators.Remove(indicator);

                context.SaveChanges();
            }
            return indicator;
        }



        [HttpPost]
        public object IndicatorType([FromBody] IndicatorType _indicatorType)
        {
            context.IndicatorTypes.Add(_indicatorType);
            context.SaveChanges();

            return Json(_indicatorType);
        }

        [HttpGet("{ProjectID}")]
        public object IndicatorTypes(long ProjectID)
        {
            return context.IndicatorTypes.Include(i => i.Project).Where(i => i.Project.Id == ProjectID).ToArray();
        }



        [HttpGet("{IndicatorId}/{Date}/{MeasureId}/{PeriodId}/{DepartmentId}/{EmployeeId}")]
        public Indicator_DTO IndicatorData(long IndicatorId, DateTime Date, long MeasureId = 0, long PeriodId = 0, long DepartmentId = 0, long EmployeeId = 0)
        {
            return IndicatorService.IndicatorData(IndicatorId, Date, MeasureId, PeriodId, DepartmentId, EmployeeId);
        }



        [HttpGet("{ProjectID}")]
        public object FreeIndicators(long ProjectID)
        {
            return context.Indicators.Where(x => x.Project.Id == ProjectID).ToList();
            //List<Indicator> UserIndicators = context.UserShelf.Include( x => x.Shelf.Project )
            //    .Where( x => x.Shelf.Project.Id == ProjectID ).Select( x => x.Indicator ).ToList();
            //return context.Indicators.Where( x => x.Project.Id == ProjectID && !UserIndicators.Contains( x ) ).ToList();
        }



        [HttpGet("{ReportDate}/{ProjectId}/{IndicatorId}/{PlanId}/{MeasureId}/{PeriodId}")]
        public object DepartmentTableData(DateTime ReportDate, long ProjectId, long IndicatorId, long PlanId, long MeasureId, long PeriodId)
        {
            Indicator indicator = context.Indicators.Include(x => x.Type).FirstOrDefault(x => x.Id == IndicatorId);

            if (indicator == null)
                return null;

            return context.Departments.Where(x => x.Project.Id == ProjectId).Select(i => new
            {
                Id = i.Id,
                Name = i.Name,
                ParentId = i.Parent == null ? 0 : i.Parent.Id,
                Type = indicator.Type,
                DataFact = context.DataFacts.Where(x => x.Indicator.Id == IndicatorId && x.Measure.Id == MeasureId && x.Period.Id == PeriodId && x.Department.Id == i.Id && x.ReportDate == ReportDate.Date).OrderBy(x => x.Id).LastOrDefault(),
                DataPlan = context.DataPlans.OrderBy(x => x.ReportDate).LastOrDefault(x => x.Indicator.Id == IndicatorId && x.Measure.Id == MeasureId && x.Period.Id == PeriodId && x.Department.Id == i.Id && x.ReportDate <= ReportDate.Date)
            });
        }

        [HttpGet("{ReportDate}/{ProjectId}/{IndicatorId}/{DepartmentId}/{MeasureId}/{PeriodId}")]
        public object EmployeeTableData(DateTime ReportDate, long ProjectId, long IndicatorId, long DepartmentId, long MeasureId, long PeriodId)
        {
            return null;
            //List<EmployeePost> employeePosts = context.EmployeePosts.Include( x => x.Employee ).Include( x => x.Post.Department ).Where( x => x.Post.Department.Id == DepartmentId ).ToList();

            //List<Employee> employees = context.Employees.Include( x => x.Post.Post.Type ).Where( x => x.Project.Id == ProjectId && employeePosts.Select( i => i.Employee.Id ).Contains( x.Id ) ).ToList();
            //return employees.Select( i => new
            //{
            //    Id = i.Id,
            //    Name = i.Name,
            //    DataEmployee = context.DataEmployee.LastOrDefault( x => x.Employee.Id == i.Id && x.Indicator.Id == IndicatorId && x.Department.Id == DepartmentId && x.Measure.Id == MeasureId && x.Period.Id == PeriodId && x.ReportDate == ReportDate.Date ),
            //    StuffNorm = context.StuffNorms.OrderBy( x => x.Id ).LastOrDefault( x => x.Indicator.Id == IndicatorId && x.Department.Id == DepartmentId && x.Measure.Id == MeasureId && x.Period.Id == PeriodId && x.Post == i.Post.Post.Type && x.DateStart <= ReportDate.Date && ( x.DateEnd >= ReportDate.Date || x.DateEnd == DateTime.MinValue ) )
            //} );
        }



        [HttpPost]
        public object PlanFactData([FromBody] List<PlanFactData_DTO> DataList)
        {
            return IndicatorService.PlanFactData(DataList);
        }

        [HttpPost]
        public object EmployeePlanFactData([FromBody] List<DataEmployee_DTO> DataList)
        {
            return IndicatorService.EmployeePlanFactData(DataList);
        }

        [HttpPost]
        public object StuffNorm([FromBody] List<StuffNorm_DTO> DataList)
        {
            return IndicatorService.StuffNorm(DataList);
        }



        [HttpGet("{ProjectID}")]
        public object Shelves(long ProjectID)
        {
            List<Shelf_DTO> Shelves = context.Shelf.Include(x => x.Project).Where(x => x.Project.Id == ProjectID).Select(x => new Shelf_DTO(x)).ToList();

            foreach (var shelf in Shelves)
            {
                shelf.Indicators = new List<Indicator_DTO>();
                List<UserShelf> tempShelf = context.UserShelf.OrderBy(x => x.Id).Include(x => x.Indicator).Include(x => x.Measure).Include(x => x.Period).Include(x => x.Department).Include(x => x.Employee).Where(x => x.Shelf.Id == shelf.Id).ToList();
                foreach (var userShelf in tempShelf)
                {
                    Indicator_DTO temp = IndicatorData(userShelf.Indicator.Id,
                                                       (userShelf.Date.Date != DateTime.MinValue ? userShelf.Date : DateTime.Now),
                                                       (userShelf.Measure != null ? userShelf.Measure.Id : 0),
                                                       (userShelf.Period != null ? userShelf.Period.Id : 0),
                                                       (userShelf.Department != null ? userShelf.Department.Id : 0),
                                                       (userShelf.Employee != null ? userShelf.Employee.Id : 0));
                    temp.UserShelfId = userShelf.Id;
                    shelf.Indicators.Add(temp);
                }

            }
            return Shelves;

        }

        [HttpPost]
        public object UserShelfChangeData([FromBody] UserShelf indicatorData)
        {
            if (indicatorData == null)
                throw new Exception("Input data was null");

            UserShelf userShelf = context.UserShelf.Include(x => x.Department).FirstOrDefault(i => i.Id == indicatorData.Id);

            if (userShelf != null && indicatorData.Id != 0)
            {
                userShelf.Measure = context.Measures.FirstOrDefault(x => x.Id == indicatorData.Measure.Id);
                userShelf.Period = context.Periods.FirstOrDefault(x => x.Id == indicatorData.Period.Id);
                userShelf.Department = context.Departments.FirstOrDefault(x => x.Id == indicatorData.Department.Id);
                userShelf.Employee = context.Employees.FirstOrDefault(x => x.Id == indicatorData.Employee.Id);
                userShelf.Date = indicatorData.Date;
                context.SaveChanges();
            }

            Indicator_DTO indicator = IndicatorData(indicatorData.Indicator.Id, indicatorData.Date, indicatorData.Measure.Id, indicatorData.Period.Id, indicatorData.Department.Id, indicatorData.Employee.Id);
            indicator.UserShelfId = indicatorData.Id;

            return indicator;
        }



        [HttpPost("{ProjectID}/{Name}/{shelfId}")]
        public object Shelf(long ProjectID, string Name, long shelfId)
        {
            Shelf shelf;

            if (shelfId != 0)
            {
                shelf = context.Shelf.FirstOrDefault(x => x.Id == shelfId && x.Project.Id == ProjectID);
                if (shelf != null)
                    shelf.Name = Name;
            }
            else
            {
                shelf = new Shelf();
                shelf.Project = context.Projects.FirstOrDefault(x => x.Id == ProjectID);
                shelf.Name = Name;

                if (shelf.Project == null) return null;

                context.Shelf.Add(shelf);
            }
            context.SaveChanges();
            return new Shelf_DTO(shelf);
        }

        [HttpPost("{ShelfId}/{IndicatorId}")]
        public Indicator_DTO ShelfIndicator(long ShelfId, long IndicatorId)
        {
            Indicator indicDefaults = context.Indicators.Include(x => x.MeasureDefault).Include(x => x.PlanDefault).Include(x => x.PeriodDefault).Include(x => x.Owner)
                .FirstOrDefault(x => x.Id == IndicatorId);
            if (indicDefaults == null)
                throw new Exception("No Indicators with such Id");

            Indicator_DTO indicator = IndicatorData(IndicatorId, DateTime.MinValue, indicDefaults.MeasureDefault.Id, indicDefaults.PeriodDefault.Id, indicDefaults.Owner.Id);

            UserShelf shelfIndicator = new UserShelf();
            shelfIndicator.Shelf = context.Shelf.FirstOrDefault(x => x.Id == ShelfId);
            shelfIndicator.Indicator = context.Indicators.FirstOrDefault(x => x.Id == IndicatorId);

            if (shelfIndicator.Shelf == null || shelfIndicator.Indicator == null)
                return null;

            if (indicator.DataFact != null)
            {
                shelfIndicator.Measure = indicator.DataFact.Measure;
                shelfIndicator.Period = indicator.DataFact.Period;
                shelfIndicator.Date = indicator.DataFact.ReportDate;
                shelfIndicator.Department = indicator.DataFact.Department;
            }

            context.UserShelf.Add(shelfIndicator);
            context.SaveChanges();

            indicator.UserShelfId = shelfIndicator.Id;

            return indicator;
        }

        [HttpPost("{IndicatorId}")]
        public object RemoveShelfIndicator(long IndicatorId)
        {
            UserShelf userShelf = context.UserShelf.FirstOrDefault(x => x.Id == IndicatorId);

            if (userShelf != null)
            {
                context.UserShelf.Remove(userShelf);
                context.SaveChanges();
            }

            return new object();
        }

        [HttpPost("{ShelfId}")]
        public object RemoveShelf(long ShelfId)
        {
            Shelf shelf = context.Shelf.FirstOrDefault(x => x.Id == ShelfId);

            if (shelf != null)
            {
                foreach (var indicator in context.UserShelf.Include(x => x.Shelf).Where(x => x.Shelf.Id == shelf.Id))
                    context.UserShelf.Remove(indicator);

                context.Shelf.Remove(shelf);
                context.SaveChanges();
            }

            return new object();
        }

        [HttpGet("{IndicatorId}/{Type}/{Value}")]
        public object ShelfIndicatorRepository(long IndicatorId, string Type = null, string Value = null)
        {
            DateTime Date = default(DateTime);
            long MeasureId = 0, PeriodId = 0, DepartmentId = 0, EmployeeId = 0;

            if (Type != null)
                //foreach (IndicatorParam param in IndicatorParams)
                switch (Type)
                {
                    case "d":
                        DepartmentId = long.Parse(Value);
                        break;
                    case "m":
                        MeasureId = long.Parse(Value);
                        break;
                    case "p":
                        PeriodId = long.Parse(Value);
                        break;
                    case "e":
                        EmployeeId = long.Parse(Value);
                        break;
                    case "date":
                        Date = DateTime.Parse(Value);
                        break;
                    default:
                        break;
                }


            List<Department> departments = context.DataFacts.Include(x => x.Indicator)
                .Where(x => x.Indicator.Id == IndicatorId
                       && (MeasureId != 0 ? x.Measure.Id == MeasureId : true)
                       && (PeriodId != 0 ? x.Period.Id == PeriodId : true)
                       && (Date != default(DateTime) ? x.ReportDate.Date == Date.Date : true))
                .Select(x => x.Department).Distinct().ToList();

            List<Measure> measures = context.DataFacts.Include(x => x.Indicator)
                .Where(x => x.Indicator.Id == IndicatorId
                       && (DepartmentId != 0 ? x.Department.Id == DepartmentId : true)
                       && (PeriodId != 0 ? x.Period.Id == PeriodId : true)
                       && (Date != default(DateTime) ? x.ReportDate.Date == Date.Date : true))
                .Select(x => x.Measure).Distinct().ToList();

            List<Period> periods = context.DataFacts.Include(x => x.Indicator)
                .Where(x => x.Indicator.Id == IndicatorId
                       && (DepartmentId != 0 ? x.Department.Id == DepartmentId : true)
                       && (MeasureId != 0 ? x.Measure.Id == MeasureId : true)
                       && (Date != default(DateTime) ? x.ReportDate.Date == Date.Date : true))
                .Select(x => x.Period).Distinct().ToList();

            List<DateTime> dates = context.DataFacts.Include(x => x.Indicator)
                .Where(x => x.Indicator.Id == IndicatorId
                       && (DepartmentId != 0 ? x.Department.Id == DepartmentId : true)
                       && (MeasureId != 0 ? x.Measure.Id == MeasureId : true)
                       && (PeriodId != 0 ? x.Period.Id == PeriodId : true))
                .Select(x => x.ReportDate).GroupBy(x => x.Date).Select(x => x.Last()).Distinct().OrderBy(x => x.Date).ToList();


            List<Employee> employees = context.DataEmployee
                .Where(x => x.Indicator.Id == IndicatorId
                        && (DepartmentId != 0 ? x.Department.Id == DepartmentId : departments.IndexOf(x.Department) != -1)
                        && (MeasureId != 0 ? x.Measure.Id == MeasureId : true)
                        && (PeriodId != 0 ? x.Period.Id == PeriodId : true)
                        && (Date != default(DateTime) ? x.ReportDate == Date.Date : true))
                .Select(x => x.Employee).Distinct().ToList();
            //context.Employees.Include( x => x.Post ).Include( x => x.Post.Department )
            //.Where( x => x.Project.Id == context.Indicators.FirstOrDefault( i => i.Id == IndicatorId ).Project.Id
            //        && departments.IndexOf( x.Post.Department ) != -1).Distinct().ToList();

            return new
            {
                Departments = departments,
                Measures = measures,
                Periods = periods,
                Dates = dates,
                Employees = employees
            };
        }

        /// <summary>
        /// Get child Indicators from Parent ( by department ) on same layer
        /// </summary>
        /// <param name="IndicatorId"></param>
        /// <param name="Date"></param>
        /// <param name="MeasureId"></param>
        /// <param name="PeriodId"></param>
        /// <param name="DepartmentId"></param>
        /// <param name="ShelfId"></param>
        /// <returns></returns>
        [HttpGet("{IndicatorId}/{Date}/{MeasureId}/{PeriodId}/{DepartmentId}/{ShelfId}")]
        public List<Indicator_DTO> IndicatorByDepartment(long IndicatorId, DateTime Date, long MeasureId, long PeriodId, long DepartmentId, long ShelfId)
        {
            Shelf shelf = context.Shelf.FirstOrDefault(x => x.Id == ShelfId);
            Measure measure = context.Measures.FirstOrDefault(x => x.Id == MeasureId);
            Period period = context.Periods.FirstOrDefault(x => x.Id == PeriodId);
            Indicator indicator = context.Indicators.FirstOrDefault(x => x.Id == IndicatorId);

            if (shelf == null || measure == null || period == null || indicator == null)
                throw new Exception("Invalid Id params");

            var temp = context.DataFacts.Include(x => x.Department.Parent).Include(x => x.Measure).Include(x => x.Period).Include(x => x.Indicator)
                .Where(x =>
                    x.Indicator.Id == IndicatorId &&
                    x.Measure.Id == MeasureId &&
                    x.Period.Id == PeriodId &&
                    x.ReportDate == Date.Date &&
                    x.Department.Parent.Id == DepartmentId).ToList();
            List<Department> departments = temp.GroupBy(x => x.Department).Distinct().Select(x => x.FirstOrDefault().Department).ToList();

            if (departments.Count < 1)
                return null;

            List<Indicator_DTO> indicators = new List<Indicator_DTO>();
            UserShelf tempUserShelf = new UserShelf();
            Indicator_DTO tempIndicator = new Indicator_DTO();

            foreach (var dep in departments)
            {
                tempUserShelf = new UserShelf() { Shelf = shelf, Department = dep, Period = period, Measure = measure, Date = Date, Indicator = indicator };
                context.UserShelf.Add(tempUserShelf);
                context.SaveChanges();

                tempIndicator = IndicatorData(IndicatorId, Date, MeasureId, PeriodId, dep.Id, 0);
                tempIndicator.UserShelfId = tempUserShelf.Id;
                indicators.Add(tempIndicator);
            }


            return indicators;
        }

        /// <summary>
        /// Get child Indicators from Parent ( by indicator ) on same layer
        /// </summary>
        /// <param name="IndicatorId"></param>
        /// <param name="Date"></param>
        /// <param name="DepartmentId"></param>
        /// <param name="ShelfId"></param>
        /// <returns></returns>
        [HttpGet("{IndicatorId}/{Date}/{DepartmentId}/{ShelfId}")]
        public List<Indicator_DTO> IndicatorByParent(long IndicatorId, DateTime Date, long DepartmentId, long ShelfId)
        {
            Shelf shelf = context.Shelf.FirstOrDefault(x => x.Id == ShelfId);
            Indicator indicator = context.Indicators.FirstOrDefault(x => x.Id == IndicatorId);
            Department department = context.Departments.FirstOrDefault(x => x.Id == DepartmentId);

            if (shelf == null || indicator == null || department == null)
                throw new Exception("Invalid Id params");


            List<Indicator> indicators = context.Indicators.Include(x => x.MeasureDefault).Include(x => x.PeriodDefault).Where(x => x.Parent.Id == IndicatorId).ToList();

            if (indicators.Count < 1)
                return null;

            UserShelf tempUserShelf = new UserShelf();
            List<Indicator_DTO> indicatorsData = new List<Indicator_DTO>();
            Indicator_DTO tempIndicator;

            foreach (var indic in indicators)
            {
                tempUserShelf = new UserShelf() { Shelf = shelf, Department = department, Period = indic.PeriodDefault, Measure = indic.MeasureDefault, Date = Date, Indicator = indic };
                context.UserShelf.Add(tempUserShelf);
                context.SaveChanges();

                tempIndicator = IndicatorData(indic.Id, Date, indic.MeasureDefault.Id, indic.PeriodDefault.Id, department.Id, 0);
                tempIndicator.UserShelfId = tempUserShelf.Id;

                indicatorsData.Add(tempIndicator);
            }


            return indicatorsData;
        }


        [HttpGet("{IndicatorId}/{Date}/{MeasureId}/{PeriodId}/{DepartmentId}")]
        public object DashboardChartData(long IndicatorId, DateTime Date, long MeasureId, long PeriodId, long DepartmentId)
        {

            List<DataFact_DTO> facts = context.DataFacts.Include(x => x.Department).Include(x => x.Measure).Include(x => x.Period).Include(x => x.Indicator)
                .OrderByDescending(x => x.ReportDate)
                    .Where(x => x.Indicator.Id == IndicatorId
                                   && (MeasureId != 0 ? x.Measure.Id == MeasureId : true)
                                   && (PeriodId != 0 ? x.Period.Id == PeriodId : true)
                                   && (DepartmentId != 0 ? x.Department.Id == DepartmentId : true)
                                   && (x.ReportDate.Date <= Date.Date))
                    .Select(x => new DataFact_DTO(x))
                    .GroupBy(x => x.ReportDate.Date).Distinct().Select(x => x.Last()).Take(20).ToList();


            List<PlanFactData_DTO> dates = new List<PlanFactData_DTO>();

            foreach (var item in facts)
            {
                var plan = context.DataPlans.Include(x => x.Department).Include(x => x.Measure).Include(x => x.Period).Include(x => x.Indicator)
                        .OrderByDescending(x => x.ReportDate).LastOrDefault(x =>
                         x.Indicator.Id == IndicatorId
                         && x.Measure.Id == MeasureId
                         && x.Period.Id == PeriodId
                         && x.Department.Id == DepartmentId
                         && x.ReportDate.Date <= item.ReportDate.Date
                         );
                dates.Add(new PlanFactData_DTO()
                {
                    FactValue = item.Value,
                    PlanValue = plan != null ? plan.Value : 0,
                    ReportDate = item.ReportDate
                });
            }


            return dates;
        }

        [HttpGet("{IndicatorId}/{Date}/{MeasureId}/{PeriodId}/{DepartmentId}")]
        public object DashboardRatings(long IndicatorId, DateTime Date, long MeasureId, long PeriodId, long DepartmentId)
        {
            List<DataEmployee> data = context.DataEmployee.Include(x => x.Employee).Where(x =>
                   x.ReportDate.Date == Date.Date &&
                   x.Indicator.Id == IndicatorId &&
                   x.Measure.Id == MeasureId &&
                   x.Period.Id == PeriodId &&
                   x.Department.Id == DepartmentId
                 ).OrderByDescending(x => x.Value).ToList();

            if (data.Count == 0)
                return data;

            float average = data.Average(x => x.Value);
            data.ForEach(x => x.Value = x.Value / average * 100);

            return data.Take(5);
        }

        [HttpGet("{ProjectId}")]
        public List<IndicatorTree_DTO> IndicatorsTree(long ProjectId, long IndicatorId = 0)
        {
            List<IndicatorTree_DTO> indicators = context.Indicators.Include(x => x.Project).Include(x => x.Parent).Include(x => x.MeasureDefault).Include(x => x.PeriodDefault).Include(x => x.PlanDefault).Include(x => x.Type)
                .Where(x =>
                   x.Project.Id == ProjectId &&
                   (IndicatorId != 0 ? (x.Parent != null && x.Parent.Id == IndicatorId) : x.Parent == null)
                ).Select(x => new IndicatorTree_DTO(x)).ToList();

            if (indicators != null)
                foreach (var indicator in indicators)
                    indicator.Children = IndicatorsTree(ProjectId, indicator.Id);

            return indicators.OrderBy(x => x.Name).ToList();
        }



        [HttpPost]
        public Indicator IndicatorDefaults([FromBody] Indicator _indicator)
        {
            if (_indicator == null)
                throw new Exception("Nullable indicator in param");


            Indicator indicator = context.Indicators.Include(x => x.Parent).FirstOrDefault(x => x.Id == _indicator.Id);

            indicator.Parent = context.Indicators.FirstOrDefault(x => x.Id == _indicator.Parent.Id);
            indicator.Type = context.IndicatorTypes.FirstOrDefault(x => x.Id == _indicator.Type.Id);
            indicator.MeasureDefault = context.Measures.FirstOrDefault(x => x.Id == _indicator.MeasureDefault.Id);
            indicator.PlanDefault = context.Plans.FirstOrDefault(x => x.Id == _indicator.PlanDefault.Id);
            indicator.PeriodDefault = context.Periods.FirstOrDefault(x => x.Id == _indicator.PeriodDefault.Id);
            indicator.Owner = context.Departments.FirstOrDefault(x => x.Id == _indicator.Owner.Id);

            context.SaveChanges();

            return indicator;
            //return new IndicatorTree_DTO( indicator );
        }

        [HttpPost("{IndicatorId}/{resursive}/{MeasureId}/{PeriodId}/{PlanId}/{TypeId}/{DepartmentId}/{date}")]
        public object CalculateIndicator(long IndicatorId, Boolean resursive, long MeasureId, long PeriodId, long PlanId, long TypeId, long DepartmentId, DateTimeOffset date)
        {
            IndicatorService.CalculateIndicator(IndicatorId, resursive, MeasureId, PeriodId, PlanId, TypeId, DepartmentId, date);

            return Response.StatusCode;
        }


        /// <summary>
        /// Autocomplete для отображения индикаторов
        /// </summary>
        /// <param name="value">Подстрока для поиска</param>
        /// <param name="ProjectId">Идентификатор Project в котором ищется</param>
        /// <param name="OptionalTopAmount">Количество отображения первых n записей, если пользователь ничего не ввел</param>
        /// <returns>Коллекция индикаторов, удовлетворяющая условиям</returns>
        [HttpGet("{ProjectId}")]
        [ProducesResponseType(typeof(List<UniversalAutocomplete_DTO>), 200)]
        public IActionResult AutocompleteIndicators(string value, long ProjectId, int OptionalTopAmount = 5)
        {
            List<UniversalAutocomplete_DTO> indicators = null;
            if (string.IsNullOrEmpty(value))
                indicators = context.Indicators.Include(x => x.Project).Include(x => x.Parent)
                    .Where(x => x.Project.Id == ProjectId && x.Parent == null).Take(OptionalTopAmount).Select(x => new UniversalAutocomplete_DTO(x)).ToList();
            else
                indicators = context.Indicators.Include(x => x.Project).Where(x => x.Project.Id == ProjectId
                      && x.Name.ToLower().Contains(value.ToLower())).Select(x => new UniversalAutocomplete_DTO(x)).ToList();

            return Json(indicators);
        }
    }

}
