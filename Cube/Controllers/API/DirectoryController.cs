using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Cube.Models;

namespace Cube.Controllers.API
{
    [Route( "api/[controller]/[action]" )]
    public class DirectoryController
    {
        private readonly AppDbContext context;

        public DirectoryController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        public void ListValidation<T>(List<T> list)
        {
            if (list == null)
                throw new Exception( "Incorrect data!" );

            if (list.Count == 0)
                throw new Exception( "Empty data!" );
        }

        [HttpPost]
        public object PostType([FromBody] List<PostType> postTypes)
        {
            ListValidation( postTypes );

            foreach (PostType type in postTypes)
                context.PostTypes.Add( type );

            context.SaveChanges();

            return postTypes;
        }

        [HttpPost]
        public object Departments([FromBody] List<Department> departments)
        {
            ListValidation( departments );

            foreach (Department depart in departments)
            {
                depart.Type = context.DepartmentTypes.FirstOrDefault( x => x.Id == depart.Type.Id );
                depart.Project = context.Projects.FirstOrDefault( x => x.Id == depart.Project.Id );
                context.Departments.Add( depart );
            }

            context.SaveChanges();

            return departments;
        }

        [HttpGet("{ProjectId}")]
        public object Employees(long ProjectId)
        {
            return context.Employees.Include( x => x.Project ).Where( x => x.Project.Id == ProjectId ).ToArray();
        }

        public class EmployeeShort
        {
            public string Name { get; set; }
            public string Department { get; set; }
        }

        [HttpPost]
        public object Employees([FromBody] List<EmployeeShort> EmployeeShort)
        {

            Department dep;
            Post post;
            Employee empl;
            foreach (var employee in EmployeeShort.OrderBy( x => x.Name ))
            {
                dep = context.Departments.Include( x => x.Project ).FirstOrDefault( x => x.Name == employee.Department );
                post = context.Posts.FirstOrDefault( x => x.Department == dep );
                empl = context.Employees.LastOrDefault( x => x.Name == employee.Name );
                if (empl == null)
                {
                    empl = new Employee()
                    {
                        Name = employee.Name,
                        //Post = post,
                        TabNum = "001",
                        Project = context.Projects.FirstOrDefault( x => x == dep.Project )
                    };
                    context.Employees.Add( empl );
                }

                context.EmployeePosts.Add( new EmployeePost { Employee = empl, Post = post, DateOpen = DateTimeOffset.MinValue } );
                context.SaveChanges();
            }

            return EmployeeShort;
        }
    }
}
