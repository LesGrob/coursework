using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Cube.Models;
using Cube.DTO;
using Microsoft.AspNetCore.Authorization;

namespace Cube.Controllers.API
{
    [Authorize( Policy = "WhiteTokensOnly" )]
    [Route( "api/[controller]/[action]" )]
    public class StaffController : Controller
    {
        private readonly AppDbContext context;

        public StaffController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        public class EmployeePostId
        {
            public long Id { get; set; }
            public EmployeePostId(long id) { Id = id; }
        }


        [HttpGet( "{ProjectID}" )]
        public List<StaffTreeItem_DTO> StaffData(long ProjectID, long DepartmentParentId = 0)
        {
            List<StaffTreeItem_DTO> data = new List<StaffTreeItem_DTO>();

            //foreach (var department in context.Departments.Include( x => x.Type ).Where( x => x.Project.Id == ProjectID && ( DepartmentParentId == 0 ? x.Parent == null : x.Parent.Id == DepartmentParentId ) ).Select( x => new StaffTreeItem_DTO( x ) ))
            //{
            //    department.Children = new List<StaffTreeItem_DTO>();
            //    department.Children.AddRange( StaffData( ProjectID, department.Id ) );
            //    data.Add( department );
            //}
            //foreach (var post in context.Posts.Include( x => x.Type ).Where( x => x.Project.Id == ProjectID && x.Department.Id == DepartmentParentId ).Select( x => new StaffTreeItem_DTO( x ) ))
            //{
            //    post.Children = context.EmployeePosts.Where( x => x.Post.Id == post.Id )
            //        .GroupBy( x => x.Employee ).Distinct().Select( x => new StaffTreeItem_DTO( x.FirstOrDefault().Employee ) ).ToList();
            //    data.Add( post );
            //}

            data.AddRange( context.Departments.Include( x => x.Parent ).Include( x => x.Type ).Where( x => x.Project.Id == ProjectID ).Select( x => new StaffTreeItem_DTO( x ) ) );
            data.AddRange( context.Posts.Include( x => x.Department ).Include( x => x.Type ).Where( x => x.Project.Id == ProjectID && x.Department != null ).Select( x => new StaffTreeItem_DTO( x ) ) );
            data.AddRange( context.EmployeePosts.Include( x => x.Employee ).Include( x => x.Post.Project ).Where( x => x.Post.Project.Id == ProjectID )
                    .GroupBy( x => x.Employee ).Distinct().Select( x => new StaffTreeItem_DTO( x.FirstOrDefault() ) ) );

            foreach (var depart in data.Where( x => x.Code == "Department" ))
                depart.ParentUID = data.Any( x => x.Code == "Department" && x.Id == depart.ParentId ) ? data.FirstOrDefault( x => x.Code == "Department" && x.Id == depart.ParentId ).UID.ToString() : null;
            foreach (var post in data.Where( x => x.Code == "Post" ))
                post.ParentUID = data.Any( x => x.Code == "Department" && x.Id == post.ParentId ) ? data.FirstOrDefault( x => x.Code == "Department" && x.Id == post.ParentId ).UID.ToString() : null;
            foreach (var employee in data.Where( x => x.Code == "Employee" ))
                employee.ParentUID = data.Any( x => x.Code == "Post" && x.Id == employee.ParentId ) ? data.FirstOrDefault( x => x.Code == "Post" && x.Id == employee.ParentId ).UID.ToString() : null;

            return data;
        }

        [HttpGet( "{ProjectID}/{ObjectType}" )]
        public object GetStaffTypes(long ProjectID, string ObjectType)
        {
            switch (ObjectType)
            {
                case "Department": return context.DepartmentTypes.Where( x => x.Project.Id == ProjectID ).ToList();
                case "Post": return context.PostTypes.Where( x => x.Project.Id == ProjectID ).ToList();
                default: return null;
            }
        }

        [HttpPost]
        public object ChangeStaffItem([FromBody] StaffTreeItem_DTO item)
        {
            if (item.Code == "Department")
            {
                Department dep = context.Departments.FirstOrDefault( x => x.Id == item.Id );
                if (dep != null)
                {
                    dep.Name = item.Name;
                    dep.Parent = context.Departments.FirstOrDefault( x => x.Id == item.ParentId );
                    dep.Type = context.DepartmentTypes.FirstOrDefault( x => x.Id == item.Type.Id );
                    dep.DateStart = item.DateOpen;
                    dep.DateEnd = item.DateClose;
                    context.SaveChanges();
                }
                return dep;
            }
            if (item.Code == "Post")
            {
                Post post = context.Posts.FirstOrDefault( x => x.Id == item.Id );
                if (post != null)
                {
                    post.Name = item.Name;
                    post.Department = context.Departments.FirstOrDefault( x => x.Id == item.ParentId );
                    post.Type = context.PostTypes.FirstOrDefault( x => x.Id == item.Type.Id );
                    post.DateOpen = item.DateOpen;
                    post.DateClose = item.DateClose;
                    context.SaveChanges();
                }
                return post;
            }
            if (item.Code == "Employee")
            {
                EmployeePost employeePost = context.EmployeePosts.Include( x => x.Employee ).Include( x => x.Post ).FirstOrDefault( x => x.Id == item.Id );
                if (employeePost != null)
                {
                    Employee employee = employeePost.Employee;
                    employee.Name = item.Name;

                    employeePost.Post = context.Posts.FirstOrDefault( x => x.Id == item.ParentId );
                    employeePost.DateOpen = item.DateOpen;
                    employeePost.DateClose = item.DateClose;
                    employeePost.Unit = item.Type.Value;
                    context.SaveChanges();
                }
                return employeePost;
            }
            return null;
        }

        [HttpPost]
        public object AddStaffItem([FromBody] StaffTreeItem_DTO item)
        {
            if (item.Code == "Department")
            {
                Department dep = new Department();
                dep.Name = item.Name;
                dep.Code = item.Code;
                dep.Parent = context.Departments.FirstOrDefault( x => x.Id == item.ParentId );
                dep.Type = context.DepartmentTypes.FirstOrDefault( x => x.Id == item.Type.Id );
                if (item.DateOpen != null)
                    dep.DateStart = item.DateOpen.Date;
                if (item.DateClose != null)
                    dep.DateEnd = item.DateClose.Value.Date;
                dep.Project = context.Projects.FirstOrDefault( x => x.Id == item.Project.Id );

                context.Departments.Add( dep );
                context.SaveChanges();

                return new StaffTreeItem_DTO( dep );
            }
            if (item.Code == "Post")
            {
                Post post = new Post();
                post.Name = item.Name;
                post.Department = context.Departments.FirstOrDefault( x => x.Id == item.ParentId );
                post.Type = context.PostTypes.FirstOrDefault( x => x.Id == item.Type.Id );
                if (item.DateOpen != null)
                    post.DateOpen = item.DateOpen.Date;
                if (item.DateClose != null)
                    post.DateClose = item.DateClose.Value.Date;
                post.Project = context.Projects.FirstOrDefault( x => x.Id == item.Project.Id );

                context.Posts.Add( post );
                context.SaveChanges();

                return new StaffTreeItem_DTO( post );
            }
            if (item.Code == "Employee")
            {
                Employee employee = new Employee();
                employee.Name = item.Name;
                if (item.DateOpen != null)
                    employee.DateOpen = item.DateOpen.Date;
                employee.Project = context.Projects.FirstOrDefault( x => x.Id == item.Project.Id );
                employee.TabNum = "007";
                context.Employees.Add( employee );
                context.SaveChanges();

                EmployeePost employeePost = new EmployeePost();
                employeePost.Employee = employee;
                employeePost.Post = context.Posts.FirstOrDefault( x => x.Id == item.ParentId );
                employeePost.Unit = item.Type.Value;
                if (item.DateOpen != null)
                    employeePost.DateOpen = item.DateOpen.Date;
                if (item.DateClose != null)
                    employeePost.DateClose = item.DateClose.Value.Date;

                context.EmployeePosts.Add( employeePost );
                context.SaveChanges();

                return new StaffTreeItem_DTO( employeePost );
            }
            return null;
        }


        [HttpGet( "{EmployeePostId}/{PostId}/{Value}" )]
        public bool CheckEmployeeUnit(long EmployeePostId, long PostId, double Value)
        {
            Post employeePost = context.Posts.Include( x => x.Type ).Include( x => x.Department ).FirstOrDefault( x => x.Id == PostId );
            if (employeePost != null)
            {
                double UnitSum = context.EmployeePosts.Where( x => x.Id != EmployeePostId && x.Post.Type == employeePost.Type && x.Post.Department == employeePost.Department ).Select( x => x.Unit ).Sum() + Value;
                if (UnitSum < 1.1 && UnitSum > 0)
                    return true;
            }
            return false;
        }
    }
}
