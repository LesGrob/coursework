using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Cube.Models;

namespace Cube.Controllers.API
{
    [Route( "api/[controller]/[action]" )]
    public class PeriodController : Controller
    {
        private readonly AppDbContext context;

        public PeriodController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpGet( "{ProjectID}" )]
        public object Periods(long ProjectID)
        {
            return context.Periods.Include( i => i.Project ).Where( i => i.Project.Id == ProjectID ).ToArray();
        }

        [HttpPost]
        public object Period([FromBody] Period _period)
        {
            context.Periods.Add( _period );
            context.SaveChanges();

            return Json( _period );
        }
    }
}
