using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Cube.Models;

namespace Cube.Controllers.API
{
    [Route( "api/[controller]/[action]" )]
    public class PostController : Controller
    {
        private readonly AppDbContext context;

        public PostController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpGet( "{ProjectID}" )]
        public object Posts(long ProjectID)
        {
            return context.Posts.Include( i => i.Project ).Where( i => i.Project.Id == ProjectID ).ToArray();
        }

        [HttpPost]
        public object Posts([FromBody] Post _posts)
        {
            context.Posts.Add( _posts );
            context.SaveChanges();

            return Json( _posts );
        }


        [HttpGet( "{ProjectID}" )]
        public object PostTypes(long ProjectID)
        {
            return context.PostTypes.ToArray();
            //return context.Periods.Include( i => i.Project ).Where( i => i.Project.Id == ProjectID ).ToArray();
        }

        [HttpPost]
        public object PostType([FromBody] PostType _postType)
        {
            context.PostTypes.Add( _postType );
            context.SaveChanges();

            return Json( _postType );
        }
    }
}
