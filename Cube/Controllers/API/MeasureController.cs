using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Cube.Models;

namespace Cube.Controllers.API
{
    [Route( "api/[controller]/[action]" )]
    public class MeasureController : Controller
    {
        private readonly AppDbContext context;

        public MeasureController(AppDbContext dbContext)
        {
            context = dbContext;
        }
        
        [HttpGet( "{ProjectID}" )]
        public object Measures(long ProjectID)
        {
            //return context.Measures.ToArray();
            return context.Measures.Include( i => i.Project ).Where( i => i.Project.Id == ProjectID ).ToArray();
        }

        [HttpPost]
        public object Measure([FromBody] Measure _measure)
        {
            context.Measures.Add( _measure );
            context.SaveChanges();

            return Json( _measure );
        }
    }
}
