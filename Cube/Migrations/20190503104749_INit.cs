﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Cube.Migrations
{
    public partial class INit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MathSigns",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Description = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MathSigns", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false),
                    UID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DepartmentTypes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DepartmentTypes_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Surname = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    Middlename = table.Column<string>(nullable: true),
                    DateOpen = table.Column<DateTimeOffset>(nullable: false),
                    DateClose = table.Column<DateTimeOffset>(nullable: true),
                    TabNum = table.Column<string>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employees_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "IndicatorTypes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndicatorTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IndicatorTypes_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Measures",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Measures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Measures_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Periods",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Periods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Periods_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Plans",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plans", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Plans_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PostTypes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostTypes_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Shelf",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false),
                    ProjectId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shelf", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Shelf_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Statuses",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Statuses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Statuses_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    DateStart = table.Column<DateTimeOffset>(nullable: false),
                    DateEnd = table.Column<DateTimeOffset>(nullable: true),
                    ParentId = table.Column<long>(nullable: true),
                    TypeId = table.Column<long>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Departments_Departments_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Departments_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Departments_DepartmentTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "DepartmentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Indicators",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    TypeId = table.Column<long>(nullable: false),
                    OwnerId = table.Column<long>(nullable: false),
                    ParentId = table.Column<long>(nullable: true),
                    MeasureDefaultId = table.Column<long>(nullable: false),
                    PeriodDefaultId = table.Column<long>(nullable: false),
                    PlanDefaultId = table.Column<long>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Indicators", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Indicators_Measures_MeasureDefaultId",
                        column: x => x.MeasureDefaultId,
                        principalTable: "Measures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Indicators_Departments_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Indicators_Indicators_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Indicators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Indicators_Periods_PeriodDefaultId",
                        column: x => x.PeriodDefaultId,
                        principalTable: "Periods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Indicators_Plans_PlanDefaultId",
                        column: x => x.PlanDefaultId,
                        principalTable: "Plans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Indicators_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Indicators_IndicatorTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "IndicatorTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false),
                    TypeId = table.Column<long>(nullable: false),
                    DepartmentId = table.Column<long>(nullable: false),
                    DateOpen = table.Column<DateTimeOffset>(nullable: false),
                    DateClose = table.Column<DateTimeOffset>(nullable: true),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Posts_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Posts_PostTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "PostTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DataEmployee",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Value = table.Column<float>(nullable: false),
                    ReportDate = table.Column<DateTime>(nullable: false),
                    MeasureId = table.Column<long>(nullable: true),
                    PeriodId = table.Column<long>(nullable: true),
                    DepartmentId = table.Column<long>(nullable: true),
                    IndicatorId = table.Column<long>(nullable: true),
                    EmployeeId = table.Column<long>(nullable: true),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataEmployee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DataEmployee_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DataEmployee_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DataEmployee_Indicators_IndicatorId",
                        column: x => x.IndicatorId,
                        principalTable: "Indicators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DataEmployee_Measures_MeasureId",
                        column: x => x.MeasureId,
                        principalTable: "Measures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DataEmployee_Periods_PeriodId",
                        column: x => x.PeriodId,
                        principalTable: "Periods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DataEmployee_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DataFacts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Value = table.Column<float>(nullable: false),
                    ReportDate = table.Column<DateTime>(nullable: false),
                    MeasureId = table.Column<long>(nullable: false),
                    PeriodId = table.Column<long>(nullable: false),
                    DepartmentId = table.Column<long>(nullable: false),
                    IndicatorId = table.Column<long>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataFacts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DataFacts_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DataFacts_Indicators_IndicatorId",
                        column: x => x.IndicatorId,
                        principalTable: "Indicators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DataFacts_Measures_MeasureId",
                        column: x => x.MeasureId,
                        principalTable: "Measures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DataFacts_Periods_PeriodId",
                        column: x => x.PeriodId,
                        principalTable: "Periods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DataFacts_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DataPlans",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Value = table.Column<float>(nullable: false),
                    ReportDate = table.Column<DateTime>(nullable: false),
                    MeasureId = table.Column<long>(nullable: false),
                    PeriodId = table.Column<long>(nullable: false),
                    DepartmentId = table.Column<long>(nullable: false),
                    IndicatorId = table.Column<long>(nullable: false),
                    PlanId = table.Column<long>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataPlans", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DataPlans_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DataPlans_Indicators_IndicatorId",
                        column: x => x.IndicatorId,
                        principalTable: "Indicators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DataPlans_Measures_MeasureId",
                        column: x => x.MeasureId,
                        principalTable: "Measures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DataPlans_Periods_PeriodId",
                        column: x => x.PeriodId,
                        principalTable: "Periods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DataPlans_Plans_PlanId",
                        column: x => x.PlanId,
                        principalTable: "Plans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DataPlans_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StuffNorms",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    PostId = table.Column<long>(nullable: false),
                    IndicatorId = table.Column<long>(nullable: false),
                    PeriodId = table.Column<long>(nullable: false),
                    MeasureId = table.Column<long>(nullable: false),
                    DepartmentId = table.Column<long>(nullable: false),
                    Value = table.Column<float>(nullable: false),
                    DateStart = table.Column<DateTime>(nullable: false),
                    DateEnd = table.Column<DateTime>(nullable: false),
                    ProjectId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StuffNorms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StuffNorms_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StuffNorms_Indicators_IndicatorId",
                        column: x => x.IndicatorId,
                        principalTable: "Indicators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StuffNorms_Measures_MeasureId",
                        column: x => x.MeasureId,
                        principalTable: "Measures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StuffNorms_Periods_PeriodId",
                        column: x => x.PeriodId,
                        principalTable: "Periods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StuffNorms_PostTypes_PostId",
                        column: x => x.PostId,
                        principalTable: "PostTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StuffNorms_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserShelf",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ShelfId = table.Column<long>(nullable: false),
                    IndicatorId = table.Column<long>(nullable: false),
                    MeasureId = table.Column<long>(nullable: true),
                    PeriodId = table.Column<long>(nullable: true),
                    DepartmentId = table.Column<long>(nullable: true),
                    EmployeeId = table.Column<long>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserShelf", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserShelf_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserShelf_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserShelf_Indicators_IndicatorId",
                        column: x => x.IndicatorId,
                        principalTable: "Indicators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserShelf_Measures_MeasureId",
                        column: x => x.MeasureId,
                        principalTable: "Measures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserShelf_Periods_PeriodId",
                        column: x => x.PeriodId,
                        principalTable: "Periods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserShelf_Shelf_ShelfId",
                        column: x => x.ShelfId,
                        principalTable: "Shelf",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EmployeePosts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    EmployeeId = table.Column<long>(nullable: false),
                    PostId = table.Column<long>(nullable: false),
                    DateOpen = table.Column<DateTimeOffset>(nullable: false),
                    DateClose = table.Column<DateTimeOffset>(nullable: true),
                    Unit = table.Column<float>(nullable: false),
                    MainPost = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeePosts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmployeePosts_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeePosts_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DataEmployee_DepartmentId",
                table: "DataEmployee",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_DataEmployee_EmployeeId",
                table: "DataEmployee",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_DataEmployee_IndicatorId",
                table: "DataEmployee",
                column: "IndicatorId");

            migrationBuilder.CreateIndex(
                name: "IX_DataEmployee_MeasureId",
                table: "DataEmployee",
                column: "MeasureId");

            migrationBuilder.CreateIndex(
                name: "IX_DataEmployee_PeriodId",
                table: "DataEmployee",
                column: "PeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_DataEmployee_ProjectId",
                table: "DataEmployee",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_DataFacts_DepartmentId",
                table: "DataFacts",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_DataFacts_IndicatorId",
                table: "DataFacts",
                column: "IndicatorId");

            migrationBuilder.CreateIndex(
                name: "IX_DataFacts_MeasureId",
                table: "DataFacts",
                column: "MeasureId");

            migrationBuilder.CreateIndex(
                name: "IX_DataFacts_PeriodId",
                table: "DataFacts",
                column: "PeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_DataFacts_ProjectId",
                table: "DataFacts",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_DataPlans_DepartmentId",
                table: "DataPlans",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_DataPlans_IndicatorId",
                table: "DataPlans",
                column: "IndicatorId");

            migrationBuilder.CreateIndex(
                name: "IX_DataPlans_MeasureId",
                table: "DataPlans",
                column: "MeasureId");

            migrationBuilder.CreateIndex(
                name: "IX_DataPlans_PeriodId",
                table: "DataPlans",
                column: "PeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_DataPlans_PlanId",
                table: "DataPlans",
                column: "PlanId");

            migrationBuilder.CreateIndex(
                name: "IX_DataPlans_ProjectId",
                table: "DataPlans",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Departments_ParentId",
                table: "Departments",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Departments_ProjectId",
                table: "Departments",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Departments_TypeId",
                table: "Departments",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentTypes_ProjectId",
                table: "DepartmentTypes",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeePosts_EmployeeId",
                table: "EmployeePosts",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeePosts_PostId",
                table: "EmployeePosts",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_ProjectId",
                table: "Employees",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Indicators_MeasureDefaultId",
                table: "Indicators",
                column: "MeasureDefaultId");

            migrationBuilder.CreateIndex(
                name: "IX_Indicators_OwnerId",
                table: "Indicators",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Indicators_ParentId",
                table: "Indicators",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Indicators_PeriodDefaultId",
                table: "Indicators",
                column: "PeriodDefaultId");

            migrationBuilder.CreateIndex(
                name: "IX_Indicators_PlanDefaultId",
                table: "Indicators",
                column: "PlanDefaultId");

            migrationBuilder.CreateIndex(
                name: "IX_Indicators_ProjectId",
                table: "Indicators",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Indicators_TypeId",
                table: "Indicators",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_IndicatorTypes_ProjectId",
                table: "IndicatorTypes",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Measures_ProjectId",
                table: "Measures",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Periods_ProjectId",
                table: "Periods",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Plans_ProjectId",
                table: "Plans",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_DepartmentId",
                table: "Posts",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_ProjectId",
                table: "Posts",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_TypeId",
                table: "Posts",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PostTypes_ProjectId",
                table: "PostTypes",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Shelf_ProjectId",
                table: "Shelf",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Statuses_ProjectId",
                table: "Statuses",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_StuffNorms_DepartmentId",
                table: "StuffNorms",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_StuffNorms_IndicatorId",
                table: "StuffNorms",
                column: "IndicatorId");

            migrationBuilder.CreateIndex(
                name: "IX_StuffNorms_MeasureId",
                table: "StuffNorms",
                column: "MeasureId");

            migrationBuilder.CreateIndex(
                name: "IX_StuffNorms_PeriodId",
                table: "StuffNorms",
                column: "PeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_StuffNorms_PostId",
                table: "StuffNorms",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_StuffNorms_ProjectId",
                table: "StuffNorms",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_UserShelf_DepartmentId",
                table: "UserShelf",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_UserShelf_EmployeeId",
                table: "UserShelf",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserShelf_IndicatorId",
                table: "UserShelf",
                column: "IndicatorId");

            migrationBuilder.CreateIndex(
                name: "IX_UserShelf_MeasureId",
                table: "UserShelf",
                column: "MeasureId");

            migrationBuilder.CreateIndex(
                name: "IX_UserShelf_PeriodId",
                table: "UserShelf",
                column: "PeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_UserShelf_ShelfId",
                table: "UserShelf",
                column: "ShelfId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DataEmployee");

            migrationBuilder.DropTable(
                name: "DataFacts");

            migrationBuilder.DropTable(
                name: "DataPlans");

            migrationBuilder.DropTable(
                name: "EmployeePosts");

            migrationBuilder.DropTable(
                name: "MathSigns");

            migrationBuilder.DropTable(
                name: "Statuses");

            migrationBuilder.DropTable(
                name: "StuffNorms");

            migrationBuilder.DropTable(
                name: "UserShelf");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Indicators");

            migrationBuilder.DropTable(
                name: "Shelf");

            migrationBuilder.DropTable(
                name: "PostTypes");

            migrationBuilder.DropTable(
                name: "Measures");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "Periods");

            migrationBuilder.DropTable(
                name: "Plans");

            migrationBuilder.DropTable(
                name: "IndicatorTypes");

            migrationBuilder.DropTable(
                name: "DepartmentTypes");

            migrationBuilder.DropTable(
                name: "Projects");
        }
    }
}
