using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Cube.Models;

namespace Cube.AuthorizationPolicies
{
    //A handler that can determine whether a token is in BlackList => token must be revoke
    public class IsWhiteTokenAuthorizationHandler : AuthorizationHandler<WhiteTokenRequirement>
    {
        AppDbContext DBContext;

        public IsWhiteTokenAuthorizationHandler(AppDbContext dbContext)
        {
            DBContext = dbContext;
        }

        protected override System.Threading.Tasks.Task HandleRequirementAsync(AuthorizationHandlerContext context, WhiteTokenRequirement requirement)
        {
            //Check user Token in database
            //Check has user claim with his identity email
            //if (!context.User.HasClaim( c => c.Type == ClaimTypes.Email ))
            //{
            //    //Пропускаем, так как невозможно определить пользователя исходя из токена - валидация не успешна
            //    return System.Threading.Tasks.Task.CompletedTask;
            //}
            ////Get user Email
            //string userEmail = context.User.FindFirst( c => c.Type == ClaimTypes.Email ).Value;
            ////Find user in DB
            //User user = DBContext.Users.FirstOrDefault( x => x.Email == userEmail );
            //if (user == null)
            //    return System.Threading.Tasks.Task.CompletedTask;
            ////Check user token is white or black. Если tokens содержит пользовательские токены => они белые
            //List<UserToken> tokens = DBContext.Tokens.Where( x => x.user.Id == user.Id ).ToList();

            ////Валидация переданного requirement успешна
            //if (tokens.Count > 0)
                //context.Succeed( requirement );

            return System.Threading.Tasks.Task.CompletedTask;
        }
    }

    //A custom authorization requirement which requires check list of tokens and say user token in black list or no
    public class WhiteTokenRequirement : IAuthorizationRequirement
    {
        //public string Description { get; private set; }

        //public TokenInBlackListRequirement(string description) {
        //  Description = description;
        //}

    }
}
