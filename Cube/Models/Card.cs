using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    /// <summary>
    /// Сущность карточка
    /// </summary>
    public class Card
    {
        public Card() { }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTimeOffset Date { get; set; }
        /// <summary>
        /// Создатель карточки
        /// </summary>
        public virtual User CreatorUser { get; set; }
        /// <summary>
        /// Текст карточки
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// Тип карточки
        /// </summary>
        public virtual CardType CardType { get; set; }
        /// <summary>
        /// Задача, к которой прикреплена карта
        /// </summary>
        public virtual Task Task { get; set; }
        /// <summary>
        /// Пользователь, которому выдана карта
        /// </summary>
        public virtual User TargetUser { get; set; }
    }
}
