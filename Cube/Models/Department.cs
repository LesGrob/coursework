using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    /// <summary>
    /// Сущность подразделение
    /// </summary>
    public class Department
    {
        public Department() { }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Код
        /// </summary>
        [Required]
        public string Code { get; set; }
        /// <summary>
        /// Наименование поздразделения
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Дата создания подразделения
        /// </summary>
        [Required]
        public DateTimeOffset DateStart { get; set; }
        /// <summary>
        /// Дата закрытия подразделения
        /// </summary>
        public DateTimeOffset? DateEnd { get; set; }
        /// <summary>
        /// Родительское подразделение
        /// </summary>
        public virtual Department Parent { get; set; }
        /// <summary>
        /// Тип подразделения
        /// </summary>
        [Required]
        public virtual DepartmentType Type { get; set; }
        /// <summary>
        /// WorkSpace
        /// </summary>
        public virtual Project Project { get; set; }
    }
}
