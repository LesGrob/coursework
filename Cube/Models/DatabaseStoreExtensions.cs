using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Npgsql;

namespace Cube.Models
{
    /// <summary>
    /// Зерно БД
    /// </summary>
    public static class DatabaseStoreExtensions
    {
        /// <summary>
        /// Заполнить БД базовыми значениями
        /// </summary>
        /// <param name="context">Контекст подключения к БД</param>
        public static void EnsureSeedData(this AppDbContext context)
        {

            //Measures
            if (!context.Measures.Any())
            {
                foreach (var project in context.Projects)
                {
                    context.Measures.AddRange(
                       new Measure { Project = project, Code = "rur", Name = "Рубли" },
                       new Measure { Project = project, Code = "unit", Name = "Штуки" },
                       new Measure { Project = project, Code = "percent", Name = "Проценты" });
                }
                context.SaveChanges();
            }


            //Plan
            if (!context.Plans.Any())
            {
                foreach (var project in context.Projects)
                {
                    context.Plans.AddRange(
                        new Plan { Project = project, Code = "basic", Name = "Базовый" },
                        new Plan { Project = project, Code = "stress", Name = "Стресс план" });
                };
                context.SaveChanges();
            }


            //Period
            if (!context.Periods.Any())
            {
                foreach (var project in context.Projects)
                {
                    context.Periods.AddRange(
                        new Period { Project = project, Code = "day", Name = "День" },
                        new Period { Project = project, Code = "week", Name = "Неделя" },
                        new Period { Project = project, Code = "mounth", Name = "Месяц" });
                };
                context.SaveChanges();
            }


            //Department type
            if (!context.DepartmentTypes.Any())
            {
                foreach (var project in context.Projects)
                {
                    context.DepartmentTypes.AddRange(
                        new DepartmentType { Project = project, Code = "division", Name = "Отдел" },
                        new DepartmentType { Project = project, Code = "branch", Name = "Точка продажи" });
                }
                context.SaveChanges();
            }


            //Indicator type
            if (!context.IndicatorTypes.Any())
            {
                foreach (var project in context.Projects)
                {
                    context.IndicatorTypes.AddRange(
                        new IndicatorType { Project = project, Code = "direct", Name = "Прямой" },
                        new IndicatorType { Project = project, Code = "inverse", Name = "Обратный" });
                };
                context.SaveChanges();
            }


            //Post type
            if (!context.PostTypes.Any())
            {
                foreach (var project in context.Projects)
                {
                    context.PostTypes.AddRange(
                        new PostType { Project = project, Code = "presenter", Name = "Презентационная должность" });
                }
                context.SaveChanges();
            }


            //Department
            if (!context.Departments.Any())
            {
                foreach (var project in context.Projects)
                {
                    context.Departments.Add(new Department { Code = "001", Name = "Головной отдел", DateStart = DateTime.Now, Type = context.DepartmentTypes.FirstOrDefault(), Project = project });
                    context.Departments.Add(new Department { Code = "002", Name = "Подразделение", DateStart = DateTime.Now, Type = context.DepartmentTypes.FirstOrDefault(), Project = project, Parent = context.Departments.FirstOrDefault(x => x.Project == project && x.Code == "001") });
                };
                context.SaveChanges();
            }


            //Post
            if (!context.Posts.Any())
            {
                foreach (var project in context.Projects)
                {
                    {
                        context.Posts.AddRange(new Post { Project = project, Name = "Презентатор", Type = context.PostTypes.FirstOrDefault(), Department = context.Departments.FirstOrDefault(), DateOpen = DateTime.Now });
                    };
                    context.SaveChanges();
                }

                //Employee
                if (!context.Employees.Any())
                {
                    foreach (var project in context.Projects)
                    {
                        context.Employees.AddRange(new Employee { Project = project, Name = "Сотрудник", DateOpen = DateTime.Now, TabNum = "001" });
                    };
                    context.SaveChanges();
                    foreach (var project in context.Projects)
                    {
                        context.EmployeePosts.Add(new EmployeePost { Employee = context.Employees.LastOrDefault(), Post = context.Posts.LastOrDefault(x => x.Project == project), DateOpen = DateTimeOffset.MinValue });
                    }
                    context.SaveChanges();
                }

                //Indicator
                if (!context.Indicators.Any())
                {
                    foreach (var project in context.Projects)
                    {
                        context.Indicators.AddRange(new Indicator
                        {
                            Code = "TestIndicators",
                            MeasureDefault = context.Measures.FirstOrDefault(x => x.Name == "Рубли"),
                            Name = "Тестовый индикатор 1",
                            Owner = context.Departments.FirstOrDefault(x => x.Code == "001"),
                            Parent = null,
                            PeriodDefault = context.Periods.FirstOrDefault(x => x.Code == "week"),
                            PlanDefault = context.Plans.FirstOrDefault(x => x.Code == "basic"),
                            Type = context.IndicatorTypes.FirstOrDefault(x => x.Code == "direct"),
                            Project = project
                        });
                    };
                    context.SaveChanges();
                }

                //Математические знаки
                if (!context.MathSigns.Any())
                {
                    context.MathSigns.AddRange(new MathSign { Value = "<", Description = "Less" },
                        new MathSign { Value = "<=", Description = "Less or equal" },
                        new MathSign { Value = "=", Description = "Equal" },
                        new MathSign { Value = ">", Description = "More" },
                        new MathSign { Value = ">=", Description = "More or equal" });
                    context.SaveChanges();
                }
            }

        }
    }
}