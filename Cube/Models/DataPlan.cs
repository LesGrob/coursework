using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    public class DataPlan
    {
        public DataPlan() { }

        public long Id { get; set; }

        [Required]
        public float Value { get; set; }
        [Required]
        public DateTime ReportDate { get; set; }
        [Required]
        public virtual Measure Measure { get; set; }
        [Required]
        public virtual Period Period { get; set; }
        [Required]
        public virtual Department Department { get; set; }
        [Required]
        public virtual Indicator Indicator { get; set; }
        [Required]
        public virtual Plan Plan { get; set; }

        public virtual Project Project { get; set; }
    }
}
