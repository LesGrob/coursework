using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    /// <summary>
    /// Сущность тип департамента
    /// </summary>
    public class DepartmentType
    {
        public DepartmentType() { }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Код типа подразделения
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Наименование подразделения
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Ссылка на WorkSpace
        /// </summary>
        public virtual Project Project { get; set; }
    }
}
