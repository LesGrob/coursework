using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    public class DataEmployee
    {
        public DataEmployee() { }

        public long Id { get; set; }

        public float Value { get; set; }

        public virtual DateTime ReportDate { get; set; }

        public virtual Measure Measure { get; set; }
        public virtual Period Period { get; set; }
        public virtual Department Department { get; set; }
        public virtual Indicator Indicator { get; set; }
        public virtual Employee Employee { get; set; }

        public virtual Project Project { get; set; }
    }
}
