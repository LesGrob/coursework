using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    public class StuffNorm
    {
        public StuffNorm() { }

        public long Id { get; set; }

        [Required]
        public virtual PostType Post { get; set; }

        [Required]
        public virtual Indicator Indicator { get; set; }
        [Required]
        public virtual Period Period { get; set; }
        [Required]
        public virtual Measure Measure { get; set; }
        [Required]
        public virtual Department Department { get; set; }

        [Required]
        public float Value { get; set; }

        [Required]
        public virtual DateTime DateStart { get; set; }

        public virtual DateTime DateEnd { get; set; }

        public virtual Project Project { get; set; }
    }
}
