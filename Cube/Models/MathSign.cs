using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    /// Сущность математический знак, используется в условиях
    public class MathSign
    {
        public MathSign() { }
        public long Id { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
    }
}
