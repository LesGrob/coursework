using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    public class Shelf
    {
        public Shelf() { }

        public long Id { get; set; }
        [Required]
        public long UserId = 1;
        [Required]
        public string Name { get; set; }
        [Required]
        public virtual Project Project  { get; set; }
    }
}
