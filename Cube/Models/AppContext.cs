using Microsoft.EntityFrameworkCore;

namespace Cube.Models
{
    /// <summary>
    /// Контекст подключения к БД
    /// </summary>
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    //Уникальный индекс для ViolationData по дате создания и типу отклонения
        //    //modelBuilder.Entity<ViolationData>().HasIndex(x => new { x.ReportDate, x.ViolationId, x.EmployeeId, x.DepartmentId }).IsUnique();

        //    //modelBuilder.Entity<DataFact>().Property(x => x.ReportDate).HasColumnType("date");
        //    //modelBuilder.Entity<DataPlan>().Property(x => x.ReportDate).HasColumnType("date");
        //    //modelBuilder.Entity<DataEmployee>().Property(x => x.ReportDate).HasColumnType("date");
        //    //modelBuilder.Entity<StuffNorm>().Property(x => x.DateEnd).HasColumnType("date");
        //    //modelBuilder.Entity<StuffNorm>().Property(x => x.DateStart).HasColumnType("date");

        //    //modelBuilder.Entity<ViolationData>().Property(x => x.ReportDate).HasColumnType("date");
        //}

        public DbSet<Project> Projects { get; set; }

        public DbSet<Indicator> Indicators { get; set; }
        public DbSet<DataFact> DataFacts { get; set; }
        public DbSet<DataPlan> DataPlans { get; set; }
        public DbSet<StuffNorm> StuffNorms { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<Plan> Plans { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<DepartmentType> DepartmentTypes { get; set; }
        public DbSet<IndicatorType> IndicatorTypes { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PostType> PostTypes { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<DataEmployee> DataEmployee { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Shelf> Shelf { get; set; }
        public DbSet<UserShelf> UserShelf { get; set; }
        public DbSet<EmployeePost> EmployeePosts { get; set; }
        public DbSet<MathSign> MathSigns { get; set; }
    }
}