using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    /// <summary>
    /// Сущность сотрудник
    /// </summary>
    public class Employee
    {
        public Employee() { }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string Middlename { get; set; }
        /// <summary>
        /// Долнжость сотрудника
        /// </summary>
        //[Required]
        //public virtual EmployeePost Post { get; set; }
        /// <summary>
        /// Дата найма сотрудника
        /// </summary>
        [Required]
        public virtual DateTimeOffset DateOpen { get; set; }
        /// <summary>
        /// Дата увольнения сотрудника
        /// </summary>
        public virtual DateTimeOffset? DateClose { get; set; }
        /// <summary>
        /// Табельный номер
        /// </summary>
        [Required]
        public string TabNum { get; set; }
        /// <summary>
        /// WorkSpace
        /// </summary>
        public virtual Project Project { get; set; }
    }
}
