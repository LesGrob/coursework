using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    public class Period
    {
        public Period() { }

        public long Id { get; set; }
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual Project Project { get; set; }
    }
}
