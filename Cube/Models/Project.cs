using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    /// <summary>
    /// Сущность описывающая WorkSpace
    /// </summary>
    public class Project
    {
		public Project() { }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Глобальный уникальный идентификатор WorkSpace
        /// </summary>
        [Required]
        public Guid UID { get; set; }
    }
}
