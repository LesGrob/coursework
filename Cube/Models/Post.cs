using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    /// <summary>
    /// Сущность должность
    /// </summary>
    public class Post
    {
        public Post() { }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Наименование должности
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// тип должности
        /// </summary>
        [Required]
        public virtual PostType Type { get; set; }
        /// <summary>
        /// Подразделение
        /// </summary>
        [Required]
        public virtual Department Department { get; set; }
        /// <summary>
        /// Дата создания должности
        /// </summary>
        [Required]
        public virtual DateTimeOffset DateOpen { get; set; }
        /// <summary>
        /// Дата закрытия долнжости
        /// </summary>
        public virtual DateTimeOffset? DateClose { get; set; }
        /// <summary>
        /// WorkSpace
        /// </summary>
        public virtual Project Project { get; set; }
    }
}
