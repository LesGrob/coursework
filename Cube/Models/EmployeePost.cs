using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    public class EmployeePost
    {
        public EmployeePost() { }

        public long Id { get; set; }

        [Required]
        public Employee Employee { get; set; }

        [Required]
        public virtual Post Post { get; set; }

        [Required]
        public virtual DateTimeOffset DateOpen { get; set; }
        public virtual DateTimeOffset? DateClose { get; set; }

        [Required]
        public float Unit { get; set; }
        public bool MainPost { get; set; }
    }
}
