using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    /// <summary>
    /// Сущность статус отклонения
    /// </summary>
    public class Status
    {
        public Status() { }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// WorkSpace
        /// </summary>
        public virtual Project Project { get; set; }
    }
}
