using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    public class UserShelf
    {
        public long Id { get; set; }
        [Required]
        public Shelf Shelf { get; set; }
        [Required]
        public Indicator Indicator { get; set; }

        public virtual Measure Measure { get; set; }
        public virtual Period Period { get; set; }
        public virtual Department Department { get; set; }
        public virtual Employee Employee { get; set; }
        public DateTime Date { get; set; }

        public UserShelf() { }
    }
}
