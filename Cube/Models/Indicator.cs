using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cube.Models
{
    public class Indicator
    {
        public Indicator() { }

        public long Id { get; set; }
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        public virtual IndicatorType Type { get; set; }
        [Required]
        public virtual Department Owner { get; set; }
        public virtual Indicator Parent { get; set; }

        [Required]
        public virtual Measure MeasureDefault { get; set; }
        [Required]
        public virtual Period PeriodDefault { get; set; }
        [Required]
        public virtual Plan PlanDefault { get; set; }

        public virtual Project Project { get; set; }
    }
}
